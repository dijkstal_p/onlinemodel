import os
import shutil
import math
from subprocess import call
import numpy as np

# Wrapper class to handle a distribution

class Dist:    

    def __init__(self):
        self.loaded=False
        self.betax=1
        self.betay=1
        self.alphax=0
        self.alphay=0
        self.emitx=0.3e-6
        self.emity=0.3e-6
        self.q=200e-12;
        self.p0=150/0.511
        self.sigt=3e-12
        self.sige=1e-5
        self.chirp=0
        self.sddsfile=''
        self.litrackfile=''

        


    def writeMadXInput(self):
        out=[]
        if self.loaded==False:
            return out
        out.append('\nBEAM, particle=Electron,energy=%f,sigt=%f,sige=%f;\n' % (self.p0*0.511e-3,self.sigt*3e8,self.sige/self.p0))
        out.append('betax0=%f;\n' % self.betax)
        out.append('betay0=%f;\n' % self.betay)
        out.append('alphax0=%f;\n' % self.alphax)
        out.append('alphay0=%f;\n\n' % self.alphay)
        return out


    def exportLiTrack(self):
        if len(self.litrackfile)>0:     # file exists already   
            return self.litrackfile

        if len(self.sddsfile)>0:        # elegant file exists and needs conversion
            fid=open('dummy','w')
            call(["sdds2stream",self.sddsfile,"-col=t,p"],stdout=fid)
            fid.close()
            dat=np.loadtxt("dummy")            
            call(["rm","dummy"])
            self.litrackfile="input.lit"
            fid=open(self.litrackfile,"w")
            for ele in dat:
                t=ele[0]*3e8*1e3
                e=ele[1]/self.p0*100
                fid.write("%20.14e %20.10e\n" % (t,e))
            fid.close()
            return self.litrackfile

        # generate an internal file
        N=50000
        dt=self.sigt*3e8*1e3
        t=np.random.normal(scale=dt,size=N)  # longitudinal position
        e=np.random.normal(scale=self.sige,size=N)*100+100 # normalized energy E/P0
        self.litrackfile="input.lit"
        fid=open(self.litrackfile,"w")
        for idx in range(0,N):
            e[idx]+=self.chirp*t[idx]/dt
            fid.write("%20.10e %20.10e\n" % (t[idx],e[idx]))
        fid.close()
        return self.litrackfile

   
    def importSDDS(self,filename,path, printout=0): 

        print('Importing SDDS Particle Distribution:', filename)

        self.sddsfile=path+'/'+filename
        fid=open(path+'/dist.txt','r')
        line=fid.readline()
        fid.close()
        col=line.strip().split()

        self.emitx=float(col[0])
        self.emity=float(col[1])
        self.betax=float(col[2])
        self.betay=float(col[3])
        self.alphax=float(col[4])
        self.alphay=float(col[5])
        self.sigt=float(col[6])
        self.sige=float(col[7])
        self.p0=float(col[8])
#        self.q=float(col[9])
        self.q=200e-12
        self.loaded=True

        if printout==0:
            return

        print('Bunch Length (ps)    :',self.sigt*1e12)
        print('Mean Energy (MeV)    :',self.p0*0.511)
        print('RMS Energy Spread (%):',self.sige*100)
        print('Emittance in x (nm)  :',self.emitx*1e9)
        print('Emittance in y (nm)  :',self.emity*1e9)
        print('Beta in x (m)        :',self.betax)
        print('Beta in y (m)        :',self.betay)
        print('Alpha in x           :',self.alphax)
        print('Alpha in y           :',self.alphay)
        print('Charge (pC)          :',self.q*1e12)
