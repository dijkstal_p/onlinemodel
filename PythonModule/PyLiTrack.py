## LiTrack in python
## Author: Masamitsu Aiba, masamitsu.aiba@psi.ch
## May, 2014
## The original LiTrack written in Matlab by Paul Emma (SLAC)
## is converted into python by the author.
##
## Some changes through the conversion:
## 0) Written in a class for general use
## 1) The input should be prepared through the member variables
## 2) The output will be in a class instance (can be easily switched to dictionary if necessary)
##    BLj is added to output, which contains beamline definition of the corresponding output
##    for example dE_Ej(3) is output at the end of BLj(3)=beamline[j],
##    where j is 3 or smaller, depending on the previous codes
## 3) Beamline definition is extended
##    - In the original code, it has 6 columns
##    - If 7th column (optional) is given in string,
##      it will be the element name in the output
##    - If code=2, the particle distribution is stored in memory for later use
##      and a method to damp to ascii file is provided
##      (Inddition to code=2, code=99 and <0 also store the particle distribution as in the original code)
## 4) GUI is unequipped because PyLiTrack is supporsed to be use in a python script
## 5) Importing the wakefield data from file is placed outside the tracking loop
##    Faster when the same file is used at the expence of a higher memory consumption
## 6) Initialisation of particle disctribution is in a separate method
## 7) It seems that the parabolic distribution in the original code is something wrong...
##    A common generation based on random numbers is implemented 
## 
## The reason for 1) and 2) is that, with such an interface,
## the script using this class is not necessarily modified when the code is updated. 
## For example, when one more output is added the exsiting code should still work as it is.
##
##

#### The following comments are from the original code

#function [zposj,dE_Ej,Ebarj,dFWpctj,ZFWmmj,z_barj,Ebarcutsj,fcutj,sigzGj,sigEGj,I_pkj,I_pkfj] = LiTrack(fn,seed,z0in,dQ_Q,param,blnew,inp_struc,wake_fn);
#%	[zposj,dE_Ej,Ebarj,dFWpctj,ZFWmmj,z_barj,Ebarcutsj,fcutj,sigzGj,sigEGj] = litrack_for_gui(fn[,seed,z0in,dQ_Q,param,blnew,inp_struc,wake_fn]);
#%	TRY IT....  just type:   litrack('spps0')
#%
#%	Function to do 2-D longitudinal phase space tracking of many
#%	electrons through several accelerator sections including bunch
#%	compression and the longitudinal wakefield of the accelerating
#%  	structures.
#%
#%	The beam and the various sections are described in your M-file
#%	called "*_lit.m" where "*" is a string contained in the above
#%	input argument: fn.
#%
#%  	The initial zpos and dE/E particle coordinates can be 'LiTrack'-
#%	generated Matlab gaussian distributions, uniform distributions, or
#%	they can be input from a user's ASCII, 2-column file (Z/mm, dE/E/%).
#%
#%	If no output arguments are provided, this function generates plots
#%	(see below).  If at least one output argument is provided, no plots
#%	are generated and the longitudinal coordinates of the N particles
#%	are returned as described below.
#%
#%  INPUTS:
#%	fn:		    A string which describes the leading characters of the beam-
#%			    line M-file with name "*_lit.m" where the fn string is represented
#%			    here as "*" (SEE FOR EXAMPLE the file LCLS_LIT.M which is
#%			    internall documented and would be run by setting fn='lcls').
#%	seed:	    (Optional,DEF=1) The random generator seed used.  If argument
#%			    not given the seed defaults to 1 for repeatability.
#%	z0in:	    (Optional,DEF=0) Initial 'phase' offset of bunch [mm].
#%	dQ_Q:	    (Optional,DEF=0) Relative charge error (e.g. 0.01 -> 1% more charge)
#%	param:	    (Optional,DEF=none) A 3-element vector of, param(1): the value of
#%			    the parameter to be scanned, param(2) the row number of the 'beamline'
#%			    array, and param(3) the column number of the 'beamline' array to be
#%			    replaced by param(1) - use 'scan_litrack_param.m'.
#%	blnew:	    (Optional,DEF=none) A full beamline to replace the 'beamline' array
#%			    in the *_lit.m file, so that the parameters can be randomized as
#%			    is done in 'rand_litrack.m'.
#%	inp_struc:	(Optional,DEF=none) Input structure to run LiTrack from a GUI, where
#%				all input parameters come from the GUI data structure, rather than
#%				from an LiTrack file such as 'spps0_lit.m'. No other input arguments
#%				are used when this structure is given (e.g., set them all = 0).
#%	wake_fn:	ASCII list of wakefield file names where one get pointed to in
#%				beamline array.
#%
#%  OUTPUTS:     (all below evaluated at each location where cod < 0 | cod == 99)
#%	zposj:	    The matrix of axial coordinates (after cuts) within
#%			    the bunch [in meters - head at z<0] w.r.t. the bunch center.  If
#%			    no output arguments are provided, the function will simply
#%			    generate a plot of 1) Z-distr., 2) dE/E-distr., 3) Wake-induced
#%			    voltage (only if the beamline described in fn ends on a linac
#%			    section with the wakefields switched on), and 4) dE/E versus Z
#%			    scatter plot.
#%	dE_Ej:	    The matrix of dE/E coordinates (after cuts, w.r.t. the mean energy)
#%			    as a unitless value (i.e. 0.01 corresponds to 1%).
#%	Ebarj:	    The array of mean electron energies BEFORE ANY CUTS [GeV]
#%	dFWpctj:	dE/E spread array, full-width at half-maximum [%]
#%	ZFWmmj:     Z full-width array at half-maximum [mm]
#%	z_barj:		Mean longitudinal bunch position array AFTER CUTS [mm]
#%	Ebarcutsj:	The mean electron energy array AFTER ALL CUTS [GeV]
#%	fcutj:		The fraction of particles that were cut out [ ]
#%	sigzGj:		Gaussian fitted rms bunch length array [mm]
#%	sigEGj:		Gaussian fitted rms relative energy spread array [%]
#%   I_pkj:     Peak current anywhere in bunch [kA]
#%	I_pkfj:		Peak current from maximum of a Gaussian fit [kA]
#
#
#
#%==============================================================================================================
from math import *
import numpy as np
#from scipy.special import erf
from time import gmtime, asctime
from random import seed,random,gauss,uniform,betavariate, sample
from string import *

global isScipy
try:
    import scipy.special as ss 
    import scipy.optimize as so
    isScipy = 1
except:
    print('Scipy is not available...')
    isScipy = 0

# in case scipy is not available... 
def erf(x):
    # constants
    a1 =  0.254829592
    a2 = -0.284496736
    a3 =  1.421413741
    a4 = -1.453152027
    a5 =  1.061405429
    p  =  0.3275911

    # Save the sign of x
    sign = 1
    if x < 0:
        sign = -1
    x = abs(x)

    # A & S 7.1.26
    t = 1.0/(1.0 + p*x)
    y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x)

    return sign*y


class dummyClass: # dummyClass to be packed with the ouput
    def __init__(self,inDict):
        self.__dict__.update(inDict)


class PyLiTrack:
    def __init__(self):
        

        self.beamline = []        # Beam line sequece
        self.wake_fn = []      # Wake functions to be used
        self.wakefunction = [] # to be filled
        self.Nbin=100          # Number of bins for wakefield 
        self.seed = 1          # Seed for the random number generation
        seed(self.seed)
        self.param = 0
        self.noparam = 0


        #self.inp_struc=[] # GUI is omitted


        ## Parameters for the initial distribution

        self.Ne = 1e10      # The number of initial real particles (not macro particle)
        self.E0 = 0.13 # Initial energy in [GeV]

        self.z0 = []
        self.d0 = []
        self.sigz0 = 1e-3
        self.sigd0 = 0.1e-3
        self.Nesim = 10000
        self.z0_bar = 0.0 # Position offset of bunch in [m] (used also with file input - mean of file removed first)     
        self.d0_bar = 0.0 # Relative energy offset of bunch []  (used also with file input - mean of file removed first)

        #self.gzfit = 0     # if ==1: fit Z-distribution to gaussian (defaults to no-fit if 'gzfit' not provided)      
        #self.gdfit = 0     # if ==1: fit dE/E-distribution to gaussian (defaults to no-fit if 'gdfit' not provided)   
        self.sz_scale = 1   # z scalar defaults to 1
        self.asym = 0       # default to no dist. asymmetry
        self.nsamp = 1      # default to using all input file points
        self.unif_halo = 0  # set =1 if you want uniform z and E halo 
        self.tail = 0       # default to no dist. tail
        self.cut = 10       # default to no dist. cuts
        self.halo = 0       # default to no dist. 1%-halo
        self.halo_pop = 0   # halo relative population defaults to zero  
        self.z0in = 0
        self.dQ_Q = 0

        self.inp = 'G'      # The default distribution is Gaussian

        ## Extra parameters for PyLiTrack flow
        self.isPartDist= 0


        ## Physics constants
        self.elec = 1.6022e-9  # Coulombs/(1E10 e-)
        self.cspeed = 2.99792458e8  # light speed [m/sec]


        ## Parameters for GUI omitted
        #self.contf= 0
        #self.splots
        #self.plot_frac
        #self.comment=''

    def changeSeed(self,seedi):
        self.seed = seedi or self.seed
        seed(self.seed)

    def inputDeck(self):
        fout=open('PyLiTrack_input.txt','w')
        fout.write('### Assme your instance name is lt.\n\n')
        fout.write("lt.wake_fn = ['slac.dat']   # Wake functions to be used\n")
        fout.write('lt.Nbin=100       # Number of bins for wakefield (and enrgy/z cut)\n')
        fout.write('lt.Ne = 1e9       # The number of initial real particles (not macro particle)\n')
        fout.write('lt.E0 = 0.13      # Initial energy in [GeV]\n')
        fout.write('lt.sigz0 = 1e-3   # Initial bunch length [m] (ignored for file input)\n')
        fout.write('lt.sigd0 = 0.1e-3 # Initial rerative energy spread [] (ignored for file input)\n')
        fout.write('lt.Nesim = 10000  # Initial number of macro particles.\n')
        fout.write('lt.z0_bar = 0.0   # Position offset of bunch in [m] (used also with file input - mean of file removed first)\n')  
        fout.write('lt.z0in = 0.0     # Initial phase offset of bunch [mm] (used also with file input - mean of file removed first)\n')
        fout.write('lt.d0_bar = 0.0   # Relative energy offset of bunch []  (used also with file input - mean of file removed first)\n')
        fout.write('lt.nsamp = 1      # default to using all input file points (used only for file input)\n')
        fout.write('lt.sz_scale = 1   # z scalar defaults to 1 (used only for file input)\n')

        fout.write('lt.asym = 0       # default to no dist. asymmetry (only for inp=''G'' and ''M'') \n')
        fout.write('lt.unif_halo = 0  # set =1 if you want uniform z and E halo (only for inp=''G'')\n')
        fout.write('lt.tail = 0       # tail (only for inp=''G'' and ''M'') \n')
        fout.write('lt.cut = 10       # cut (only for inp=''G'')\n')
        fout.write('lt.halo = 0       # halo (only for inp=''G'')\n')
        fout.write('lt.halo_pop = 0   # halo relative population defaults to zero  (only for inp=''G'')\n')
        
        fout.write('lt.dQ_Q = 0       # Relative charge fluctuation\n')
        fout.write("lt.inp = 'G'      # Particle distribution to generate, G/U/P/M/file\n")
        
        fout.write("lt.beamline = [[-1, 0, 0, 0, 0, 0, 'Start'],[99, 0, 0, 0, 0, 0, 'End']]  # Input your beamline, 7th column is optional\n\n")

        fout.write('### Run tracking after you modify the above in your script.\n')
        fout.write("### execfile('pyLiTrack_input.txt')\n")
        fout.write('### lt.initPartDist()\n')
        fout.write('### oc=lt.track()\n')
        
        
        fout.close()

    def asym_gaussian(self, xsig, xbar, HD):  # asym_gaussian.m (support function)
        # HD ='G' or 'U' specify if the halo is Gaussian or Uniform

        # Could be asym_gaussian(self, HD) with
        # xsig = self.sigz0
        # xbar = self.z0_bar + self.z0in * 1.0e-3

        if abs(self.asym) >= 1:
            print('Error: asymmetry parameter must be -1 < asym < 1')
            return []
        if self.cut < 0.5:
            print('Error: cut must be 0.5 <= cut < inf')
            return []
        if self.tail < 0 or self.tail >=1:
            print('Error: tail must be 0 <= tail < 1')
            return []
        if self.halo < 0:
            print('Error: halo must be >= 0')
            return []
        if  (self.halo>0) and (int(round(1.0/self.halo_pop))>self.Nesim):
            print('Error: halo_pop is too small...')
            return []

        f1 = 1.0 + self.asym
        f2 = 1.0 - self.asym

        if isScipy == 1:
            erfn = ss.erf(self.cut/sqrt(2))/2.0 # see erfn.m (support function)
        else:
            erfn = erf(self.cut/sqrt(2))/2.0 


        N1 = int(round(self.Nesim*f1/4/erfn))       #boost N1 to accomodate cuts
        N2 = int(round(self.Nesim*f2/4/erfn))       #boost N2 to accomodate cuts

        
        #x1 = f1*xsig*randn(2*N1,1)           # generate a gaussian distribution + offset
        #i  = find(x1<0 & x1>(-cut*xsig*f1))  # eliminate positive values and cuts
        #x  = x1(i)
        #x1 = f2*xsig*randn(2*N2,1)           # generate a gaussian distribution + offset
        #i = find(x1>0 & x1<(cut*xsig*f2))    # eliminate NEGATIVE values
        #x = [x; x1(i)]
        #clear x1, i
        #NN = length(x)
        #dN = N - NN
        #if dN > 0:
        #    x1 = min([f1 f2])*xsig*(rand(dN,1)-0.5)  # generate a gaussian distribution + offset
        #    x = [x; x1]
        #elif dN < 0:
        #    adN = abs(dN)
        #    i = round((NN/adN)*(1:adN))
        #    x(i) = []

        ## the following implementation is more exact...
        i = 0
        x=np.zeros(self.Nesim)
        r=1.0*self.Nesim/(N1+N2)
        N1=int(round(N1*r))
        N2=int(round(N2*r))
        
        if (N1+N2)!=self.Nesim:
            # one particle might be too much or missing...
            N1=N1-(self.Nesim-(N1+N2))
        while i<N1:
            x1 = f1*xsig*gauss(0.0,1.0) 
            if (x1<0.0) and (x1>(-self.cut*xsig*f1)):
                x1 = x1 + gauss(0.0,1.0)*self.tail*xsig
                x[i] = x1
                i=i+1
        while i<self.Nesim:
            x1 = f2*xsig*gauss(0.0,1.0) 
            if (x1>0.0) and (x1<(self.cut*xsig*f2)):
                x1 = x1 + gauss(0.0,1.0)*self.tail*xsig
                x[i] = x1
                i=i+1

        xm = x.mean()

        x = x - xm
        x = x*xsig/x.std() # Renormalize the bunch length taking into account the tail

        if (self.halo > 0) and (self.halo_pop > 0):
            for i in range(0,self.Nesim,int(round(1.0/self.halo_pop))):
                if HD=='G':
                    x[i] = x[i] + gauss(0.0,1.0)*self.halo*xsig
                elif HD=='U':
                    x[i] = 2*self.halo*xsig*(random()-0.5)
    

        x = x + xbar

        return x
        

    def gen_dist(self,xsig,xbar):

        slew=self.asym

        if (self.tail >= 1) or (self.tail <= 0):
            print('Error: tail must be <1 and >0')
            return []
        if (slew >= 1) or (slew <= -1):
            print('Error: slew (asym) must be < 1 and > -1')
            return []

        n  = 2.0/self.tail
        nn = int(round(n))

        #fj = 2*((0:(n/(nn-1)):n) - n/2)/n;
        fj = 2.0*(np.array(range(0,nn))-(nn-1)/2.0)/(nn-1)
        Nn = (self.Nesim/n)*(1 + slew*fj)
        Nn = np.rint(Nn)
        dN = self.Nesim - sum(Nn)
        if dN:
            for i in range(0,abs(dN)):
                if slew>=0:
                    Nn[nn-i] = Nn[nn-i] + int(signcopy(1,dN)) # python does not have sign()...
                else:
                    Nn[nn] = Nn[nn] + int(signcopy(1,dN))


        # Original code for the above if block, a bit ugly...
        # dN = N - sum(Nn);
        # Nn = Nn + round(dN/nn);
        # dN = N - sum(Nn);
        # round(nn/2)
        # Nn(round(nn/2)) = Nn(round(nn/2)) + dN

        x = np.array([])
        for i in range(0,nn):
            xj=np.zeros(int(Nn[i]))
            for j in range(0,int(Nn[i])):
                xj[j] = self.tail*gauss(0.0,1.0) + 1.5*i*self.tail;
            x  = np.concatenate([x,xj])
            
        xm = x.mean()

        x = x - xm
        x = xsig*x/x.std()
        x = x + xbar
        
        return x

    def loadZD(self, inp=None):

        inp = inp or self.inp
        try:
            fin=open(inp,'r')
        except:
            print('Error: '+self.inp+' was not found.')
            return [[], []]

        x=[]
        y=[]
        Nesim=0
        for line in fin:
            wline=line.split()
            try:
                x.append(1.0e-3*float(wline[0]))
                y.append(1.0e-2*float(wline[1]))
                Nesim=Nesim+1
            except:
                fin.close()
                print('Error: the particle distribution (ZD-file) should contain 2 columns')
                return [], []

        Nesim = len(x)

        x=np.array(x)
        y=np.array(y)
        

        fin.close()

        if Nesim<2:
            print('Error: Z, dE/E input file '+self.inp+' requires at least 2 rows for Z/mm and dE/E/% of particles')
            return [], []


        if self.nsamp > 1:  # if we want a random subset of the input file coordinates    
            ind=xrange(Nesim)
            Nesim=int(round(Nesim/self.nsamp)) # Number of particles after sampling
            inds=sample(ind,Nesim)             # random permutation of integers from 1 to Nesim (to select random set of file points)
            xnew=np.zeros(Nesim)
            ynew=np.zeros(Nesim)
            for j in range(0,Nesim):           # now take subset of permutations
                xnew[j]=x[inds[j]]
                ynew[j]=y[inds[j]]
            x=xnew
            y=ynew


        xm = x.mean()
        x = x - xm
        x = self.sz_scale*x + self.z0_bar

        ym = y.mean()
        y = y - ym
        y = self.sz_scale*y + self.d0_bar
        
        # The calculation of the rms bunch length and relative energy spread is before the scaling????
        #  sigz0 = std(z0);				        % calculate the rms bunch length [m]
        #  sigd0 = std(d0);				        % calculate the rms relative energy spread [ ]
        #  z0    = sz_scale*(z0 - mean(z0));	    % remove mean from ZD-file's z-distribution [m] and scale sigZ (you can put non-zero mean in BL-file)
        #  d0    = sz_scale*(d0 - mean(d0));		% remove mean from ZD-file's dE/E distribution [ ] and scale sigE/E (you can put non-zero mean in BL-file)
        #  disp(' ')
        #  disp('INFO: removing mean from file''s original z and dE/E-distributions before proceeding...')	% warn of this action
        #  disp(' ')
        #  z0 = z0 + z0_bar;				% add a potential Z-offset which is input from the BL-file [m]
        #  d0 = d0 + d0_bar;				% add a potential dE/E-offset which is input from the BL-file [ ]
        #
        # Let's implement sigz0 and sigd0 as the values after scaling....
        self.sigz0 = x.std()
        self.sigd0 = y.std()


        self.Nesim = Nesim

        return x,y



    def initPartDist(self, inp=None): # Initialize the particle distribution

        if not isinstance(self.Nesim, int):
            print('Error: the number of macro particles should be given as interger.')
            return 

        inp = inp or self.inp

        z0_bar_i = self.z0_bar + self.z0in * 1.0e-3
        
        if self.dQ_Q != 0:
            dQ_Q = self.dQ_Q
            if abs(dQ_Q) > 0.8:
                print('Error: dQ_Q is > 80%, this seems unreasonable - quitting.')
                return
            
            self.Ne1 = self.Ne*(1.0+dQ_Q) # This should be a member variable for the later use in tracking
        else:
            self.Ne1 = self.Ne # This should be a member variable for the later use in tracking


        Nesim=self.Nesim
        z0=np.zeros(self.Nesim)                # Pre allocation
        d0=np.zeros(self.Nesim)

        if inp == 'G':	# (Asymmetry) Gaussian distribution
            for i in range(0,self.Nesim):
                d0[i] = self.sigd0*gauss(0.0,1.0) + self.d0_bar  # always gaussian dE/E
            if self.unif_halo==0:  # Gaussian halo
                z0 = self.asym_gaussian(self.sigz0, z0_bar_i, 'G')  # generate asymmetric gaussian with cuts & tails
                # Note that the Gaussian halo is not applied to the energy spread in the original code.
            else:             # Uniform halo
                z0 = self.asym_gaussian(self.sigz0, z0_bar_i, 'U')
                if (self.halo > 0) and (self.halo_pop > 0):
                    for i in range(0,self.Nesim,int(round(1.0/self.halo_pop))):
                        d0[i] = 2*self.halo*self.sigd0*(random()-0.5)
                else:
                    print('WARNING: Though unif_halo=1(True), halo is not added because of either halo=<0, halo_pop=<0 or both...')
        elif inp == 'U':   # Uniform distribution
            zwid = sqrt(12)*self.sigz0  # interpret "sigz0" as rms (=>full width = sqrt(12)*rms)
            dwid = sqrt(12)*self.sigd0  # interpret "sigd0" as rms (=>full width = sqrt(12)*rms)
            for i in range(0,self.Nesim):
                z0[i] = zwid*(random()-0.5) + z0_bar_i;	# generate uniform distribution + offset for Z [m]
                d0[i] = dwid*(random()-0.5) + self.d0_bar;	# generate uniform distribution + offset for dE/E [ ]
        elif inp == 'P':  # Parabolic distribution
            # The original code results in zero slice energy spread beam...so, implement as a random-number-based distribution 
            zwid = sqrt(5)*self.sigz0  # interpret "sigz0" as rms (=>full width = sqrt(5)*rms)
            dwid = sqrt(5)*self.sigd0  # interpret "sigd0" as rms (=>full width = sqrt(5)*rms)
            for i in range(0,self.Nesim):
                # Parabolic distribution is special case of beta-distribution with the two parameters = [2, 2]
                z0[i] = zwid*(betavariate(2.0,2.0)-0.5) + z0_bar_i;  # generate parabolic distribution + offset for Z [m]
                d0[i] = dwid*(betavariate(2.0,2.0)-0.5) + self.d0_bar;  # generate parabolic distribution + offset for dE/E [ ]
        elif inp == 'M':   # Generalized (multiple gaussians) distribution
            z0 = self.gen_dist(self.sigz0,z0_bar_i)	# generate general distribution + offset for Z [m]
            for i in range(0,self.Nesim):
                d0[i] = self.sigd0*gauss(0.0,1.0) + self.d0_bar  # always gaussian dE/E
        else:  # Distribution taken from another file (ZD-file)...
            [z0, d0] = self.loadZD(inp) # Implemented separately
            

        #
        #if Nbin < 10					% bomb out if BL-file specifies <10 for Z and dE/E binning
        #  error(sprintf('Nbin = %g cannot be less than 10',Nbin))
        #end
        #

        if len(z0)==len(d0) and len(z0)>0:
            self.isPartDist = 1
            self.z0=z0
            self.d0=d0
        else:
            self.isPartDist = 0
            print('Error: Something wrong during the initialization of particle distribution...')
            return


    def loadWakefunctions(self,fl=None):
        fl = fl or self.wake_fn  # wake function file list
        
        if len(self.beamline)==0:
            print('Error: Define beamline before loading wakefunction(s)')
            return
        nwake_fn = len(fl)        # count number of files provided
        if nwake_fn==0:
            print('Using default wake function.')
            
            self.wake_fn=['/afs/psi.ch/intranet/FIN/Simulation/SwissFEL/PyLiTrack/slac.dat']
            nwake_fn = 1

        else:
            fmax=0
            for i in range(0,len(self.beamline)):
                if fmax<self.beamline[i][4]:
                    fmax=self.beamline[i][4]
            if fmax>nwake_fn:
                #print 'Need multiple wake function file names when "wakeON/OFF" > 1'
                print('The beamline specifies the number of wakefunctions larger than the number of file names given')

        self.wakefunction=[]
        for i in range(0,nwake_fn):
            zfi=[]
            wfi=[]
            fin=open(self.wake_fn[i],'r')
            for line in fin:
                sline=line.split()
                if len(sline)<2:
                    print('Error: Point-charge wake function needs two columns')
                    return 
                zfi.append(float(sline[0]))
                wfi.append(float(sline[1]))
            fin.close()
            self.wakefunction.append([zfi,wfi])


    #        
    #  convolution of the wake function        

    def long_wake(self, z, L=1, pcwake=[]):

        if len(pcwake)==0:
            print('Error: Empty wake function is given...?')
            return [],[]

        nn = len(z)
        sigz = z.std()

        if nn < 100:
            print(' ')
            print('Error: Probably too few particles in your input "z" array')
            print(' ')
            return [],[]
        if nn > 5e6:
            print(' ')
            print('Error: >5E6 particles in your "z" array - let''s not get carried away now')
            print(' ')
            return [],[]
        if sigz < 1e-6:
            print(' ')
            print('Error: Bunch length of <1 micron may be too short for this Green''s function')
            print(' ')
            return [],[]

        Ne = self.Ne
  

        zfnvar = pcwake[0]   # m
        Wfnvar = pcwake[1]   # V/C/m
        nA     = len(zfnvar)

        # python histogram() returns edges of bins while matlab hist() the center of bins
        [N,ze] = np.histogram(z,self.Nbin-2)         # Binning with Nbin-1
        dzc = ze[1]-ze[0]                       # 
        zc = ze + dzc*0.5                           # Edge to center
        zc=zc.tolist()
        zc.insert(0,zc[0]-dzc)
        #np.append(zc,zc[len(zc)-1]+dzc)
        zc=np.array(zc)
        N=N.tolist()
        N.insert(0,0)
        N.append(0)


        maxz_fn = zfnvar[nA-2]                  # max Z in wake file (last point usually projected estimate) index starts at 0 in python
        if (max(z)-min(z)) > maxz_fn:
            print(' ')
            #disp(['WARNING: maximum axial spread is > ' num2str(maxz_fn*1e3) ' mm and the RW-wake is inaccurate there'])
            print('WARNING: the bunch length is longer than the given wakefunction range, '+str(maxz_fn*1e3)+' mm')
            print(' ')

        # empty wakefield array
        dE  = np.zeros(self.Nbin)
        e   = self.elec/1e10 #1.6022e-19
        scl = -L*(Ne/nn)*e*1e-6                 # 1e-6 for eV to MeV (?)

        # New procedure to calculate the Wakepotential
        
        Weven=np.interp(zc-min(zc),zfnvar,Wfnvar)
        Weven[0]*=0.5    
        Ntmp=np.array(N)

        for i in range(0,self.Nbin):
            Irev=np.arange(i,-1,-1)            # reverse indexing
            dE[i]+=np.sum(scl*Weven[0:i+1]*Ntmp[Irev])

        return dE,zc 

        # the following is the old routine which is way slower in python
        # print out is included to compare both methods    
        dE*=0
        
        for j in range(0,self.Nbin):                 # jth bin is test-bin
            zcj = zc[j]                         #  save test bin z
            for k in range(0,self.Nbin):             # kth bin is field-bin
                zck = zc[k]
                if zck > zcj:			# no wake when field-bin behind test-bin
                    break
                dz = zcj - zck                  # separation of field & test-bins (>=0)
                ddz = min(abs(zfnvar-dz))
                ii  = np.argmin(abs(zfnvar-dz))
                ddz = copysign(1.0,zfnvar[ii]-dz)*ddz
                if ddz > 0:
                    i1 = ii - 1
                    i2 = ii
                    if i1 == 0:
                        print('Error: Index into zeroth entry of wakefield array - try finer steps in wake file')
                        return [],[]
                
                    dz1 = zfnvar[i1]
                    dz2 = zfnvar[i2]
                    W1  = Wfnvar[i1]
                    W2  = Wfnvar[i2]
                    W   = W2 - (W2-W1)*ddz/(dz2-dz1)
                elif ddz < 0:
                    i1 = ii
                    i2 = ii + 1
                    if i2 > len(zfnvar):
                        print('Error: Index to a point beyond wakefield array - try extending the wake file')
                        return [],[]
                    dz1 = zfnvar[i1];
                    dz2 = zfnvar[i2];
                    W1  = Wfnvar[i1];
                    W2  = Wfnvar[i2];
                    W   = W1 - (W2-W1)*ddz/(dz2-dz1);
                else:
                    W   = Wfnvar[ii]/2.0     # use 1/2 wake for self loading of bin
                #print j,k, len(dE), len(N)
                dE[j] = dE[j] + scl*N[k]*W   # add each field-bin's wake to each test-bin's energy loss
        
        return dE, zc


    def rw_wakefield(self,s,r,s0,tau=None,rf=None):
        
        for i in range(0,len(s)):
            if s[i]<0:
                print('Error: s should be >= 0')
                return [[],[]]

        s=np.array(s)
        s_s0 = s/s0

        Z0 = 120.0*pi
        sig = 2.0*r**2/Z0/s0**3

        rf = rf or 0
        tau= tau or 0

        if tau:
            Gamma = self.cspeed*tau/s0
            if Gamma<=2.5:
                # 5th-order coefficients (0<=Gamma<=2.5), col-1 is cylindrical, col-2 is parallel plates - Dec. 3, 2004
                krs0c =[[1.81620118662482, 1.29832152141677],
                        [0.29540336833708, 0.18173822141741],
                        [-1.23728482888772, -0.62770698511448],
                        [ 1.05410903517018, 0.47383850057072],
                        [-0.38606826800684, -0.15947258626548],
                        [ 0.05234403145974, 0.02034464240245]]
                # 3rd-order coefficients (0<=Gamma<=2.5), col-1 is cylindrical, col-2 is parallel plates - Dec. 3, 2004
                Qrc = [[1.09524274851589, 1.02903443223445],
                       [2.40729067134909, 1.33341005467949],
                       [0.06574992723432, -0.16585375059715],
                       [-0.04766884506469, 0.00075548123372]]	
                krs0 = 0
                Qr   = 0
                A = [1.0, pi**2.0/16.0]
                for j in range(0,len(krs0c)):
                    krs0 = krs0 + krs0c[j][rf]*Gamma**(j)
                for j in range(0,len(Qrc)):
                    Qr = Qr + Qrc[j][rf]*Gamma**(j)
                kr = krs0/s0
                E = -A[rf]*Z0*self.cspeed/pi*( np.exp(-kr*s/(2.0*Qr))*np.cos(kr*s) )/r**2
            else:
                wp = sqrt(Z0*self.cspeed*sig/tau)
                E  = -Z0*self.cspeed/pi*( np.exp(-s/(4.0*self.cspeed*tau))*np.cos(sqrt(2.0*wp/r/self.cspeed)*s) )/r**2
        else:
            alf = 0.9
            a   = 3.0/(4.0*sqrt(2.0*pi))
            E   = -Z0*self.cspeed/(4*pi)*( (16.0/3.0)*np.exp(-s_s0)*np.cos(sqrt(3.0)*s_s0) - (sqrt(2*pi)*(a**alf+s_s0**(3.0*alf/2.0))**(1/alf))**(-1) )/r**2

        return E

    def FWHM(self, X, NX, f=0.5):
        # if not f=0.5, it is a full width at f*maximum...
        # this code is not very good for a small number of bins because of no interpolation to find f-crossings...
        if (f<=0) or (f>=1):
            print('Error: "f" should be: 0 < f < 1')
            return [0,0,0]


        Nbin = len(X)
        if Nbin < 2:
            print('Error: Need at least 2 bins in array "X" and "NX"')
            return [0,0,0]


        i1 = 1
        i2 = Nbin
        ok1 = 0
        ok2 = 0
        Nmax   = max(NX)
        for j in range(0,Nbin):
            if NX[j] > f*Nmax:
                i1 = j
                ok1 = 1
                break

        for j in range(0,Nbin):
            if NX[Nbin-j-1] > f*Nmax:
                i2 = Nbin-j-1
                ok2 = 1
                break
        if ok1*ok2 == 0:
            print(' ')
            print('WARN: ==> FWHM not found')
            print(' ')
            return [0,0,0]
        if i1 >= i2:
            print(' ')
            print('WARN: ==> FWHM not found')
            print(' ')
            return [0,0,0]
        FW = abs(X[i2] - X[i1])

        return [FW, i1, i2]


    def gauss_fit(self, D, y_off=0):
        #python implementation
        
        if len(D) < 100:
            #print 'Error: Need at least 5 data points to fit a gaussian'
            print('Warning: Less than 100 particles... no gauss fit applied.')
            return [0,0,0,0]

        y,x=np.histogram(D,100)        

        x=np.array(x[0:len(x)-1]) # Drop the last edge
        x=x+(x[1]-x[0])/2.0  # From edge to centre
        y=np.array(y)
        dy = np.ones(len(y))  # fine for simulation

        
        # Initialization with rough value
        amp = y.max()
        mu  = x.mean()
        sig = x.std()
        sky = 0.0

        if isScipy:
            # scipy is available
            def chi_gauss(p):
                sig2 = p[2] ** 2
                m = p[0] * np.exp(-0.5 * (x - p[1]) ** 2 / sig2) #/ np.sqrt(2 * np.pi * sig2)
                return (y - m) / dy
 
            def chi_gauss_o(p):
                sig2 = p[2] ** 2
                m = p[3] + p[0] * np.exp(-0.5 * (x - p[1]) ** 2 / sig2) #/ np.sqrt(2 * np.pi * sig2)
                return (y - m) / dy
 
            
            if y_off:
                amp, mu, sig, sky = so.leastsq(chi_gauss_o, [amp, mu, sig, sky])[0]
            else:
                amp, mu, sig = so.leastsq(chi_gauss, [amp, mu, sig])[0]
                sky = 0.0
        else:
            # If scipy is not available, no iterative optimization
            #amp = y.max() # Done
            mu = sum(x*y)/sum(y)
            sig = np.sqrt(abs(sum((x-mu)**2*y)/sum(y))) # giving just the std of the distribution...
            # sig = D.std(), the same...
            if y_off:
                print('Warning: Sorry that Gaussian-with-offset cannot be found without scipy...')
                print('... offset is forced to be zero.')

        return [amp, mu, sig, sky]


    def track(self): # run the tacking loop


        BLj=[]                            # Beamline element in output only in python
        zposj=[]
        dE_Ej=[]
        Ebarj=[]
        z_barj=[]
        ZFWmmj=[]
        sigzGj=[]
        dFWpctj=[]
        Ebarcutsj=[]
        sigEGj=[]
        I_pkj=[]
        I_pkfj=[]
        fcutj=[]

        outdict={}
        outdict['BLj']=BLj
        outdict['zposj']=zposj
        outdict['dE_Ej']=dE_Ej
        outdict['Ebarj']=Ebarj
        outdict['z_barj']=z_barj
        outdict['ZFWmmj']=ZFWmmj
        outdict['sigzGj']=sigzGj
        outdict['dFWpctj']=dFWpctj
        outdict['Ebarcutsj']=Ebarcutsj
        outdict['sigEGj']=sigEGj
        outdict['I_pkj']=I_pkj
        outdict['I_pkfj']=I_pkfj
        outdict['fcutj']=fcutj
        
        oc=dummyClass(outdict)           # and an empty data will be spat out when error


        if not isinstance(self.Nbin, int):
            print('Error: the number of bins should be given as interger.')
            return 

        beamline = self.beamline         # Copy it to local variable...
        if len(beamline)==0:
            print('Error: No beamline defined, no tracking...')
            return oc

        nb = len(beamline)
        if nb < 1:
            print('Error: "beamline" array (self.beamline) requires at least 1 row')
            return oc
        for i in range(0,nb):
            ncol = len(beamline[i])
            if ncol<6:
                print('Error: "beamline" array (self.beamline) requires at least 6 columns')
                return oc

        if self.isPartDist==0:
            print('Error: No particle distribution, no tracking...')
            return oc

        if len(self.wakefunction)==0:
            print('Loading wakefucntion(s)...')
            self.loadWakefunctions()
            
        start_time = gmtime()
        print(' ')
        print('LiTrack (tracking) started: '+asctime(start_time))
        print(' ')
        #% Start doing real calculations:
        #% ==============================


        # Implement param later or never...
        #
        #if noparam==0
        #  beamline(param(2),param(3)) = param(1);	% used to scan a parameter using 'scan_litrack_param.m'
        #end
        #

        Ne0    = self.Ne1	          # save N electrons to get fraction lost
        Nesim0 = self.Nesim               # save N macro-particles to get fraction lost
        z      = self.z0                  # axial position within bunch w.r.t. mean position [m]
        d      = self.d0                  # relative energy deviation w.r.t. mean energy [ ]
        E      = self.E0*(1.0 + self.d0)  # absolute energy of particles [GeV]
        Ebar   = E.mean()		  # mean particle energy [GeV]
        Ebarcuts = E.mean()               # mean particle energy after later cuts [GeV]
        z_bar  = 1.0e3*z.mean()		  # mean z-position [mm]
        sigz   = self.sigz0               # rms bunch length (w.r.t. mean) [m]
        sigd   = self.sigd0               # rms relative energy spread [ ]


        if isScipy==0:
            print('Warning: Gauss-fit routine will give you a simple standard deviation without scipy...')


        #Initialize output each time to run tracking
        #jc = 0                            # start with zero output locations (where cod < 0 | cod == 99)

        # The main tracking loop
        for j in range(0,nb):                           # loop over all beamline sections of beamline
            if len(beamline[j]) == 6:
                beamline[j].append('Beamline_'+str(j))  # Add beam line element name (only in python version) 
            cod  = beamline[j][0]                       # beamline section code (e.g. 11=accelerator section - from K. Bane convention)
            if (abs(cod)==1):                           # do-nothing code (e.g., used to plot input beam)
                #  do nothing - plot only if cod=-1 (see below)
                0
            #if (abs(cod)==2):                           # will be done in below
            #    #    fnout = 'LiTrack_zd_output.dat';
            #    #    dump_LiTrack_output(1E3*z,100*d,fnout);
            #    #    dis(['2-column ASCII output file written: [z(mm) dE/E(%)]: ' fnout])
            if (abs(cod)==11) or (abs(cod)==10):        # ACCELERATION SECTION (11 and 10)
                print('Acceleration  Section (Code 11 or 10):', beamline[j][6])
                Eacc   = beamline[j][1]                 # nominal acc (w/o wake and for phi=crest(=0)) [GeV]
                phi    = beamline[j][2]                 # acc phase (crest=0, low-E head at phi < 0) [deg]
                phir   = phi*pi/180.0                   # RF phase in radians
                lam    = beamline[j][3]                 # RF wavelength [m]
                if lam<=0:
                    print('Error: RF wavelength cannot be <= 0, terminating tracking...')
                    return oc

                wakeon = beamline[j][4]                 # wakeON=1,2,..., wakeOFF=0
                Lacc   = beamline[j][5]                 # length of acc section (scales wake) [m]
                
                if wakeon:                              # if wakes calc switched ON...
                    #iswake = 1				# turns wake plot on
                    wake_fn1 = self.wake_fn[wakeon-1]   # select proper wake function depending on wakeon (=1,2,...)
                    pcwake=self.wakefunction[wakeon-1]
                    [dE_wake,zc_wake] = self.long_wake(z,Lacc,pcwake) # calculate dE_wake in MeV from Z-coordinates, acc length, N-particles, etc.
                    dE_wakes = np.interp(z,zc_wake,dE_wake)
                    dE_loss = 1e-3*dE_wakes.mean()       # wake loss [GeV]
                else:                                    # if wake calc switched OFF...
                    #      iswake = 0;				    	% no wake plot
                    # dE_wakes = np.zeros(self.Nbin)     # dE_wake is all zeros, not used actually...
                    dE_loss = 0                          # wake loss = 0 without wakes ON
                if abs(cod) == 10:                       # special case where Eacc is final energy, rather than acc-Voltage
                    Eacc = -dE_loss + (Eacc - E.mean())/cos(phir)  # Eacc was final energy, now is acc-volts again [GeV]
                Erf = E + Eacc*np.cos(phir + 2*pi*z/lam)    # energy of each particle from RF shape alone (no wake yet)
                if wakeon:
                    E = Erf + dE_wakes*1e-3              # energy from RF phase and wake added [GeV]
                else:
                    E = Erf                              # energy from RF phase and NO wake added [GeV]

                Ebar = E.mean()                          # mean particle energy [GeV]
                Ebarcuts = Ebar                          # mean energy after cuts - same as Ebar here [GeV]
                d = (E - Ebar)/Ebar                      # relative energy deviation w.r.t. mean energy [ ]
            if abs(cod) == 12:                           # zero-out all energy deviations for diagnostics ONLY
                d = np.zeros(len(d))
                E = E.mean()*np.ones(len(E))


            if abs(cod) == 13:                           # energy feedback with two phase-opposed sections, each of eV0 volts @ crest
                #    ecuts = 0;					    % no dE/E cuts shown on plots
                #    zcuts = 0;					    % no Z cuts shown on plots
                Efin = beamline[j][1]                    # Energy setpoint (goal) [GeV]
                eV0  = beamline[j][2]                    # acc. voltage available at crest for each of two fdbk sections [GeV]
                if eV0==0:
                    print('Warning: Feedback voltage of zero will not correct energy')
                phi1r = beamline[j][3]*pi/180            # acc phase of 1st section (crest=0, low-E head at phi < 0) [deg]
                phi2r = beamline[j][4]*pi/180            # acc phase of 2nd section (crest=0, low-E head at phi < 0) [deg]
                lam   = beamline[j][5]                   # RF wavelength [m]
                if lam<=0:
                    print('Error: RF wavelength cannot be <= 0')
                    return oc
    
                #    iswake  = 0;				    	% no wake plot
                # This can be done analytically...
                #options = OPTIMSET;
                #dphi    = fminsearch('fdbk_fun',0,options,phi1r,phi2r,(Efin-mean(E))/eV0);
                dE = Efin-E.mean()                       # Missing or too much energy
                Vmax = 2.0*eV0*cos((phi1r+phi2r)/2.0)    # The maximum energy gain with given phi1r and phi2r
                if abs(Vmax) < abs(dE):                  # The cavity voltage eV0 is not enough...
                    print('Warning: Feedback cavity voltage is not enough... maximum effort was made.')
                    print('Warning: The residual energy is '+str(abs(dE)-Vmax))
                    if dE*eV0*cos((phi1r+phi2r)/2.0)>0:
                        dphi=-(phi1r-phi2r)/2.0
                    elif dE*eV0*cos((phi1r+phi2r)/2.0)<0:
                        dphi=pi-(phi1r-phi2r)/2.0
                else:                                    # Enough voltage
                    dphi= acos(dE/Vmax)+(phi2r-phi1r)/2.0 # 

                #disp(sprintf('Energy feedback phase set to %8.3f deg',dphi*180/pi))
                En      = E.mean() + eV0*cos(phi1r+dphi) + eV0*cos(phi2r-dphi)
                #if abs(En-Efin)/Efin > 1E-4:
                #    print disp(sprintf('Energy feedback phase maxed out at %8.3f deg',dphi*180/pi))
                E = E  + eV0*np.cos(phi1r+dphi+2*pi*z/lam) + eV0*np.cos(phi2r-dphi+2*pi*z/lam)
                Ebar = E.mean()                          # mean particle energy [GeV]
                Ebarcuts = Ebar                          # mean energy after cuts - same as Ebar here [GeV]
                d = (E-Ebar)/Ebar                        # relative energy deviation w.r.t. mean energy [ ]
            if abs(cod)==15:				 # resistive-wall wakefield (15)
                r0   = beamline[j][1]                    # Beam-pipe radius [m]
                Lng  = beamline[j][2]                    # Beam-pipe radius [m] ??? I guess it's the section length...
                sigc = beamline[j][3]                    # Surface conductivity [(Ohm-m)^-1]
                tau  = beamline[j][4]                    # relaxation time (sec) - if =zero, use DC wake
                rf   = beamline[j][5]                    # rf=1: cylindrical chamber,  rf=2: parallel plates chamber
                if r0<=0:
                    print('Error: Resistive-wall wake cannot be calculated for negative or zero radius')
                    return oc
                if sigc<=0:
                    print('Error: Resistive-wall wake cannot be calculated for negative or zero conductivity')
                    return oc
                if tau<0:
                    print('Error: Resistive-wall wake cannot be calculated for negative relaxation time')
                    return oc
                if rf!=0 and rf!=1:
                    print('Error: Last paremeter in resistive-wall wake must be 0 or 1, for cylindrical or rectangular chambers')
                    return oc
                #    iswake = 1;				% turns wake plot on
                Z0 = 120.0*pi                              # free-space impedance [Ohm]
                s0 = (2.0*r0**2/(Z0*sigc))**(1/3.0)
                zmax = 2.01*(z.max() - z.min())
                
                zpc=np.array(range(0,1001))*zmax/1000.0
                #zpc = 0:(zmax/1000):zmax;
                if tau==0:
                    pcwakeW = self.rw_wakefield(zpc,r0,s0)    # DC-wake
                else:
                    pcwakeW = self.rw_wakefield(zpc,r0,s0,tau,rf)  # AC-wake
                pcwake = [zpc, -pcwakeW]
                [dE_wake,zc_wake] = self.long_wake(z,Lng,pcwake) # calculate dE_wake in MeV from Z-coordinates, acc length, N-particles, etc.
                dE_wakes = np.interp(z,zc_wake,dE_wake)        # inerpolate between wake calc points in bunch to evaluate dE for each e-
                dE_loss = 1e-3*dE_wakes.mean()           # wake loss [GeV]
                E = E + dE_wakes*1e-3                    # energy from RF phase and wake added [GeV]
                Ebar = E.mean()                          # mean particle energy [GeV]
                Ebarcuts = Ebar                          # mean energy after cuts - same as Ebar here [GeV]
                d = (E-Ebar)/Ebar                        # relative energy deviation w.r.t. mean energy [ ]

            if abs(cod) == 26:                           # USER'S ENERGY CUTS (26) - doesn't change Ebar
                #ecuts = 1                                # show dE/E cuts on plots
                d1 = beamline[j][1]                      # minimum dE/E to allow through [ ]
                d2 = beamline[j][2]                      # maximum dE/E "   "     "      [ ]
                if d1 >= d2:                             #  bomb out if max<min (BT-file error)
                    print('Error: Energy cuts (26) must have dE/E_min (col 2) < dE/E_max (col3)')   #in ' fnfm])
                    return oc
                try:
                    # from python2 to python3
                    #ii=np.array(filter((lambda (i,x): x>d1 and x<d2), enumerate(d))).transpose()[0]
                    ii=np.array(filter((lambda i_x: i_x[1]>d1 and i_x[1]<d2), enumerate(d))).transpose()[0]
                except:
                    ii=[]
                Ni = len(ii)                             # count particles left after cuts
                if Ni < 1:
                    #error(sprintf('Energy cuts (26) Emin=%7.4f %% and Emax=%7.4f %% threw out all particles',d1*100,d2*100))
                    print('Error: Energy cuts (26) threw out all particles')
                    return oc
                self.Ne1 = self.Ne1*Ni/self.Nesim                       # rescale N-particles to reflect cuts
                d=d[list(ii)]                            # reduce dE/E array inpose cuts
                z=z[list(ii)]                            # reduce Z array inpose cuts
                E=E[list(ii)]                            # reduce energy array inpose cuts
                Ebarcuts = E.mean()                      # mean energy after cuts [GeV]
                #disp([sprintf('E-cut (26): %6.3e',100*(1-Ni/Nesim)) '% of bunch'])
                print('E-cut (26):'+str(100*(1-float(Ni)/self.Nesim))+'% of bunch cut')
                self.Nesim = Ni                               # reduce number of simulation particles\

            if abs(cod) == 28:                           # Notch collimator for M. Hogan
                d1 = beamline[j][1]                      # minimum dE/E for notch-collimator edge [ ]
                d2 = beamline[j][2]                      # maximum dE/E for notch-collimator edge [ ]
                if d1 >= d2:                             # bomb out if max<min (BT-file error)
                    print('Error: Notch-collimator (28) must have dE/E_min (col 2) < dE/E_max (col3)') # in ' fnfm])
                    return oc
                try:
                    #ii=np.array(filter((lambda (i,x): x<d1 or x>d2), enumerate(d))).transpose()[0]
                    ii=np.array(filter((lambda i_x: i_x[1]<d1 or i_x[1]>d2), enumerate(d))).transpose()[0]
                except:
                    ii=[]
                Ni = len(ii)                             # count particles left after cuts
                if Ni < 1:
                    #error(sprintf('Notch-collimator (28) Emin=%7.4f %% and Emax=%7.4f %% threw out all particles',d1*100,d2*100))
                    print('Error: Notch-collimator (28) threw out all particles')
                    return oc
                self.Ne1 = self.Ne1*Ni/self.Nesim                       # rescale N-particles to reflect cuts
                d=d[list(ii)]                            # reduce dE/E array inpose cuts
                z=z[list(ii)]                            # reduce Z array inpose cuts
                E=E[list(ii)]                            # reduce energy array inpose cuts
                Ebarcuts = E.mean()                      # mean energy after cuts [GeV]
                
                #disp([sprintf('E-cut (26): %6.3e',100*(1-Ni/Nesim)) '% of bunch'])
                print('Notch-collimator (28) cut:'+str(100*(1-float(Ni)/self.Nesim))+'% of bunch cut')
                self.Nesim = Ni                               # reduce number of simulation particles

            if abs(cod) == 29:                           # USER'S ABSOLUTE ENERGY CUTS (29)
                E1 = beamline[j][1]                      # minimum E to allow through [GeV]
                E2 = beamline[j][2]                      # maximum E "   "     "      [GeV]
                if E1 >= E2:                             # bomb out if max<min (BT-file error)
                    print('Error: Absolute energy cuts (29) must have E_min (col 2) < E_max (col 3)')  # in ' fnfm])
                    return oc
                #    i = find(E>E1 & E<E2);			% bomb out if cuts too tight
                try:
                    #ii=np.array(filter((lambda (i,x): x>E1 and x<E2), enumerate(E))).transpose()[0]
                    ii=np.array(filter((lambda i_x: i_x[1]>E1 and i_x[1]<E2), enumerate(E))).transpose()[0]
                except:
                    ii=[]
                Ni = len(ii)                             # count particles left after cuts
                if Ni < 1:
                    #error(sprintf('Absolute energy cuts (29) Emin=%7.4f GeV and Emax=%7.4f GeV threw out all particles',E1,E2))
                    print('Error: Absolute energy cut (29) threw out all particles')
                    return oc
                self.Ne1 = self.Ne1*Ni/self.Nesim                       # rescale N-particles to reflect cuts
                d=d[list(ii)]                            # reduce dE/E array inpose cuts
                z=z[list(ii)]                            # reduce Z array inpose cuts
                E=E[list(ii)]                            # reduce energy array inpose cuts
                Ebarcuts = E.mean()                      # mean energy after cuts [GeV]
                
                #disp([sprintf('E-cut (26): %6.3e',100*(1-Ni/Nesim)) '% of bunch'])
                print('Notch-collimator (28) cut:'+str(100*(1-float(Ni)/self.Nesim))+'% of bunch cut')
                self.Nesim = Ni                               # reduce number of simulation particles

            if abs(cod) == 25:                           # AUTO-APERTURTE ENERGY WINDOW CUTS (25) - doesn't change Ebar
                dw = beamline[j][1]                      # energy width to allow (max and min set to maximize transmission) [ ]
                dspan = d.max() - d.min()                # get full span of dE/E
                if dw >= dspan/2:                        # if E-window is >= 1/2 of full dE/E span...
                    Nbin0 = 1                            # 1-bin needed
                else:                                    # if E-window < 1/2 of full dE/E span...
                    Nbin0 = int(round(dspan/dw))         # bin dE/E initially (>1)
                Nbin0_max = 250                          # reasonable lower limit on 25-code E-width (dE/E full span/Nbin0_max)
                if Nbin0 > Nbin0_max:                    # limit N-bins to reasonable scale
                    #disp(sprintf(['Auto aperture (25) of Ewid=%7.4f%% is just too narrow in ' fnfm],dw*100))
                    #disp(sprintf('...estimate %7.4f%% at minimum acceptable for this beam',100*dspan/Nbin0_max))
                    print('Error: Auto aperture (25) energy width is just too narrow')
                    #error('QUITIING')
                    return oc
                # This algorithm may miss the max density location if more than one peak
                #    if Nbin0 > 1				    % if E-window is < 1/2 dE/E full span...
                #      [Nd0,D0] = hist(d,Nbin0);		% bin all dE/E to find rough location of max density
                #      [Nd0max,iNd0max] = max(Nd0);	% find max bin as rough location of most dense population
                #      dD0 = mean(diff(D0));			% dE/E bin size [ ]
                #      d10 = D0(iNd0max) - dD0;		% rough minimum dE/E of beam core with dw width
                #      d20 = D0(iNd0max) + dD0;		% rough maximum dE/E of beam core with dw width
                #      icore = find(d>d10 & d<d20);	% pointers to core particles
                #      [Nd,D] = hist(d(icore),Nbin);	% re-bin only core dE/E to find more precise integration limits (d1 and d1+dw)
                #    else
                #      [Nd,D] = hist(d,Nbin);		% re-bin all dE/E to find more precise integration limits (d1 and d1+dw)
                #    end
                # Need to bin with dw/2 width and find the maximum among neighbouring pairs
                if Nbin0 > 1:				# if E-window is < 1/2 dE/E full span...
                    [Nd0,D0] = np.histogram(d,2*Nbin0)          # bin all dE/E to find rough location of max density
                    Nd0max=0
                    iNd0max=0
                    for i in range(0,len(Nd0)-1):
                        sNd0=Nd0[i]+Nd0[i+1]
                        if (sNd0>Nd0max):
                            iNd0max=i
                            Nd0max=sNd0
                    
                    #[Nd0max,iNd0max] = max(Nd0);	% find max bin as rough location of most dense population
                    dD0 = D0[2]-D0[0]                   # dE/E bin size [ ], 2 times because of dw/2 binning 
                    d10 = D0[iNd0max+1] - dD0           # rough minimum dE/E of beam core with dw width, the edge of the second bin is the centre of the pair bins
                    d20 = D0[iNd0max+1] + dD0           # rough maximum dE/E of beam core with dw width
                    #icore = find(d>d10 & d<d20)         # pointers to core particles
                    dcore=filter((lambda x: x>d10 and x<d20), d)
                    [Nd,D] = np.histogram(dcore,self.Nbin)           # re-bin only core dE/E to find more precise integration limits (d1 and d1+dw)
                else:
                    [Nd,D] = np.histogram(d,self.Nbin)               # re-bin all dE/E to find more precise integration limits (d1 and d1+dw)

                dD = D[1] - D[0]                        # dE/E bin size [ ]
                nj = min(np.array([int(round(dw/dD)),self.Nbin]))    # number of bins which approx. add up to the dw width wanted (max allowed = Nbin)
                if nj < 2:                              # bomb if window to narrow
                    #      error(sprintf(['Auto aperture (25) of Ewid=%7.4f%% is too narrow in ' fnfm],dw*100))
                    print('Auto aperture (25) of energy width is too narrow')
                    
                A = np.zeros(self.Nbin-nj+1)                    # initialize area array
                
                for jj in range(0,self.Nbin-nj+1):
                    A[jj] = sum(Nd[jj:(jj+nj)])         # find transmission for each possible integration 1st-limit (const width)
                Amax=A.max()
                iAmax=A.argmax()

                #    [Amax,iAmax] = max(A);			% find set of bins with most beam
                if iAmax == self.Nbin-nj:                    # if most beam is near the high energy edge of dE/E distribution...
                    #derr = 0                           # bias window 1/2-bin low so that the high-E dense are is not cut off in matlab
                    derr= -dD/2                         # in python
                elif iAmax == 1:                        # if most beam is near the low energy edge of dE/E distribution...
                    #derr = dD                          # bias window 1/2-bin high so that the low-E dense are is not cut off in matlab
                    derr = dD/2                         # in python
                else:                                   # if dense portion of beam is not near edge...
                    #derr = dD/2                        # no window bias (1/2-bin shift is necessary for accuracy) in matlab
                    derr = 0                            # in python
                d1 = D[iAmax] - derr                    # find first integration limit (low energy cut) for max transmission
                d2 = d1 + dw                            # find high energy cut


                #ii=np.array(filter((lambda (i,x): x>d1 and x<d2), enumerate(d))).transpose()[0]
                ii=np.array(filter((lambda i_x: i_x[1]>d1 and i_x[1]<d2), enumerate(d))).transpose()[0]
                Ni = len(ii)                             # count particles left after cuts
                if Ni < 1:
                    #error(sprintf('Auto aperture (25), Ewid=%7.4f%% sets Emin=%7.4f%%, Emax=%7.4f%% and throws out all particles',dw*100,d1*100,d2*100))
                    print('Error: Auto aperture (25) threw out all particles')
                    return oc
                self.Ne1 = self.Ne1*Ni/self.Nesim                       # rescale N-particles to reflect cuts
                d=d[list(ii)]                            # reduce dE/E array inpose cuts
                z=z[list(ii)]                            # reduce Z array inpose cuts
                E=E[list(ii)]                            # reduce energy array inpose cuts
                Ebarcuts = E.mean()                      # mean energy after cuts [GeV]
                #disp([sprintf('E-window-cut (25): %6.3f',100*(1-Ni/Nesim)) '% of bunch'])  
                print('E-window (25):'+str(100*(1-float(Ni)/self.Nesim))+'% of bunch cut')
                self.Nesim = Ni                               # reduce number of simulation particles

            if abs(cod) == 27:                           # USER'S constant-dN/N dE/E cuts (27)
                #    zcuts = 1;					            % show dE/E-cuts on plots
                dN_N = beamline[j][1]                    # fraction of max-dE/E-amplitude particles to cut [ ]
                if dN_N<=0 or dN_N>=1:
                    print('Error: dN_N (col 2) in constant-dN/N dE/E cuts (27) should be >0 and <1')
                    return oc

                no_charge_loss = beamline[j][2]		 # if==1, no real charge cut intended, just better binning
                
                ds = np.sort(abs(d-d.mean()))               # sort the absolute value of dE/E values (min to max)
                N1 = int(round(self.Nesim*(1.0-dN_N)))   # throw out last N1 particles
                dcut = ds[N1-1]

                try:
                    #ii=np.array( filter( (lambda (i,x): abs(x-d.mean())<dcut), enumerate(d))).transpose()[0]
                    ii=np.array( filter( (lambda i_x: abs(i_x[1]-d.mean())<dcut), enumerate(d))).transpose()[0]
                except:
                    print('Error: the fraction to cut in constant-dN/N dE/E cuts (27) is too small.')
                    return oc
                         
                Ni = len(ii)                             # count particles left after cuts
                d=d[list(ii)]                            # reduce dE/E array inpose cuts
                z=z[list(ii)]                            # reduce Z array inpose cut
                E=E[list(ii)]                            # reduce energy array inpose cuts
                if no_charge_loss==0:                    # if real charge cut intended...
                    self.Ne1 = self.Ne1*float(Ni)/self.Nesim                   # ...rescale N-particles to reflect cuts
                Ebarcuts = E.mean()                      # mean energy after cuts [GeV]
                #    disp([sprintf('Const-dN/N dE/E-cut (27): %6.3f',100*(1-Ni/Nesim)) '% of bunch'])  
                print('Const-dN/N dE/E-cut (27): '+str(100*(1-float(Ni)/self.Nesim))+'% of bunch cut')
                self.Nesim = Ni                          # reduce number of simulation particles

            if abs(cod) == 35:                           # narrow in on small z-width
                zw1 = beamline[j][1]                     # the full width in z to find peak current in [mm]
                zw2 = beamline[j][2]                     # the full width in z to find peak current in [mm]
                if zw1<=0 or zw2<=0:
                    print('Error: z windows (col 2,3) in z cut (35) should be >0')
                    return oc
                # z is not in [m] ???? Assuming it is in [m] in the following...
                zbar = z.mean()                          # z mean [mm] ( [m] )
                dz = z - zbar                            # subtract z mean [mm] ( [m] )

                #i  = find(abs(dz)<zw1/2);				% find all particles in this width around the mean
                try:
                    #ii=np.array(filter((lambda (i,x): abs(x)<zw1*1e-3/2), enumerate(dz))).transpose()[0]
                    ii=np.array(filter((lambda i_x: abs(i_x[1])<zw1*1e-3/2), enumerate(dz))).transpose()[0]
                except:
                    print('Error: The first window (col 2) in z cut (35) is too narrow')
                    return oc
                
                # Again, this may miss the peak...
                #[Nz,Z] = hist(dz(ii),Nbin);				% bin dz in this narrow window to fine peak in current
                #[mx,ix] = max(Nz);						% find maximum current in this window
                [Nz,Z] = np.histogram(dz[list(ii)],2*self.Nbin)             # bin all dE/E to find rough location of max density
                ix=0
                mx=0
                for i in range(0,2*self.Nbin-1):
                    sNz=Nz[i]+Nz[i+1]
                    if (sNz>mx):
                        ix=i 
                        mx=sNz
                #i  = find(abs(dz-Z(ix))<zw2/2);			% find all particles in this width around the mean
                try:
                    #ii=np.array(filter((lambda (i,x): abs(x)<zw2*1e-3/2), enumerate(dz-Z[ix+1]))).transpose()[0] # hist() in python returns edges, so ix+1... 
                    ii=np.array(filter((lambda i_x: abs(i_x[1])<zw2*1e-3/2), enumerate(dz-Z[ix+1]))).transpose()[0]

                except:
                    print('Error: The second window (col 3) in z cut (35) is too narrow')
                    return oc

                Ni = len(ii)                             # count particles left after cuts
                self.Ne1 = self.Ne1*float(Ni)/self.Nesim        # rescale N-particles to reflect cuts
                d=d[list(ii)]                            # reduce dE/E array inpose cuts
                z=z[list(ii)]                            # reduce Z array inpose cut
                E=E[list(ii)]                            # reduce energy array inpose cuts
                Ebarcuts = E.mean()                      # mean energy after cuts [GeV]
                #    disp([sprintf('z-window-cut (35): %6.3f',100*(1-Ni/Nesim)) '% of bunch'])  
                print('z-window-cut (35): '+str(100*(1-float(Ni)/self.Nesim))+'% of bunch')
                self.Nesim = Ni                          # reduce number of simulation particles

            if abs(cod) == 36:                           # USER'S Z-CUTS (36)
                #    zcuts = 1;					            % show Z-cuts on plots
                z1 = beamline[j][1]                      # minimum Z to allow through [m]
                z2 = beamline[j][2]                      # maximum Z "   "     "      [m]
                if z1 >= z2:				 # bomb out if max<min (BT-file error)
                    #      error(['Z-cuts (36) must have Z_min (col 2) < Z_max (col3) in ' fnfm])
                    print('Error: Z-cuts (36) must have Z_min (col 2) < Z_max (col 3)')
                    return oc
                try:
                    #ii=np.array(filter((lambda (i,x): x>z1 and x<z2), enumerate(z))).transpose()[0]
                    ii=np.array(filter((lambda i_x: i_x[1]>z1 and i_x[1]<z2), enumerate(z))).transpose()[0]
                except:
                    print('Error: The z window (col 2,3) in z cut (36) is too narrow or not overlapping with beam.')
                    return oc
                Ni = len(ii)                           # count particles left after cuts
                if Ni < 1:
                    #      error(sprintf('Z-cuts (36) Zmin=%7.4f mm and Zmax=%7.4f mm threw out all particles',z1*1E3,z2*1E3))
                    print('Error: Z-cuts (36) threw out all particles')
                    return oc
                self.Ne1 = self.Ne1*float(Ni)/self.Nesim                       # rescale N-particles to reflect cuts
                d=d[list(ii)]                            # reduce dE/E array inpose cuts
                z=z[list(ii)]                            # reduce Z array inpose cut
                E=E[list(ii)]                            # reduce energy array inpose cuts
                Ebarcuts = E.mean()                      # mean energy after cuts [GeV]
                print('Z-cut (36): '+str(100*(1-float(Ni)/self.Nesim))+'% of bunch')
                self.Nesim = Ni                          # reduce number of simulation particles
            if abs(cod) == 37:                           # USER'S constant-dN/N z cuts (37)
                dN_N = beamline[j][1]                    # fraction of max-dE/E-amplitude particles to cut [ ]
                no_charge_loss = beamline[j][2]		 # if==1, no real charge cut intended, just better binning
                
                if dN_N<=0 or dN_N>=1:
                    print('Error: dN_N (col 2) in constant-dN/N z cuts (37) should be >0 and <1')
                    return oc
                zs = np.sort(abs(z-z.mean()))               # sort the absolute value of dE/E values (min to max)
                N1 = int(round(self.Nesim*(1.0-dN_N)))       # throw out last N1 particles
                zcut = zs[N1-1]
                try:
                    #ii=np.array( filter( (lambda (i,x): abs(x)<zcut), enumerate(z))).transpose()[0]
                    ii=np.array( filter( (lambda i_x: abs(i_x[1])<zcut), enumerate(z))).transpose()[0]
                except:
                    print('Error: the fraction to cut in constant-dN/N z cuts (37) is too small.')
                    return oc
                Ni = len(ii)                             # count particles left after cuts
                d=d[list(ii)]                            # reduce dE/E array inpose cuts
                z=z[list(ii)]                            # reduce Z array inpose cut
                E=E[list(ii)]                            # reduce energy array inpose cuts
                if no_charge_loss==0:                    # if real charge cut intended...
                    self.Ne1 = self.Ne1*float(Ni)/self.Nesim                   # ...rescale N-particles to reflect cuts
                Ebarcuts = E.mean()                      # mean energy after cuts [GeV]
                #    disp([sprintf('Const-dN/N dE/E-cut (27): %6.3f',100*(1-Ni/Nesim)) '% of bunch'])  
                print('Const-dN/N z (37): '+str(100*(1-float(Ni)/self.Nesim))+'% of bunch')
                self.Nesim = Ni                          # reduce number of simulation particles
            if abs(cod) == 44:                           # add a temporal modulation
                mod_amp = beamline[j][1]                 # modulation rel. amplitude (typically 0.02 - 0.05)
                mod_lam = beamline[j][2]                 # modulation wavelength [m]
                z = z + mod_amp*mod_lam/2/pi*np.cos(2*pi*z/mod_lam)
                self.sigz = z.std()                      # re-calc bunch length for next possible pass through wake calculations
            if abs(cod) == 45:                           # add an energy modulation
                mod_amp = beamline[j][1]                 # energy modulation relative amplitude (e.g., 0.001 for 0.1%) [ ]
                mod_lam = beamline[j][2]                 # energy modulation wavelength [m]
                E = E*(1 + mod_amp*np.sin(2*pi*z/mod_lam)) # modulate energy
                Ebar = E.mean()                          # mean particle energy [GeV]
                Ebarcuts = Ebar                          # mean energy after cuts - same as Ebar here [GeV]
                d = (E - Ebar)/Ebar                      # relative energy deviation w.r.t. mean energy [ ]
            if abs(cod) == 6:                            # BUNCH COMPRESSION (R56/m, T566/m, E/GeV, U5666/m)
                print('Bunch Compressor (Code 6): ',beamline[j][6])
                R56  = beamline[j][1]                    # R56 value [m]
                T566 = beamline[j][2]                    # T566 value [m] (=-3*R56/2 for non-quad system)
                U5666= beamline[j][4]                    # U5666 value [m] (=2*R56 for non-quad system)
                E56  = beamline[j][3]                    # Nominal energy of compressor [GeV]
                if E56 < 0:
                    E56=E.mean()
                    print('Autophasing: Setting Energy to',E56)
                if E56 < 0.020:                          # need positive, reasonable nominal R56-energy [GeV]
                    #      disp(sprintf(['WARN: Compressor section (6) of R56=%7.4f m has nominal-energy too small (not ultra-relativistic) in ' fnfm],R56))
                    print('WARNING: Compressor section (6) has nominal-energy too small (not ultra-relativistic)')
                dd = (E-E56)/E56                         # relative energy error w.r.t. nominal compressor energy
                z = R56*dd + T566*dd**2 + U5666*dd**3 + z  # compress or anti-compress bunch [m]
                self.sigz = z.std()                      # re-calc bunch length for next possible pass through wake calculations
            if abs(cod) == 7:                            # BUNCH COMPRESSION CHICANE (R56/m, E/GeV [T566=-1.5*R56, U5666=2*R56])
                print('Simple bunch Compressor chicane (Code 7): ', beamline[j][6])
                R56  = beamline[j][1]                    # R56 value [m]
                dR56_R56 = beamline[j][3]                # relative R56 jitter 
                eps = dR56_R56            
                if R56>0:
                    #      error('R56 for chicane is always <0...  quitting')
                    print('Error: R56 for chicane (7) is always <0...  quitting')
                    return oc
                if dR56_R56>0:
                    print('Switched-on jitter on R56 in chicane')
                T566 = -1.5*R56                          # T566 value [m]
                U5666=  2.0*R56                          # U5666 value [m]
                E56  = beamline[j][2]                    # Nominal energy of compressor [GeV]
                if E56 < 0:
                    E56=E.mean()
                    print('Autophasing: Setting Energy to',E56)

                if E56 < 0.020:                          # need positive, reasonable nominal R56-energy [GeV]
                    #      error(sprintf(['Chicane section (7) of R56=%7.4f m has nominal-energy too small (not ultra-relativistic) in ' fnfm],R56))
                    print('WARNING: Compressor section (7) has nominal-energy too small (not ultra-relativistic)')
                dd = (E-E56)/E56                         # relative energy error w.r.t. nominal compressor energy
                z = R56*(1-eps)*dd + T566*(1-eps)*dd**2 + U5666*(1-eps)*dd**3 + z  + eps*R56/2 # compress or anti-compress bunch [m]
                self.sigz = z.std()                      # re-calc bunch length for next possible pass through wake calculations
            if abs(cod) == 8:                            # octupole ?
                #	disp('WARN: Octupole element is not reliable yet...')
                print('WARNING: Octupole element is not reliable yet...')
                K3   = beamline[j][1]                    # octupole MAD k-value [m^-4]
                Enom = beamline[j][2]                    # Nominal energy in octupole [GeV]
                Leff = beamline[j][3]                    # effective magnetic length of octupole [m]
                eta  = beamline[j][4]                    # dispersion in octupole [m]
                U5666= K3*Leff*eta**4.0/6.0
                dd   = (E-Enom)/Enom                     # relative energy error w.r.t. nominal compressor energy
                z    = U5666*dd**3 + z                   # distort bunch as per octupole, assuming in chicane center [m]
            if abs(cod) == 22:                           # INCOHERENT ENERGY SPREAD ADDITION
                id_rms = beamline[j][1]                  # rms incoherent relative energy spread to be added in quadrature [ ]
                d = d + id_rms*np.array([gauss(0.0, 1.0) for x in range(0,self.Nesim)]) 
                E = Ebar*(1 + d)                         # load energy array [GeV]


            if (cod<0) or (abs(cod) == 99) or (abs(cod) == 2):
                BLj.append(beamline[j])
                zposj.append(z)
                dE_Ej.append(d)
                Ebarj.append(E.mean())
                z_barj.append(z.mean())
                zmm = z*1e3                                # convert to [mm]
                dpct = d*100                               # convert to [%]
                [Nz1,Z] = np.histogram(zmm,self.Nbin);	           # bin the Z-distribution
                dZ = Z[1]-Z[0]                             # Z bin size [mm]
                Z = Z + dZ/2.0                             # From bin edge to center
                Z = Z[0:len(Z)-1]                          # and drop the last one
                [FW, i1, i2] = self.FWHM(Z,Nz1,0.5)
                ZFWmmj.append(FW)                          # calc. Z-FWHM [mm]
                
                [amp, mu, sig, sky] = self.gauss_fit(zmm)
                sigzGj.append(sig)

                [Nd,D]  = np.histogram(dpct,self.Nbin)          # bin the dE/E-distribution
                dD = D[1]-D[0]                             # D bin size [mm]
                D = D + dD/2.0                             # From bin edge to center
                D = D[0:len(D)-1]                          # and drop the last one
                [FW, i1, i2] = self.FWHM(D,Nd,0.5)
                dFWpctj.append(FW)                         # calc. dE/E-FWHM [%]
                Ebarcutsj.append(Ebarcuts)

                [amp, mu, sig, sky] = self.gauss_fit(dpct)
                sigEGj.append(sig)                         # gaussian fit sigma_dE/E0 [%]
                I  = Nz1*(self.Ne1/1e10/self.Nesim)*self.elec*self.cspeed/dZ   # convert N to peak current [kA] ... it should be called like slice-current...
                I_pkj.append(I.max())                      # peak current in bunch center [kA]
                #I_pkfj(jc) = max(If);
                I_pkfj.append(amp*(self.Ne1/1e10/self.Nesim)*self.elec*self.cspeed/dZ) # peak current from fit

                fcutj.append(1 - self.Ne1/Ne0)		   # fraction of particles cut [ ]
            if abs(cod) == 99:
                break


        #Packing the output for shipping
        outdict['BLj']=BLj
        outdict['zposj']=zposj
        outdict['dE_Ej']=dE_Ej
        outdict['Ebarj']=Ebarj
        outdict['z_barj']=z_barj
        outdict['ZFWmmj']=ZFWmmj
        outdict['sigzGj']=sigzGj
        outdict['dFWpctj']=dFWpctj
        outdict['Ebarcutsj']=Ebarcutsj
        outdict['sigEGj']=sigEGj
        outdict['I_pkj']=I_pkj
        outdict['I_pkfj']=I_pkfj
        outdict['fcutj']=fcutj
        
        oc=dummyClass(outdict)
    
        end_time = gmtime()
        print(' ')
        print('LiTrack (tracking) ended: '+asctime(end_time))
        print(' ')


        self.output=oc

        return oc # The output can be replaced by outdict easily

        
