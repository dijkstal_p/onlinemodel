import math
import numpy as np

from OMAppTemplate import ApplicationTemplate



#--------
#  Sub Module for PyLiTrack interface to generate the lattice from the online model using the writeLine method
#  This class will be inherited by OMLiTrack

#---------------------------
#      RF Class

class AccuLinac:
    def __init__(self):
        self.AccRe=0
        self.AccIm=0
        self.Band=''
        self.AccL=0
        self.Accf0=0
        self.wakefunc={'S':1,'C':2,'X':3}
        self.empty=True


    def flush(self,output=0): 
        if self.empty:
            return False
        #dEacc=math.sqrt(self.AccRe*self.AccRe+self.AccIm*self.AccIm)/1e9
        dEacc=math.sqrt(self.AccRe*self.AccRe+self.AccIm*self.AccIm)/1e3 # Gradient is MV/m since 29.06.2015
        phi=-math.atan2(self.AccIm,self.AccRe)*180/math.pi
        line=[11*(1-2*output),dEacc,phi,3e8/self.Accf0,self.wakefunc[self.Band],self.AccL, self.Band+"-Linac"] 
#        line=[11*(1-2*output),dEacc,phi,3e8/self.Accf0,0                       ,self.AccL, self.Band+"-Linac"] 
        self.__init__()
        return line


    def add(self,ele):
        
        ret=False
        if ele.Band!=self.Band:
            ret=self.flush(0)
        self.AccRe+=ele.Gradient*math.sin(ele.Phase*math.asin(1)/90)*ele.getLength()
        self.AccIm+=ele.Gradient*math.cos(ele.Phase*math.asin(1)/90)*ele.getLength()
        self.AccL+=ele.getLength()
        self.Band=ele.Band
        self.Accf0=ele.Frequency          
        self.empty=False
        return ret
    
#-------------------
#       Chicane class

class AccuR56:
    def __init__(self,dev=0):
        self.dev=dev   # scaling of deflection angle (aka the energy dependence on the bend angle) 
                       # to calculate the trajectories for R56, T566 and U5666
        self.empty=True
        self.angle=[0,0,0,0,0]
        self.L=[0,0,0,0,0]
        self.z=[0,0,0,0,0]
        self.x=[0,0,0,0,0]
        self.R56=0
        self.T566=0
        self.U5666=0
        self.E=0
        self.Name=""
        self.BCName=""

    def reset(self):
        self.__init__(self.dev)


    def calc(self):
        dev=[-2*self.dev,-self.dev,0,self.dev,2*self.dev]
        Lp=np.array(self.L)-self.L[2]
        ex=math.sin(self.angle[2])
        ez=math.cos(self.angle[2])
        for i in range(0,5):
            dx=self.x[i]-self.x[2]
            dz=self.z[i]-self.z[2]
            d=dx*ex+dz*ez
            Lp[i]+=d
        return np.polyfit(dev,Lp,3)

    
    def flush(self,output):
        if (self.empty==True):
            return False 
        p=self.calc()
        U5666=p[0]
        T566 =p[1]
        R56  =p[2]
        line =[6*(1-2*output),R56-self.R56,T566-self.T566,self.E,U5666-self.U5666,0,self.Name]
        self.R56=R56
        self.T566=T566
        self.U5666=U5666
        return line


    def add(self,ele):


        self.empty=False # start of new chicane            


        if isinstance(ele,Dipole):
            self.Name=ele.Name
            self.BCName=ele.BC
            #self.E=ele.p0/1e9
            self.E=ele.p0/1e3 # ele.p0 is MeV since 29.06.2015 
            angle0=ele.angle
            Lres=ele.getResLength()

            if (angle0==0):
                self.advanceDrift(Lres)
                return

            Lpath=ele.getLength()
            Ld=0.5*(Lres-Lpath)
            Lb=ele.Length

            self.advanceDrift(Ld)
            for i in range(0,5):
                
                angle=angle0/(1+(i-2)*self.dev)*math.pi/180
                R=Lb/math.sin(angle)       # bending radious 
                # direction of motion
                ex=math.sin(self.angle[i])  
                ez=math.cos(self.angle[i])
                # vector to origin of rotation/bending
                Rx= ez*R
                Rz=-ex*R
                # rotation matrix
                rc=math.cos(angle)
                rs=math.sin(angle)

                # rotating the radious vector and subtracting the starting point
                dRx= Rx*rc+Rz*rs-Rx
                dRz=-Rx*rs+Rz*rc-Rz
                # add the new location
                self.x[i]-=dRx
                self.z[i]-=dRz
                # adding path length
                self.L[i]+=R*angle
                self.angle[i]+=angle

            self.advanceDrift(Ld)    
            return    

        # drift

        L=0    
        if isinstance(ele,float) or isinstance(ele,int):
            L=ele
        else:
            L=ele.getResLength()
        self.advanceDrift(L)
        return



    def advanceDrift(self,L):
        for i in range(0,5):
#            r=math.cos(self.angle[i]-self.angle[2])    # deviation fromn reference angle
            r=math.cos(self.angle[2])/math.cos(self.angle[i])
            Ladv=L*r
            self.L[i]+=Ladv                
            self.x[i]+=math.sin(self.angle[i])*Ladv
            self.z[i]+=math.cos(self.angle[i])*Ladv

#---------------------------------------------------------------
#

class LiTrackLattice(ApplicationTemplate):
    
    def __init__(self):
        ApplicationTemplate.__init__(self)


        self.beamline=[]   # holds the beamline in litrack format.

        self.rawbeamline=[]  # temporary beamline to mostly filter out unneeded elements, e.g. drifts
    
        self.RF=AccuLinac()   # module to calculate the linac
        self.R56=AccuR56(1e-3) # module to calculate the R56 and higher terms
        self.autophase=False   # if true than BC stages will be set to mean energfy in Litrack
        self.alloutput=False   # forces output in all stages



    def demandMapID(self):
        'OMLiTrack does not request Map ID to Layout manger'
        return 0

#----------------
#  the element specific format

        
    def writeLine(self,line,seq):
        if len(line.Name)!=7:

            # step one find the BC sections

            BC={}
            for idx in range(0,len(self.rawbeamline)):
                ele=self.rawbeamline[idx]
                if (ele["Type"]=="Bend"):
                    if ele['Element'].BC in BC:
                        BC[ele['Element'].BC].append(idx)
                    else:
                        BC[ele['Element'].BC]=[idx]
                        
            # get BC beamlines   
                        
            R56beamline={}     

            for key in BC.keys(): 
                i0=min(BC[key])
                i1=max(BC[key])
                retacc=[]
                self.R56.reset()
                for i in range(i0,i1+1):
                   ele=self.rawbeamline[i]
                   ele['Type']=key
                   if 'Output' in ele:
                       ret=self.R56.flush(-1)
                       if ret!=False:
                           ret[-1]=ele['Tag']
                           retacc.append(ret)
                           continue
                   if 'L' in ele:
                       self.R56.advanceDrift(ele['L'])
                       continue
                   self.R56.add(ele['Element'])

                ret=self.R56.flush(0)
                if ret!=False:
                   retacc.append(ret)
                R56beamline[key]=retacc


            self.beamline=[[-1,0,0,0,0,0,'Start']]    

            for ele in self.rawbeamline:
                if ele['Type'] in R56beamline:
                    ret=self.RF.flush(0)
                    if ret!=False:
                        self.beamline.append(ret)
                    for line in R56beamline[ele['Type']]:
                        self.beamline.append(line)
                    del R56beamline[ele['Type']]                  
                    continue
                if ele['Type']=='Output':
                    ret=self.RF.flush(1)
                    if ret==False:
                        ret=[-1,0,0,0,0,0,ele['Tag']]
                    else:
                        ret[-1]=ele['Tag']
                    self.beamline.append(ret)    
                    continue

                if ele['Type']=='RF':
                    ret=self.RF.add(ele['Element'])
                    if ret!=False:
                        self.beamline.append(ret)
            ret=self.RF.flush(0)
            if ret!=False:
                self.beamline.append(ret)

                        
            self.beamline.append([-99,0,0,0,0,0,'End'])    
            
            self.rawbeamline=[]
   
            if self.alloutput:
                for ele in self.beamline:
                    ele[0]=-abs(ele[0])

            if self.autophase:
                for ele in self.beamline:
                    if abs(ele[0])==6:
                        ele[3]=-abs(ele[3])
                    

    
    def writeDrift(self,ele):
        if isinstance(ele,float) or isinstance(ele,int):
            self.rawbeamline.append({'Type':'Drift','L':ele})
        else:
            self.rawbeamline.append({'Type':'Drift','L':ele.getResLength()})


    def writeDiagnostic(self,ele):
        self.writeDrift(ele)    
        if ele.enable==0:
            return
        self.rawbeamline.append({'Type':'Output','Tag':ele.Name})                            


    def writeRF(self,ele): 

        if ele.Gradient==0:
            self.writeDrift(ele)
            return

        if 'TDS' in ele.Name:
            self.rawbeamline.append({'Type':'Output','Tag':ele.Name})                    
        else:
            self.rawbeamline.append({'Type':'RF','Element':ele})


    def writeBend(self,ele):
        #if (ele.angle==0) or (ele.__dict__.has_key("BC")==False):
        if (ele.angle==0) or ('BC' not in ele.__dict__):
            self.writeDrift(ele)
            return

        

        self.rawbeamline.append({'Type':'Bend','Element':ele})
        

    def writeQuadrupole(self,ele):     
        self.writeDrift(ele)
   
    def writeCorrector(self,ele): 
        self.writeDrift(ele)

    def writeSextupole(self,ele):       
        self.writeDrift(ele)

    def writeUndulator(self,ele): 
        self.writeDrift(ele)

    def writeMarker(self,ele):
        if ele.Length>0:
            self.writeDrift(ele)

    def writeSolenoid(self,ele):
        if 'MADoverlap' in ele.__dict__:
            if ele.MADoverlap==1:
                return 
        self.writeDrift(ele)
        




