import re
import numpy as np

class FacilityAccess:


    def listElement(self, word=None, NoE=0):
        # Return a list of elements that includes word
        # Wild card (*) can be used 
        # NoE stands for Name or Element, switching the output
        # 0: list of Element names, 1: list of element instances. 
        if word==None:
            return
        elif word=='*':
            allRequested=1
        elif len(word)==0:
            return []
        else:
            word=word.upper()
            word=word.split('*')
            allRequested=0

        ElementList=[]
        for p in self.PartsList:
            for sec in p[0].Element:
                for ele in sec.Element:
                    requested=1
                    for w in word:
                        if w not in ele.Name:
                            requested=allRequested
                    if requested:
                        if NoE:
                            ElementList.append(ele)
                        else:
                            if ele.Type!='Corrector':
                                ElementList.append(ele.Name)
                            if ('cor' in ele.__dict__) or ('corx' in ele.__dict__):
                                ElementList.append(ele.Name.replace(ele.Name[8:12],'MCRX'))
                            if ('cory' in ele.__dict__):
                                if (ele.Name[8:12]=='MKAC') or  (ele.Name[8:12]=='MKDC'):
                                    ElementList.append(ele.Name)
                                else:
                                    ElementList.append(ele.Name.replace(ele.Name[8:12],'MCRY'))

        return ElementList
        
    def listSection(self, word=None, NoE=0):
        # Return a list of elements that includes word
        # Wild card (*) can be used 
        # NoE stands for Name or Element, switching the output
        # 0: list of Element names, 1: list of line instances. 
        if word==None:
            return
        elif word=='*':
            allRequested=1
        elif len(word)==0:
            return []
        else:
            word=upper(word)
            word=word.split('*')
            allRequested=0

        SectionList=[]
        for p in self.PartsList:
            for sec in p[0].Element:
                requested=1
                for w in word:
                    if w not in sec.Name:
                        requested=allRequested
                if requested:
                    if NoE:
                        SectionList.append(sec)
                    else:
                        SectionList.append(sec.Name)

        return SectionList
        



    def getElement(self,name):
        name=name.upper() 
        if name in self.ElementDB:
            return self.ElementDB[name]
        else:
            return None

    def getSection(self,name):
        name=name.upper() 
        if name in self.SectionDB:
            return self.SectionDB[name]
        else:
            return None



    def setGroup(self,dn,field,values):
        # dn is a list of element names. It can be obtained from OMEpicsChannel
        for i in range(0,len(dn)):
            n=dn[i]
            ele=self.ElementDB[n]
            if field in dir(ele):
                ele.__dict__.update({field:values[i]})
            else:
                print('Error due to unregistered Channel:',ele,field)
                
 
    def getGroup(self,dn,field): 
        values=[]
        for i in range(0,len(dn)):
            n=dn[i]
            ele=self.ElementDB[n]
            if field in dir(ele):
                values.append(ele[field])
            else:
                print('Error due to unregistered Channel:',ele,field)
                
        return values



    def setRegExpElement(self,section,device,field,value):
        myRe=re.compile(section+'.*'+device)
        for key in self.ElementDB.keys():
            if (myRe.match(key)):
                ele=self.ElementDB[key]
                if field in ele.__dict__.keys():
                    if (field=='angle'):
                        ele.__dict__[field]=np.sign(ele['design_angle'])*abs(value)  # this is needed to set collectively a bunch compressor angle without flipping signs
                    else:    
                        ele.__dict__[field]=value



    def getRegExpElement(self,section,device,field):
        val=[]
        myRe=re.compile(section+'.*'+device)
        for key in self.ElementDB.keys():
            if (myRe.match(key)):
                ele=self.ElementDB[key]
                if field in ele.__dict__.keys():
                    val.append(ele[field])
        return val            


#-------------------------
#   routines from former Energy Manager

    def EnergyAt(self,ele):
        if isinstance(ele,str):
            name=ele
        else:
            name=ele.Name
        if name in self.EM.Energy.keys():
            return self.EM.Energy[name]
        else:
            print(name+' is not found in Energy manager of Module Facility!')
            return [0.0,0.0]

    def forceEnergyAt(self,elename,Ei):

        # force an update of the energy profile
        self.writeFacility(self.EM)

        if Ei<0:
            # The initial energy should be positive
            return
        try:
            # Check if elename exists
            E0=self.EM.Energy[elename][0]
            dE=Ei-E0
#            dE=Ei
#            print('energy change',elename,Ei,E0)
#            self.EM.Energy[elename][0]=Ei
        except:
            return

        # First compute the energy with the present gradient and phase
        # Note that the energy at the elements upstream of the given initial element is not touched.

        for k in self.EM.Energy.keys():
            if len(self.EM.Energy[k])==2:
                self.EM.Energy[k]=[self.EM.Energy[k][0]+dE,self.EM.Energy[k][1]]                        
            elif len(self.EM.Energy[k])==3:
                self.EM.Energy[k]=[self.EM.Energy[k][0]+dE,self.EM.Energy[k][1],self.EM.Energy[k][2]]


