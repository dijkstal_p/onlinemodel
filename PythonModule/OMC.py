import sys
#sys.path.append('/afs/psi.ch/intranet/SF/Applications/Development/OnlineModel/lib')
#sys.path.append('/afs/psi.ch/intranet/SF/Applications/Development/OnlineModel/scripts/VA/')
from OMType import *
from OMLayout import *
from OMEnergyManager import *
from OMEpicsChannel import *
from OMSwissFELmagnet import *
from OMMadx import *
from string import *

class OMC:
    def __init__(self):
        'Initialization will be done explicitly'
        
        self.SF=None
        self.EM=None
        self.EC=None
        self.SM=None
        self.SR=None
        self.PM=None
        self.LT=None

    def initializeLayout(self,s1=3):
        # s1 is the switch used when initialize Facility container (see OMType)
        # prefix is your prefix of Epics channels. 'VA' for Virtual accelerator (control room).
        Layout=SwissFEL()
        self.SF=FacilityContainer(Layout,s1)

        self.EM=EnergyManager()
        self.SF.writeFacility(self.EM)

        #self.SM=SwissFELMagnet()
        #self.SF.writeFacility(self.SM)

        
    def initializeEC(self,prefix,inlist=[]):
        # inlist is not properly setup in C... fix in python...
        newlist=[]
        for i in range(0,len(inlist)):
            newlist.append(inlist[i][0])
        # prefix is your prefix of Epics channels. 'VA' for Virtual accelerator (control room).
        self.EC=EpicsChannel(prefix)
        self.EC.switchOutput()
        self.SF.writeFacility(self.EC,1)
        #self.EC.createHandle(newlist)

    def getDeviceList(self,key):
        l=self.EC.getDeviceList(key)
        for i in range(0,len(l)):
            l[i]=l[i].replace('.','-')
        i=len(l)
        
        return i,l


    def getChannelList(self,key):
        skey=key.split(':')
        if len(skey)!=2:
            return 0,[]
        l=self.EC.getDeviceList(skey[0])
        for i in range(0,len(l)):
            l[i]=l[i].replace('.','-')+':'+skey[1]
        i=len(l)
        return i,l

    def initFunction(self, suff):
        i=1
 
        Layout=SwissFEL()
        self.SF=FacilityContainer(Layout,1)
        self.EC=EpicsChannel(suff)
        self.EC.switchOutput()
        self.SF.writeFacility(self.EC,1)
        
        # Extra grouping for RFsystem
        rf=self.EC.getDeviceList('RFsystem')

        for r in rf:
            chs=[]
            for atr in self.EC.RFsysAtr:
                m=r.replace('.','-')+':'+atr
                chs.append(m)
            self.EC.ch[r]=chs
        
        
        N=[]
        for l in self.EC.ch.values():
            N.append(len(l))

        # keys() and values() are list in python2 but not in python3.
        return len(self.EC.ch.keys()),N,list(self.EC.ch.keys()),list(self.EC.ch.values())


    def testPM(self):
        PM=MadX()
        PM.command('option;')
        PM.pmad.finish()
        

    def getPropagation(self,indict,matlab=0):

        if not self.SF:
            print('initialize SF')

        if not self.EC:
            print('initialize EC')
            return

        if not self.SM:
            self.SM=SwissFELmagnet()
            self.SF.writeFacility(self.SM,1,self.EM)


        if not self.PM:
            self.PM=MadX(0) # Don't use MADX of this instance
        tw=self.PM.getPropagation(self.SF,self.EM,self.EC,self.SM,indict,matlab)

        if matlab:
            return len(tw),tw
        else:

            twd={}
            if tw==None:
                twd['NAME']=['False']
                twd['S']=[0.0]
                twd['BETX']=[0.0]
                twd['ALFX']=[0.0]
                twd['BETY']=[0.0]
                twd['ALFY']=[0.0]
                twd['X']=[0.0]
                twd['PX']=[0.0]
                twd['Y']=[0.0]
                twd['PY']=[0.0]
                twd['DX']=[0.0]
                twd['DPX']=[0.0]
                twd['DY']=[0.0]
                twd['DPY']=[0.0]
                twd['MUX']=[0.0]
                twd['MUY']=[0.0]
            else:
                twd['NAME']=[]
                twd['S']=[]
                twd['BETX']=[]
                twd['ALFX']=[]
                twd['BETY']=[]
                twd['ALFY']=[]
                twd['X']=[]
                twd['PX']=[]
                twd['Y']=[]
                twd['PY']=[]
                twd['DX']=[]
                twd['DPX']=[]
                twd['DY']=[]
                twd['DPY']=[]
                twd['MUX']=[]
                twd['MUY']=[]
                l=0
                for n in tw.NAME:
                    if 'DRIFT' not in n:
                        twd['NAME'].append(n)
                        twd['S'].append(float(tw.S[tw.indx[n]]))
                        twd['BETX'].append(tw.BETX[tw.indx[n]])
                        twd['ALFX'].append(tw.ALFX[tw.indx[n]])
                        twd['BETY'].append(tw.BETY[tw.indx[n]])
                        twd['ALFY'].append(tw.ALFY[tw.indx[n]])
                        twd['X'].append(tw.X[tw.indx[n]])
                        twd['PX'].append(tw.PX[tw.indx[n]])
                        twd['Y'].append(tw.Y[tw.indx[n]])
                        twd['PY'].append(tw.PY[tw.indx[n]])
                        twd['DX'].append(tw.DX[tw.indx[n]])
                        twd['DPX'].append(tw.DPX[tw.indx[n]])
                        twd['DY'].append(tw.DY[tw.indx[n]])
                        twd['DPY'].append(tw.DPY[tw.indx[n]])
                        twd['MUX'].append(tw.MUX[tw.indx[n]])
                        twd['MUY'].append(tw.MUY[tw.indx[n]])
                        l=l+1


            twd['MADlog']=tw.MADlog

            return l,len(tw.MADlog),twd



    def getMatching(self,indict,matlab=0):

        

        if not self.SF:
            print('initialize SF')

        if not self.EC:
            print('initialize EC')
            return

        if not self.SM:
            self.SM=SwissFELmagnet()
            self.SF.writeFacility(self.SM,1,self.EM)


        if not self.PM:
            self.PM=MadX(0) # Don't use MADX of this instance


        

        if matlab:
            mcommand=self.PM.getMatching(self.SF,self.EM,self.EC,self.SM,indict,matlab)
            return len(mcommand),mcommand
        else:
            tw,QKL=self.PM.getMatching(self.SF,self.EM,self.EC,self.SM,indict)
            outdict={}
            if tw==None:
                outdict['NAME']=['False']
                outdict['S']=[0.0]
                outdict['BETX']=[0.0]
                outdict['ALFX']=[0.0]
                outdict['BETY']=[0.0]
                outdict['ALFY']=[0.0]
                outdict['X']=[0.0]
                outdict['PX']=[0.0]
                outdict['Y']=[0.0]
                outdict['PY']=[0.0]
                outdict['DX']=[0.0]
                outdict['DPX']=[0.0]
                outdict['DY']=[0.0]
                outdict['DPY']=[0.0]
                outdict['MUX']=[0.0]
                outdict['MUY']=[0.0]
            else:
                outdict['NAME']=[]
                outdict['S']=[]
                outdict['BETX']=[]
                outdict['ALFX']=[]
                outdict['BETY']=[]
                outdict['ALFY']=[]
                outdict['X']=[]
                outdict['PX']=[]
                outdict['Y']=[]
                outdict['PY']=[]
                outdict['DX']=[]
                outdict['DPX']=[]
                outdict['DY']=[]
                outdict['DPY']=[]
                outdict['MUX']=[]
                outdict['MUY']=[]
                l=0
                for n in tw.NAME:
                    if 'DRIFT' not in n:
                        outdict['NAME'].append(n)
                        outdict['S'].append(float(tw.S[tw.indx[n]]))
                        outdict['BETX'].append(tw.BETX[tw.indx[n]])
                        outdict['ALFX'].append(tw.ALFX[tw.indx[n]])
                        outdict['BETY'].append(tw.BETY[tw.indx[n]])
                        outdict['ALFY'].append(tw.ALFY[tw.indx[n]])
                        outdict['X'].append(tw.X[tw.indx[n]])
                        outdict['PX'].append(tw.PX[tw.indx[n]])
                        outdict['Y'].append(tw.Y[tw.indx[n]])
                        outdict['PY'].append(tw.PY[tw.indx[n]])
                        outdict['DX'].append(tw.DX[tw.indx[n]])
                        outdict['DPX'].append(tw.DPX[tw.indx[n]])
                        outdict['DY'].append(tw.DY[tw.indx[n]])
                        outdict['DPY'].append(tw.DPY[tw.indx[n]])
                        outdict['MUX'].append(tw.MUX[tw.indx[n]])
                        outdict['MUY'].append(tw.MUY[tw.indx[n]])
                        l=l+1

            outdict['QKL']=QKL
            outdict['MADlog']=tw.MADlog

            return l,len(QKL),len(tw.MADlog),outdict


    def generateChannelList(self,include=[]):

        if not self.EC:
            print('initialize EC')
            return
        # inlist is not properly setup in C... fix in python...
        newlist=[]
        for i in range(0,len(include)):
            newlist.append(include[i][0])

        ch=[]
        for cl in self.EC.ch.values():
            for c in cl:
                filt=1
                for inc in newlist:
                    if inc not in c:
                        filt=0
                if filt:
                    ch.append(c)

        return len(ch),ch

    def MagnetInterface(self,indict):

        

        if not self.SF:
            print('Initialize SF')
            return

        output=[]
        i=0
        for d in indict['DeviceName']:
            if 'MQUA' in d:
                d=d.replace('-','.')
                try:
                    if indict['Mode']==0:
                        output.append(self.SM.QuadrupoleI2KL(d,indict.InputValues[i],indict.Energy[i]))
                    elif indict['Mode']==1:
                        output.append(self.SM.QuadrupoleKL2I(d,indict.InputValues[i],indict.Energy[i]))
                    elif indict['Mode']==2:
                        ele=self.SF.getElement(d)
                        output.append(ele.Length)
                except:
                    output.append(None)
            i=i+1

        return len(output), output
