import os
import shutil
import math
from subprocess import call
from OMElegantLattice import ElegantLattice
from OMDistribution import Dist

# Elegant Interface

class Elegant(ElegantLattice):    

    def __init__(self):

        ElegantLattice.__init__(self)
        self.Distribution=1 # SDDS distribution, but generic when string is empty        
        self.doAperture=0    # flag to include apertures
        self.distsample=1    # sample interval for external distirbution
        
 


    def openStream(self,name,path,dist):

        self.writeInputDeck(path,name,dist)
        self.openLatticeStream(path,name) 

    
    def closeStream(self):
        self.closeLatticeStream()


     
    def writeInputDeck(self, path,name, dist):
        
        self.charge=dist.q
        filename='%s/%s.ele' % (path,name)
        self.fid=open(filename,'w')
        self.fid.write('&run_setup\n')
        self.fid.write('\tlattice\t\t= %s.lat,\n' % name)
        self.fid.write('\tuse_beamline\t= %s,\n' % name)
        self.fid.write('\trootname\t= %s,\n' % name)
        self.fid.write('\toutput\t\t= %s.out,\n')
        self.fid.write('\tcentroid\t\t= %s.cen,\n')
        if (self.doAperture!=0):
            self.fid.write('\tlosses\t\t= %s.lost,\n')
        self.fid.write('\tsigma\t\t= %s.sig,\n')
        self.fid.write('\tfinal\t\t= %s.fin,\n')
        self.fid.write('\tparameters\t= %s.par,\n')
        self.fid.write('\tmagnets\t\t= %s.mag,\n')
        self.fid.write('\tcombine_bunch_statistics = 0,\n')
        self.fid.write('\tdefault_order\t= 2,\n')
        self.fid.write('\tconcat_order\t= 0,\n')
        self.fid.write('\tprint_statistics\t= 0,\n')
        self.fid.write('\trandom_number_seed\t= 9876543210,\n')
        self.fid.write('\tp_central\t= %f,\n' % dist.p0)
        self.fid.write('\ttracking_updates\t= 1\n')
        self.fid.write('&end\n\n')
        self.fid.write('&run_control\n')
        self.fid.write('\tn_steps\t= 1,\n')
        self.fid.write('\treset_rf_for_each_step = 1\n')
        self.fid.write('&end\n\n')
        if self.doAperture!=0:
            self.fid.write('&aperture_data\n')
            self.fid.write('\tinput\t= %s.ape,\n' % name)
            self.fid.write('\tpersistent = 1\n')
            self.fid.write('&end\n\n')          
        self.fid.write('&twiss_output\n')
        self.fid.write('\tfilename\t= %s.twi,\n')
        self.fid.write('\tmatched\t\t= 0,\n')
        self.fid.write('\tbeta_x\t\t= %f,\n' % dist.betax)
        self.fid.write('\tbeta_y\t\t= %f,\n' % dist.betay)
        self.fid.write('\talpha_x\t\t= %f,\n' %dist.alphax)
        self.fid.write('\talpha_y\t\t= %f,\n' % dist.alphay)
        self.fid.write('&end\n\n')
        
        if dist.loaded:
            self.fid.write('&sdds_beam\n')
            self.fid.write('\tinput_type= "elegant",\n')
            self.fid.write('\tsample_interval\t= %d,\n' % self.distsample)
            self.fid.write('\tinput = %s,\n' % dist.sddsfile)
            self.fid.write('\treuse_bunch\t= 0 \n')
            self.fid.write('&end\n\n')
        else:
            self.fid.write('&bunched_beam\n')
            self.fid.write('\tuse_twiss_command_values=1,\n')
            self.fid.write('\tn_particles_per_bunch\t= 10000 \n')
            self.fid.write('\temit_nx\t= %e,\n' % dist.emitx)
            self.fid.write('\temit_ny\t= %e,\n' % dist.emity)
            self.fid.write('\tsigma_s\t= %e,\n' % (dist.sigt*3e8))
            self.fid.write('\tsigma_dp\t= %e,\n' % (dist.sige/dist.p0))
            self.fid.write('\tPo\t= %e,\n' % dist.p0)
            self.fid.write('&end\n\n')          
        self.fid.write('&track\n')
        self.fid.write('&end\n\n')
        self.fid.close()
 

# routines to write lattice

