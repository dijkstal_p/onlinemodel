import math
import sys
#sys.path.append('/afs/psi.ch/intranet/FIN/Applications/Development/Masa.old/VirtualAccelerator/lib')
#from pythoncafe import *
from OMAppTemplate import ApplicationTemplate
import numpy as np

from time import sleep

class SwissFELmagnet(ApplicationTemplate):
    def __init__(self):
        ApplicationTemplate.__init__(self)
        self.MagnetType={}
        
        # Status: ASUUMED = just assumed because of no specification available
        #         SPECIFICATION = specified based on beam dynamics simulations
        #         DESIGN = designed based on field computations, taking into account the specification
        #         MEASUREMENT = measurement with (prototype) fabricated manget
        # Ideally, the following specifications are all from MEASUREMENT. At this moment, they are also from DESIGN, SPECIFICATION or even "ASSUMED".

        self.QuadSpec={} # [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3,R] # b1/R is the gradient at Imax. If Ilin = Imax+0.1, Ilin is not specified.
        self.QuadSpec['QFA']  =[0.15, 0.173, 150.0, 91.1, 0.0, 0.74521, -0.00813, -0.03542, 22.5e-3] # From MEASUREMENT. Documentation: https://intranet.psi.ch/pub/Swiss_FEL/FinQuadrupoles/QFA.pdf
        self.QuadSpec['QFB']  =[0.08,  0.11,  10.0, 10.1, 0.0,  1.6956,      0.0,      0.0,     1.0] # From MEASUREMENT(?). Documentation(Implementaion): the matlab version Injector on-line model, Curr2KLquad.m
        self.QuadSpec['QFC']  =[0.08,  0.11,  10.0, 10.1, 0.0,     0.5,      0.0,      0.0,     1.0] # From DESIGN(?). Documentation: Injector wiki, Subsystems (as of 14th Aug 2014)
        self.QuadSpec['QFD']  =[0.15,  0.15,  10.0, 10.1, 0.0,    20.0,      0.0,      0.0,     1.0] # From DESIGN. Documentation: FEL-SS88-007-7.pdf (found in Alfresco: Company Home > Projects > SwissFEL > Facility > 8850 Magnets)



        self.QuadSpec['QFF']  =[0.08, 0.08,   10.0, 10.1, 0.0,    50.0,      0.0,      0.0,     1.0] # From SPECIFICATION. Documentation: FEL-SS88-008-8.pdf (found in Alfresco)
        self.QuadSpec['QFM']  =[ 0.3,  0.3,   50.0, 50.1, 0.0,    50.0,      0.0,      0.0,     1.0] # From SPECIFICATION. Documentation: FEL-SS88-010-0_Requirement_Specification_QFM_Magnets.pdf (found in Alfresco)
        self.QuadSpec['QFCOR']=[0.16, 0.11,   10.0, 10.1, 0.0,  0.0278,      0.0,      0.0,     1.0] # From DESIGN. Documentation: qp.pdf (found in Alfresco)
        self.QuadSpec['QFU']  =[0.04, 0.04,      0,    0,   6,     0.0,      0.0,      0.0,     1.0] # Alignment quadrupole. Where is the specification found? 10 T/m is ASSUMED.
        # Imax is specified as 1 A for the permanent magnet such that the polarity can be specified with +1 A or -1 A 
        
        self.QuadSpec['QFA-SKEW']=self.QuadSpec['QFA']
        self.QuadSpec['QFB-SKEW']=self.QuadSpec['QFB']
        self.QuadSpec['QFC-SKEW']=self.QuadSpec['QFC']
        self.QuadSpec['QFD-SKEW']=self.QuadSpec['QFD']
        self.QuadSpec['QFDM']=self.QuadSpec['QFD']
        self.QuadSpec['QFCOR-SKEW']=self.QuadSpec['QFCOR']


        self.BendSpec={} # [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3] # b1 is the field strength [T] (linear term) at Imax
        self.BendSpec['SHA']  =[0.25,   0.235,  30.942, 31.042,  0.0,   0.052168,    0.0,    0.0] # From MEASUREMENT(?). Documentation: Injector wiki, Momentum measurement (as of 14th Aug 2014). Imax is replaced by Inominal
        self.BendSpec['AFL']  =[ 0.1,     0.1,    50.0,   50.1,  0.0,     0.1863,    0.0,    0.0] # From SPECIFICATION. Documentation: FEL-GR06-143-0 (found in Alfresco)
        self.BendSpec['AFBC1']=[0.25,  0.2678,   200.0,  200.1,  0.0,    0.46328,    0.0,    0.0] # From MEASUREMENT. Documentation: AFBC1 Shimming of .... V. Vrankovic, May-June 2013 (found in Alfresco)
        self.BendSpec['AFBC3']=[ 0.5,     0.5,   150.0,  150.1,  0.0,   0.929385,    0.0,    0.0] # From MEASUREMENT. Documentation: FEL-SS88-040-1 (found in Alfresco)
        self.BendSpec['AFS']  =[ 0.76,     0.76,   200.0,  200.1,  0.0,       0.95667,    0.0,    0.0] # Where is the specification found? 5.5 degree @ 3 GeV with 200 A power supply is ASSUMED. 
        self.BendSpec['AFSS'] =[0.36,    0.36,   200.0,  200.1,  0.0,      0.281,    0.0,    0.0] # Where is the specification found? 0.3 degree @ 5.8 GeV with 200 A power supply is ASSUMED. 
        self.BendSpec['AFD1'] =[ 2.0,     2.0,   200.0,  200.1,  0.0,     1.6871,    0.0,    0.0] # Where is the specification found? 10 degree @ 5.8 GeV with 200 A power supply is ASSUMED. 
        self.BendSpec['AFD2'] =[ 1.1,     1.1,   150.0,  150.1,  0.0,      1.609,    0.0,    0.0] # From DESIGN, AFD2-150.xls in Alfresco, 15.12.2014
        self.BendSpec['AFP1'] =[ 0.3,     0.3,    50.0,   50.1,  0.0,        1.0,    0.0,    0.0] # Where is the specification found?  1 T at 50 A is ASSUMED. 
        self.BendSpec['AFP2'] =[ 0.3,     0.3,    50.0,   50.1,  0.0,        1.0,    0.0,    0.0] # Where is the specification found?  1 T at 50 A is ASSUMED. 
        # Imax is specified as 1 A for the permanent magnet. The current I can then be 1 A nominally and may be 0 A when you want to "turn it off".
        self.BendSpec['AFDL'] =[ 1.2,     1.2,    50.0,   50.1,  0.0,        1.0,    0.0,    0.0] # Where is the specification found? 1 T at 50 A is ASSUMED. 


        
        self.CorrSpec={} # [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3] # b1 is the field strength [T] (linear term) at Imax
        self.CorrSpec['QFD']  =[ 0.15,    0.15,    10.0,   10.1,  0.0,    0.05155,    0.0,    0.0] # From SPECIFICATION, 400 urad at 5.8 GeV 
        self.CorrSpec['QFDM'] =[ 0.15,    0.15,    10.0,   10.1,  0.0,    0.01288,    0.0,    0.0] # From SPECIFICATION, 100 urad at 5.8 GeV, modified QFD for the low-energy part of the linac 
        self.CorrSpec['QFF']  =[ 0.08,    0.08,    10.0,   10.1,  0.0,    0.02576,    0.0,    0.0] # From SPECIFICATION, 200 urad at 5.8 GeV 
        self.CorrSpec['AFL']  =[  0.1,     0.1,    10.0,   10.1,  0.0,    0.02135,    0.0,    0.0] # From SPECIFICATION, 200 urad at 3.0 GeV. Documentation: FEL-GR06-143-0.pdf (found in Alfresco)
        self.CorrSpec['AFBC1']=[ 0.25,   0.269,    10.0,   10.1,  0.0,   0.001211,    0.0,    0.0] # From MEASUREMENT. Documentation: AFBC1_shimming.pdf (found in Alfresco)
        self.CorrSpec['AFBC3']=[  0.5,     0.5,    10.0,   10.1,  0.0,   0.016305,    0.0,    0.0] # From SPECIFICATION. b0 computed from the motive-force ratio (0.929385*15*10/57/150). Documentation: FEL-SS88-040-0.pdf (found in Alfresco)
        self.CorrSpec['AFSS'] =[ 0.36,    0.36,   200.0,  200.1,  0.0,   0.028119,    0.0,    0.0] # Where is the specification found? 0.003 degree @ 5.8 GeV with 200 A power supply is ASSUMED. 
        self.CorrSpec['AFDL'] =[  1.2,     1.2,    50.0,   50.1,  0.0,   0.001454,    0.0,    0.0] # Where is the specification found? 0.01 degree @ 300 MeV with 50 A power supply is ASSUMED. 
        self.CorrSpec['SFC']  =[ 0.05,    0.05,    10.0,   10.1,  0.0,       0.02,    0.0,    0.0] # Where is the specification found? 1 mrad @ 300 MeV with 10 A power supply is ASSUMED. 
        self.CorrSpec['SFQFM']=[  0.2,  0.2171,    10.0,   10.1,  0.0,     0.0198,    0.0,    0.0] # From DESIGN. Documentation: SFQFM.pdf 
        self.CorrSpec['SFDD'] =[0.015,  0.0171,    20.0,   20.1,  0.0,    0.00026,    0.0,    0.0] # From MEASUREMENT. Documentation: https://intranet.psi.ch/pub/Swiss_FEL/FinSteeringMagnets/SFDD.pdf
        self.CorrSpec['SFD1'] =[0.015,  0.0171,    20.0,   20.1,  0.0,    0.00026,    0.0,    0.0] # Where is the specification found? Copied from SFDD
        self.CorrSpec['SFB']  =[0.005,   0.005,    10.0,   10.1,  0.0,    0.00667,    0.0,    0.0] # Where is the specification found? 1 mrad @ 10 MeV with 10 A power supply is ASSUMED. 


        self.CorrSpec['RES-KICKER-AC'] =[ 1.0,     1.0,    1.0,   1.1,  0.0,     0.0,    0.0,    0.0] # Where is the specification found? No field is ASSUMED
        self.CorrSpec['RES-KICKER-DC'] =[0.21,    0.21,    1.0,   1.1,  0.0,     0.0,    0.0,    0.0] # Where is the specification found? No field is ASSUMED

        self.CorrSpec['SFU']  =[ 0.06,    0.06,    10.0,   10.1,  0.0,       0.02,    0.0,    0.0] # Where is the specification found? 1 mrad @ 300 MeV with 10 A power supply is ASSUMED. 
        self.CorrSpec['SFU-H'] = self.CorrSpec['SFU']
        self.CorrSpec['U15'] = [ 0.06,    0.06,    10.0,   10.1,  0.0,       0.02,    0.0,    0.0] # Where is the specification found? 1 mrad @ 300 MeV with 10 A power supply is ASSUMED. 
        self.CorrSpec['UE40'] = [ 0.06,    0.06,    10.0,   10.1,  0.0,       0.02,    0.0,    0.0] # Where is the specification found? 1 mrad @ 300 MeV with 10 A power supply is ASSUMED. 

    def writeBend(self,ele):
        if ele.Name not in self.MagnetType.keys():
            self.MagnetType[ele.Name]=ele.Baugruppe
            if 'cor' in ele.__dict__:
                name=ele.Name.replace('MBND','MCRX')
                self.MagnetType[name]=ele.Baugruppe
        return

    def writeQuadrupole(self,ele):     
        if ele.Name not in self.MagnetType.keys():
            self.MagnetType[ele.Name]=ele.Baugruppe
            if 'corx' in ele.__dict__:
                name=ele.Name.replace('MQUA','MCRX')
                self.MagnetType[name]=ele.Baugruppe
            if 'cory' in ele.__dict__:
                name=ele.Name.replace('MQUA','MCRY')
                self.MagnetType[name]=ele.Baugruppe
        return
   
    def writeCorrector(self,ele):                
        if ele.Name not in self.MagnetType.keys():
            self.MagnetType[ele.Name]=ele.Baugruppe
            if 'corx' in ele.__dict__:
                name=ele.Name.replace('MCOR','MCRX')
                self.MagnetType[name]=ele.Baugruppe
            if 'cory' in ele.__dict__:
                name=ele.Name.replace('MCOR','MCRY')
                self.MagnetType[name]=ele.Baugruppe
        return

    def writeSextupole(self,ele):       
        if ele.Name not in self.MagnetType.keys():
            self.MagnetType[ele.Name]=ele.Baugruppe
        return
       

    def writeUndulator(self,ele): 
        #Bug to fix here
        if ele.Name not in self.MagnetType.keys():
            self.MagnetType[ele.Name]=ele.Baugruppe
            if 'cor' in ele.__dict__:
                name=ele.Name.replace('UIND','MCRX')
                self.MagnetType[name]=ele.Baugruppe
        return


    def writeSolenoid(self,ele):
        if ele.Name not in self.MagnetType.keys():
            self.MagnetType[ele.Name]=ele.Baugruppe
        return

    
    def Brho(self,Energy):
        gamma=(Energy+0.511)/0.511 # Energy in units of MeV since 29.06.2015
        beta=math.sqrt(1-1/gamma**2)
        p=511e3*beta*gamma/1e9 # momentum in GeV/c
        brho=p/0.299792 # P[GeV/c]=0.3*Brho[T.m]
        return brho

    def QuadrupoleI2KL(self,ele,I,EM=None):
        # Return KL,physical length and magnetic length

        def getKL(Type,brho,I):
            [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3,R]=self.QuadSpec[Type]
            if abs(I)<Ilin:
                G = (b0 + b1*abs(I)/Imax)/R
            else:
                G = (b0 + b1*abs(I)/Imax + b2*((abs(I)-Ilin)**2/(Imax-Ilin)**2) + b3*((abs(I)-Ilin)**3/(Imax-Ilin)**3))/R
            K = np.sign(I)*G/brho
            KL = K*MagneticLength
            return KL


        def singleElem(Name,EM,I):
            if isinstance(EM,int) or isinstance(EM,float):
                Energy=float(EM)
            else:
                Energy=EM.Energy[Name][0]
            Type=self.MagnetType[Name]
            brho=self.Brho(Energy)
            if brho<=0:
                return 0
            else:
                return getKL(Type,brho,I)
                
        if not EM:
            'Energy is essential to compute KL'
            return 0,0,0

        # accept ele as a single element or many elements in list
        # also ele can be element name in string or base-element instance
        # The last input argument can be either kinetic energy [GeV] or EnergyManager instance
        KLlist=[]
        if isinstance(ele,list):
            for i in range(0,len(ele)):
                if not isinstance(ele[i],str):
                    # In case, where ele is instance of a base element
                    Name=ele[i].Name
                else:
                    Name=ele[i]
                # return a list when the input is a list
                if isinstance(EM,list):
                    KLlist.append(singleElem(Name,EM[i],I[i]))
                else:
                    KLlist.append(singleElem(Name,EM,I[i]))
            return KLlist
        else:
            if not isinstance(ele,str):
                # In case, where ele is instance of a base element
                Name=ele.Name
            else:
                Name=ele
            return singleElem(Name,EM,I)


    def QuadrupoleKL2I(self,ele,QuadKL,EM=None):
        # Return KL,physical length and magnetic length

        def getI(Type,brho,KL):
            [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3,R]=self.QuadSpec[Type]
            G = brho*KL/MagneticLength
            I=Imax*(G*R -b0)/b1
                
            if abs(I)>Ilin:
                coeff=[b3/(Imax-Ilin)**3,b2/(Imax-Ilin)**2,b1/Imax,b0-G*R+b1*Ilin/Imax]
                I=KL/abs(KL)*(np.roots(coeff)[0]+Ilin)

                #G = (b0 + b1*abs(I)/Imax + b2*((abs(I)-Ilin)**2/(Imax-Ilin)**2) + b3*((abs(I)-Ilin)**3/(Imax-Ilin)**3))/R
            return I


        def singleElem(Name,EM,KL):
            if isinstance(EM,int) or isinstance(EM,float):
                Energy=float(EM)
            else:
                Energy=EM.Energy[Name][0]
            Type=self.MagnetType[Name]
            brho=self.Brho(Energy)
            if brho<=0:
                return 0
            else:
                return getI(Type,brho,KL)
                
        if not EM:
            'Energy is essential to compute KL'
            return 0

        # accept ele as a single element or many elements in list
        # also ele can be element name in string or base-element instance
        # The last input argument can be either kinetic energy [GeV] or EnergyManager instance
        Ilist=[]
        if isinstance(ele,list):
            for i in range(0,len(ele)):
                if not isinstance(ele[i],str):
                    # In case, where ele is instance of a base element
                    Name=ele[i].Name
                else:
                    Name=ele[i]
                # return a list when the input is a list
                if isinstance(EM,list):
                    Ilist.append(singleElem(Name,EM[i],QuadKL[i]))
                else:
                    Ilist.append(singleElem(Name,EM,QuadKL[i]))
            return Ilist
        else:
            if not isinstance(ele,str):
                # In case, where ele is instance of a base element
                Name=ele.Name
            else:
                Name=ele
            return singleElem(Name,EM,QuadKL)


    def PQuadrupoleKL(self,ele,EM=None):
        # Return KL,physical length and magnetic length

        def getKL(Type,brho):
            [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3,R]=self.QuadSpec[Type]
            K=b0/brho
            KL = K*MagneticLength
            return KL


        def singleElem(Name,EM):
            if isinstance(EM,int) or isinstance(EM,float):
                Energy=float(EM)
            else:
                Energy=EM.Energy[Name][0]
            Type=self.MagnetType[Name]
            brho=self.Brho(Energy)
            if brho<=0:
                return 0
            else:
                return getKL(Type,brho)
                
        if not EM:
            'Energy is essential to compute KL'
            return 0,0,0

        # accept ele as a single element or many elements in list
        # also ele can be element name in string or base-element instance
        # The last input argument can be either kinetic energy [GeV] or EnergyManager instance
        KLlist=[]
        if isinstance(ele,list):
            for i in range(0,len(ele)):
                if not isinstance(ele[i],str):
                    # In case, where ele is instance of a base element
                    Name=ele[i].Name
                else:
                    Name=ele[i]
                # return a list when the input is a list
                if isinstance(EM,list):
                    KLlist.append(singleElem(Name,EM[i]))
                else:
                    KLlist.append(singleElem(Name,EM))
            return KLlist
        else:
            if not isinstance(ele,str):
                # In case, where ele is instance of a base element
                Name=ele.Name
            else:
                Name=ele
            if brho<=0:
                return 0
            else:
                return singleElem(Name,EM)



    def BendI2Angle(self,ele,I,EM=None):
        # Return KL,physical length and magnetic length

        def getAngle(Type,brho,I):
            [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3]=self.BendSpec[Type]
            if abs(I)<Ilin:
                B = (b0 + b1*abs(I)/Imax)
            else:
                B = (b0 + b1*abs(I)/Imax + b2*((abs(I)-Ilin)**2/(Imax-Ilin)**2) + b3*((abs(I)-Ilin)**3/(Imax-Ilin)**3))
            Bbrho = np.sign(I)*B/brho
            Angle = Bbrho*MagneticLength*180.0/math.pi # Return value is in units of degree!
            return Angle


        def singleElem(Name,EM,I):
            if isinstance(EM,int) or isinstance(EM,float):
                Energy=float(EM)
            else:
                Energy=EM.Energy[Name][0]
            Type=self.MagnetType[Name]
            brho=self.Brho(Energy)
            if brho<=0:
                return 0
            else:
                return getAngle(Type,brho,I)
            
                
        if not EM:
            'Energy is essential to compute KL'
            return 0,0,0

        # accept ele as a single element or many elements in list
        # also ele can be element name in string or base-element instance
        # The last input argument can be either kinetic energy [GeV] or EnergyManager instance
        AngleList=[]        
        if isinstance(ele,list):
            for i in range(0,len(ele)):
                if not isinstance(ele[i],str):
                    # In case, where ele is instance of a base element
                    Name=ele[i].Name
                else:
                    Name=ele[i]
                # return a list when the input is a list
                if isinstance(EM,list):
                    AngleList.append(singleElem(Name,EM[i],I[i]))
                else:
                    AngleList.append(singleElem(Name,EM,I[i]))
            return AngleList
        else:
            if not isinstance(ele,str):
                # In case, where ele is instance of a base element
                Name=ele.Name
            else:
                Name=ele
            return singleElem(Name,EM,I) 


    def BendAngle2I(self,ele,Angle,EM=None):
        # Return KL,physical length and magnetic length

        def getI(Type,brho,Ang):
            [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3]=self.BendSpec[Type]
            Ang=Ang*math.pi/180 # Angle specification is given in units of radian!
            B = brho*Ang/MagneticLength
            I=Imax*(B -b0)/b1
                
            if abs(I)>Ilin:
                coeff=[b3/(Imax-Ilin)**3,b2/(Imax-Ilin)**2,b1/Imax,b0-B+b1*Ilin/Imax]
                I=Ang/abs(Ang)*(np.roots(coeff)[0]+Ilin)

                #G = (b0 + b1*abs(I)/Imax + b2*((abs(I)-Ilin)**2/(Imax-Ilin)**2) + b3*((abs(I)-Ilin)**3/(Imax-Ilin)**3))/R
            return I


        def singleElem(Name,EM,Ang):
            if isinstance(EM,int) or isinstance(EM,float):
                Energy=float(EM)
            else:
                Energy=EM.Energy[Name][0]
            Type=self.MagnetType[Name]
            brho=self.Brho(Energy)
            if brho<=0:
                return 0
            else:
                return getI(Type,brho,Ang)
                
        if not EM:
            'Energy is essential to compute KL'
            return 0,0,0

        # accept ele as a single element or many elements in list
        # also ele can be element name in string or base-element instance
        # The last input argument can be either kinetic energy [GeV] or EnergyManager instance
        Ilist=[]
        if isinstance(ele,list):
            for i in range(0,len(ele)):
                if not isinstance(ele[i],str):
                    # In case, where ele is instance of a base element
                    Name=ele[i].Name
                else:
                    Name=ele[i]
                # return a list when the input is a list
                if isinstance(EM,list):
                    Ilist.append(singleElem(Name,EM[i],Angle[i]))
                else:
                    Ilist.append(singleElem(Name,EM,Angle[i]))
            return Ilist
        else:
            if not isinstance(ele,str):
                # In case, where ele is instance of a base element
                Name=ele.Name
            else:
                Name=ele
            return singleElem(Name,EM,Angle)




    def CorrectorI2Kick(self,ele,I,EM=None):

        # Return KL,physical length and magnetic length
        def getKick(Type,brho,I):
            [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3]=self.CorrSpec[Type]
            if abs(I)<Ilin:
                B = (b0 + b1*abs(I)/Imax)
            else:
                B = (b0 + b1*abs(I)/Imax + b2*((abs(I)-Ilin)**2/(Imax-Ilin)**2) + b3*((abs(I)-Ilin)**3/(Imax-Ilin)**3))
            Bbrho = np.sign(I)*B/brho
            Angle = Bbrho*MagneticLength
            return Angle


        def singleElem(Name,EM,I):
            if isinstance(EM,int) or isinstance(EM,float):
                Energy=EM
            else:
                Energy=EM.Energy[Name][0]
            Type=self.MagnetType[Name]
            brho=self.Brho(Energy)
            if brho<=0:
                return 0
            else:
                return getKick(Type,brho,I)

        if not EM:
            'Energy is essential to compute KL'
            return 0,0,0

        # accept ele as a single element or many elements in list
        # also ele can be element name in string or base element instance
        # The last input argument can be either kinetic energy [GeV] or EnergyManager instance
        KickList=[]
        if isinstance(ele,list):
            for i in range(0,len(ele)):
                if not isinstance(ele[i],str):
                    # In case, where ele is instance of a base element
                    Name=ele[i].Name
                else:
                    Name=ele[i]
                # return a list when the input is a list
                if isinstance(EM,list):
                    KickList.append(singleElem(Name,EM[i],I[i]))
                else:
                    KickList.append(singleElem(Name,EM,I[i]))
            return KickList
        else:
            if not isinstance(ele,str):
                # In case, where ele is instance of a base element
                Name=ele.Name
            else:
                Name=ele
            return singleElem(Name,EM,I)


    def CorrectorKick2I(self,ele,Kick,EM=None):

        # Return KL,physical length and magnetic length
        def getI(Type,brho,k):
            [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3]=self.CorrSpec[Type]
            B = brho*k/MagneticLength
            I=Imax*(B -b0)/b1
                
            if abs(I)>Ilin:
                coeff=[b3/(Imax-Ilin)**3,b2/(Imax-Ilin)**2,b1/Imax,b0-B+b1*Ilin/Imax]
                I=k/abs(k)*(np.roots(coeff)[0]+Ilin)
            return I


        def singleElem(Name,EM,k):
            if isinstance(EM,int) or isinstance(EM,float):
                Energy=EM
            else:
                Energy=EM.Energy[Name][0]
            Type=self.MagnetType[Name]
            brho=self.Brho(Energy)
            if brho<=0:
                return 0
            else:
                return getI(Type,brho,k)

        if not EM:
            'Energy is essential to compute KL'
            return 0,0,0

        # accept ele as a single element or many elements in list
        # also ele can be element name in string or base element instance
        # The last input argument can be either kinetic energy [GeV] or EnergyManager instance
        Ilist=[]
        if isinstance(ele,list):
            for i in range(0,len(ele)):
                if not isinstance(ele[i],str):
                    # In case, where ele is instance of a base element
                    Name=ele[i].Name
                else:
                    Name=ele[i]
                # return a list when the input is a list
                if isinstance(EM,list):
                    Ilist.append(singleElem(Name,EM[i],Kick[i]))
                else:
                    Ilist.append(singleElem(Name,EM,Kick[i]))
            return Ilist
        else:
            if not isinstance(ele,str):
                # In case, where ele is instance of a base element
                Name=ele.Name
            else:
                Name=ele
            return singleElem(Name,EM,Kick)






        


    def SextupoleI2K2L(self,ele,I,EM=None):
        'Return 0 for the time being'
        return 0,0,0

    def SolenoidI2KL(self,ele,I,EM=None):
        'Return 0 for the time being'
        return 0,0,0


    def QuadrupoleKL2K(self,dn,QuadKL,Mode='P'):
        # K of the physical length by default
        QuadK=[]
        for i in range(0,len(dn)):
            Type=self.MagnetType[dn[i]]
            if Mode=='P':
                L=self.QuadSpec[Type][0] # Physical length
            elif Mode=='M':
                L=self.QuadSpec[Type][1] # Magnetic length
            K=QuadKL[i]/L
            QuadK.append(K)

        return QuadK


    

    def QuadrupoleK2KL(self,dn,QuadK,Mode='P'):
        # Need to know if K is of physical length or magnetic length 
        QuadKL=[]
        for i in range(0,len(dn)):
            Type=self.MagnetType[dn[i]]
            if Mode=='P':
                L=self.QuadSpec[Type][0]
            elif Mode=='M':
                L=self.QuadSpec[Type][1]
            KL=QuadK[i]*L
            QuadKL.append(KL)

        return QuadKL




    def updateQuadKfromEpics(self,CI=None,EC=None):
        # Obsolete?
        if not CI:  # Control interface
            return
        if not EC:  # Control interface
            return

        dn,QuadKL=EC.get('Quad:KL')
        QuadK=self.QuadrupoleKL2K(dn,QuadKL)
        CI.setGroup(dn,'k1',QuadK)



    def writeQuad2Epics(self,SF=None,EC=None,EM=None):
        # Used in Virtual Accelerator but general function
        if not SF:
            return
        if not EC:
            return
        if not EM:
            return

        dn=EC.getDeviceList('Quad')
        QuadK=SF.getGroup(dn,'k1')
        QuadKL=self.QuadrupoleK2KL(dn,QuadK)
        EC.put('Quad:KL',QuadKL)

        if EM: # Synchronize also I-SET if Energy manager is given.
            I=self.QuadrupoleKL2I(dn,QuadKL,EM)

        EC.put('Quad:I-SET',I)
        return



    def BeamPathFromEpics(self,EC=None,SF=None,EM=None):
        # Method for Virtual Accelerator
        if not EC:
            return
        if not SF:
            return

        mapping=SF.mapping()

        if EM:
            # For Virtual accelerator, ANGLE is computed from the real beam energy not the ones in Epics
            Bends,I=EC.get('Bend:I-SET')
            Angle=self.BendI2Angle(Bends,I,EM)
        else:
            # Assuming that AngleI daemon is working
            Bends,Angle=EC.get('Bend:ANGLE')


        # This will be implemented later
        #Screens,InOut=EC.get('Screen','InOut')

        
        OnOff=[]
        BranchBendMap={}
        for e in SF.BranchBend:
            ele=SF.getElement(e)
            
            key=str(ele.mapping[0])+'_'+str(ele.mapping[1])
            BranchBendMap[key]=e
            dang=ele.design_angle
            ang=Angle[Bends.index(e)]
            if abs(ang)>0.05*abs(ele.design_angle):
                OnOff.append(1) # Dipole is on
            else:
                OnOff.append(0)


        for i in range(0,len(mapping)):
            if mapping[i][1]==0:
                MotherID=i

        # Tracing the path
        CurrentID=MotherID # tracing starts from the mother
        LastKid=0
        Trace=[]
        while not LastKid:
            BranchID=-1
            for j in range(1,mapping[CurrentID][2]+1):
                key=str(CurrentID)+'_'+str(mapping[CurrentID][2]+1-j)
                bb=BranchBendMap[key]
                if OnOff[SF.BranchBend.index(bb)]:
                    BranchID=mapping[CurrentID][2]+1-j

            # Find the kid
            KidFound=0
            CID=CurrentID
            for i in range(0,len(mapping)):
                m=mapping[i]
                if m[0]==CID and m[1]==BranchID:
                    KidFound=1
                    CurrentID=i

            if BranchID==-1:
                Trace.append([CID,BranchID])
            else:
                key=str(CID)+'_'+str(BranchID)
                bb=BranchBendMap[key]
                Trace.append([CID,BranchID,SF.foundMarker[key],bb,Angle[Bends.index(bb)]])


            # If no kid is found, this is the last kid
            if KidFound==0:
                LastKid=1


        return Trace

    def computeBCdelay(self,EC=None,SF=None):
        # Used in Virtual Accelerator but general function (Can be used for BC tool?)
        # This function may be moved to OMType (Facility container)

        if not EC:
            return
        if not SF:
            return
        
        # The numbers may need to be more precise
        freq={'S':3.0e9,'C':6.0e9,'X':12.0e9}
        
        RF=EC.getDeviceList('RF')
        Chicane,Angle= EC.get('Chicane:ANGLE')
        
        PhaseBC=[0.0]*len(RF)
        for i in range(0,len(Chicane)):
            ang=Angle[i]*math.pi/180.0
            c=Chicane[i]
            if ang!=0.0:
                for bc in SF.BC:
                    if c.split(',')[0]==bc[0].split('.')[0]:
                        arm=bc[4]
                        mag=bc[0]
                        dL=2.0*arm*(1.0/math.cos(ang)-1.0)
                        dt=dL/3.0e8
                        for cav in self.RF:
                            if SF.isUpstream(mag,cav):
                                cave=SF.getElement(cav)
                                frf=freq[cave.Band]
                                dph=2.0*math.pi*fre*dt*(180.0/math.pi) # in degree
                                PhaseBC[RF.index(cav)]=PhaseBC[RF.index(cav)]+dph
                                
        return PhaseBC

         
        
    def connectChannels(self,EC,SF):
        # Method for Virtual accelerator
        # Connect dipole magnets that are excited by single power supply.

        def cb_ps(h):
            bc=info[h][0]
            Nmag=info[h][1]
            ename=EC.prefix+bc[0].Name.replace('.','-')+':I-SET'
            c=EC.cafe.getPVCache(ename)
            I=c.value[0]
            for i in range(1,Nmag):
                ename=EC.prefix+bc[i].Name.replace('.','-')+':I-SET'
                EC.put1(ename,I)


        handle=[]
        info={}
        EC.cafe.openPrepare()
        for bc in SF.BC:
            if len(bc)==5:
                Nmag=4
            else:
                Nmag=len(bc)  
            if Nmag!=3: # Athos dipoles may have own power supply?
                for ib in range(0,Nmag):
                    ename=EC.prefix+bc[ib].Name.replace('.','-')+':I-SET'
                    h=EC.cafe.open(ename)
                    handle.append(h)
                    info[h]=[bc,Nmag]
        EC.cafe.openNowAndWait(10)
        i=0
        EC.cafe.openMonitorPrepare()
        for bc in SF.BC:
            if len(bc)==5:
                Nmag=4
            else:
                Nmag=len(bc)  
            if Nmag!=3: # Athos dipoles may have own power supply.
                for ib in range(0,Nmag):
                    h=handle[i]
                    m0=EC.cafe.monitorStart(h, cb=cb_ps, dbr=EC.cyca.CY_DBR_PLAIN, mask=EC.cyca.CY_DBE_VALUE)
                    i=i+1
        EC.cafe.openMonitorNowAndWait(10)

        sleep(0.1)
