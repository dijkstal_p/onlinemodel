import math
import sys
from OMAppTemplate import ApplicationTemplate
import numpy as np
import random

from OMType import *
from OMLayout import *

from itertools import chain

from time import sleep

from copy import deepcopy

class SwissFELdiagnostic(ApplicationTemplate):
    def __init__(self):
        ApplicationTemplate.__init__(self)

        self.ScreenFilled=[] # A list for Virtual accelerator

        # temporary specification, #pixcelX,#pixcelY,Xcenter,Ycenter,bit,sizeX,sizeY
        self.tempSpec=[2560,2160,1280,1080,65535,10e-6,10e-6]
        self.ScreenSpec={}

        try:
            Layout=SwissFEL()
            SF=FacilityContainer(Layout,2)
            self.SFBC=SF.BC
            del(SF)
            del(Layout)
        except:
            self.SFBC=None



    def getScreenParameter(self,ele):
        if not isinstance(ele,basestring):
            name=ele.Name
        else:
            name=ele


        return self.ScreenSpec[name]


    def writeDiagnostic(self,ele):
        if 'DSCR' in ele.Name:
            spec=self.tempSpec
            if 'SARUN' in ele.Name:
                spec[5]=2e-6
                spec[6]=2e-6
            self.ScreenSpec[ele.Name]=spec




    def CreateScreenImage(self,EM,n,tw,LT,SF):
        # Method for Virtual Accelerator

        if '.MARK' in n:
            nm=n
            nn=n.replace('.MARK','')
        else:
            nm=n+'.MARK'
            nn=n

        nex=tw.EX
        ney=tw.EY
        Benergy=EM.EnergyAt(nn)[0]

        betx=tw.BETX[tw.indx[nm]]
        bety=tw.BETY[tw.indx[nm]]


        X=tw.X[tw.indx[nm]]
        Y=tw.Y[tw.indx[nm]]

        hx=tw.DX[tw.indx[nm]]
        hy=tw.DY[tw.indx[nm]]



        Xpix=self.ScreenSpec[nn][5]
        Ypix=self.ScreenSpec[nn][6]
        Nx=self.ScreenSpec[nn][0] # Number of pixel
        Ny=self.ScreenSpec[nn][1]
        Nxc=self.ScreenSpec[nn][2] # Pixel location corresponding to beam axis
        Nyc=self.ScreenSpec[nn][3]

        Nxcb=int(X/Xpix) # Pxel location corresponding to Beam centre
        Nycb=int(Y/Ypix)

        image=[0]*(Nx*Ny)

        maxV=0
        
        Edist=[]
        zdist=[]
        if LT:
            TDStemp=[]
            Ne=LT.pylt.Nesim
            for j in range(0,len(LT.pylt.output.BLj)):
                if nn==LT.pylt.output.BLj[j][6] and len(Edist)==0:
                    zdist=LT.pylt.output.zposj[j]
                    Edist=LT.pylt.output.dE_Ej[j]
                    bl_scr=LT.pylt.output.sigzGj[j]
                    bety_scr=tw.BETY[tw.indx[nn+'.MARK']]
                    muy_scr=tw.MUY[tw.indx[nn+'.MARK']]
                if 'TDS' in LT.pylt.output.BLj[j][6]:
                    c=LT.pylt.output.BLj[j][6]
                    if SF.isUpstream(c,nn):
                        ele=SF.getElement(c)
                        bl_tds=LT.pylt.output.sigzGj[j]
                        bety_tds=tw.BETY[tw.indx[ele.Name]]
                        muy_tds=tw.MUY[tw.indx[ele.Name]]
                        #muy=2*math.pi*(muy_scr-muy_tds)
                        #R34=math.sqrt(muy_tds*muy)
                        # TDS voltage 'difference' from the bunch length at TDS
                        dVtds=2*math.pi*ele.Frequency*ele.Gradient*ele.Length*math.cos(ele.Phase)*bl_tds/3e8
                        kick=dVtds/EM.EnergyAt(c)[0] # Approximate E/c~P
                        TDStemp.append([kick,bety_tds,muy_tds])
            TDSdy=[]
            for tds in TDStemp:
                muy=2*math.pi*(muy_scr-muy_tds)
                R34=math.sqrt(bety_tds*bety_scr)*math.sin(muy) # Assuming no coupling...
                TDSdy.append(kick*R34)
        else:
            Ne=20000


        dxa=[]
        dya=[]
        for ip in range(0,Ne):
            # First, the distribution is made for BETX/Y=1 m and NEX/Y=1 m
            #z1=random.random()
            #r=math.log(1.0/(1.0-z1))
            r=random.gauss(0,1)
            #theta=2.0*math.pi*random.random()
            #dx=math.sqrt(r)*math.sin(theta)
            dx=r

            #z1=random.random()
            #r=math.log(1.0/(1.0-z1))
            r=random.gauss(0,1)
            #theta=2.0*math.pi*random.random()
            #dy=math.sqrt(r)*math.cos(theta)
            dy=r


            # Scale with beta and emittance
            ex=nex/(Benergy/0.511)  # Geometrical emittance
            ey=ney/(Benergy/0.511)  # Energy in units of MeV since 29.06.2015

            dx=math.sqrt(betx*ex)*dx+Xpix/2.0
            dy=math.sqrt(bety*ey)*dy+Ypix/2.0


            if LT and len(Edist):
                dx=dx+hx*Edist[ip]
                dy=dy+hy*Edist[ip]
                if TDSdy:
                    for tdy in TDSdy:
                        dy=dy+tdy*zdist[ip]/bl_scr # streaking

            dxa.append(dx)
            dya.append(dy)


            Nxi=Nxc+Nxcb+int(dx/Xpix)
            Nyi=Nyc+Nycb+int(dy/Ypix)

            if dx<0:
                Nxi=Nxi-1
            if dy<0:
                Nyi=Nyi-1


            if Nxi<Nx and Nyi<Ny and Nxi>0 and Nyi>0:
                image[Nyi*Nx+Nxi]=image[Nyi*Nx+Nxi]+1
                if maxV<image[Nyi*Nx+Nxi]:
                    maxV=image[Nyi*Nx+Nxi]

        image=np.array(image)/float(maxV)*self.ScreenSpec[nn][4]

        image=list(image)

        dxa=np.array(dxa)
        dya=np.array(dya)
        bsx=dxa.std()*1.0e6
        bsy=dya.std()*1.0e6 # in micrometers


        return image,bsx,bsy
        

    def ScreenImageROIfill(self,n,Roi,image):
        # Method for Virtual Accelerator
        if '.MARK' in n:
            #nm=n
            nn=n.replace('.MARK','')
        else:
            #nm=n+'.MARK'
            nn=n

        Nx=self.ScreenSpec[nn][0]
        Ny=self.ScreenSpec[nn][1]
        
        if Roi[0]>Roi[1]:
            xmax=Roi[0]
            xmin=Roi[1]
        else:
            xmax=Roi[1]
            xmin=Roi[0]

        if Roi[2]>Roi[3]:
            ymax=Roi[2]
            ymin=Roi[3]
        else:
            ymax=Roi[3]
            ymin=Roi[2]

        if xmax>Nx-1:
            #return [0]*Nx*Ny
            xmax=Nx-1
        if ymax>Ny-1:
            #return [0]*Nx*Ny
            ymax=Ny-1
        if xmin<0:
            #return [0]*Nx*Ny
            xmin=0
        if ymin<0:
            #return [0]*Nx*Ny
            ymin=0

        imageROI=[]
        for yi in range(0,Ny):
            if yi>=ymin and yi<=ymax:
                xline=image[yi*Nx:yi*Nx+Nx]
                xlinecut=xline[xmin:xmax+1]
                imageROI.append(xlinecut)


        return imageROI

    def ScreenImageROI(self,n,Roi,image):
        # Method for Virtual Accelerator
        if '.MARK' in n:
            #nm=n
            nn=n.replace('.MARK','')
        else:
            #nm=n+'.MARK'
            nn=n


        Nx=self.ScreenSpec[nn][0]
        Ny=self.ScreenSpec[nn][1]
        
        if Roi[0]>Roi[1]:
            xmax=Roi[0]
            xmin=Roi[1]
        else:
            xmax=Roi[1]
            xmin=Roi[0]

        if Roi[2]>Roi[3]:
            ymax=Roi[2]
            ymin=Roi[3]
        else:
            ymax=Roi[3]
            ymin=Roi[2]

        if xmax>Nx-1:
            #return [0]*Nx*Ny
            xmax=Nx-1
        if ymax>Ny-1:
            #return [0]*Nx*Ny
            ymax=Ny-1
        if xmin<0:
            #return [0]*Nx*Ny
            xmin=0
        if ymin<0:
            #return [0]*Nx*Ny
            ymin=0

        imageROI=[]
        for yi in range(0,ymax-ymin+1):
            xline=image[yi*(xmax-xmin+1):(yi+1)*(xmax-xmin+1)]
            xlinecut=xline[0:xmax-xmin+1]
            imageROI.append(xlinecut)


        return imageROI

    def fillDiagnostics(self,EC=None,SF=None,EM=None,SM=None,tw=None,LT=None,NoBeam=0):
        # Method for Virtual Accelerator
        # Only 1 bunch for now...
        if not EM:
            return
        if not SM:
            return
        if not EC:
            return
        if not SF:
            return
        if not tw:
            return


        Screens,ScreenIn=EC.get('Screen:GET-SCREEN')
        Stoppers,StopperIn=EC.get('BeamStopper:IN-OUT')


        BPMs=EC.getDeviceList('BPM')


        BPMx=[0]*len(BPMs)
        BPMy=[0]*len(BPMs)
        BPMq=[0]*len(BPMs)
        BPMval=[0]*len(BPMs)

        for ib in range(0,10):
            BPMval[ib]=1
            BPMq[ib]=tw.BCURRENT/1e-12 # C to pC

        if NoBeam:
            for ib in range(0,10):
                BPMval[ib]=0
                BPMq[ib]=0
            EC.put('BPM:Q1-VALID',BPMval)
            EC.put('BPM:X1-VALID',BPMval)
            EC.put('BPM:Y1-VALID',BPMval)
            EC.put('BPM:SIM-X1',BPMx)
            EC.put('BPM:SIM-Y1',BPMy)
            EC.put('BPM:Q1',BPMq)
            if len(self.ScreenFilled):
                for s in self.ScreenFilled:
                    EC.putScreenImage(s,[0]) # Flush screen image since there is no beam
                self.ScreenFilled=[]
            return

        # Check if the beam goes through BCs
        dn,I=EC.get('Bend:I-SET')
        angle=SM.BendI2Angle(dn,I,EM) # The bending angle for the true beam energy that Virtual accelerator knows.
        dang = dict(zip(dn, angle))
        BCfound=[]
        for bc in self.SFBC:
            if len(bc)==5:
                if bc[0].Name in tw.NAME:
                    BCfound.append(bc)

        
        term=None
        BCBPMx={}
        #for bc in BCfound:
        for bi in range(0,len(BCfound)):
            bc=BCfound[bi]
            sec=bc[0].Name.split('.')[0]
            for bcc in EC.Chicane:
                if sec in bcc:
                    dn,values=EC.get(bcc)
                    pos=values[1] # Set angle of the BC that may not corresponds to the bending angle, dang.
                    angleset=math.atan(pos/1000/bc[4])*180.0/math.pi # an approximation
                    anglemag=dang[bc[0].Name]
                    # Rough modeling... 
                    if bc[4]*abs(abs(anglemag)-abs(angleset))*math.pi/180>0.015: 
                        if term==None:
                            term=bc[0].Name
                    #if abs(anglemag)>0: 
                    #    dPP=(abs(angleset)-abs(anglemag))/abs(anglemag)  # dP/P
                    #else:
                    #    dPP=0
                    bcbpm=SF.BCBPM[bc[0].Name]
                    bpmname=bcbpm[0].Name+'.MARK' # BPM name in Twiss
                    if bpmname in tw.NAME:
                        arm=bcbpm[2]
                        if arm>0:
                            BCBPMx[bpmname]=arm*(math.tan(abs(anglemag*math.pi/180))-math.tan(abs(angleset*math.pi/180)))*1000
                    bpmname=bcbpm[1].Name+'.MARK' # BPM name in Twiss
                    if bpmname in tw.NAME:
                        arm=bcbpm[2]
                        if arm>0:
                            BCBPMx[bpmname]=arm*(math.tan(abs(anglemag*math.pi/180))-math.tan(abs(angleset*math.pi/180)))*1000
                    

        Alive=1
        for n in tw.NAME:
            try:
                dn=n[0:15]
                if EM.Energy[dn][0]+EM.Energy[dn][1]<0:
                    Alive=0
            except:
                'Nothing to do'

            if n==term: #BC set angle does not match to the BC dipole field
                Alive=0

            if n.replace('.MARK','') in Screens:
                if ScreenIn[Screens.index(n.replace('.MARK',''))]!='Out' and Alive==1:
                    self.ScreenFilled.append(n.replace('.MARK','')) # Keep info if the screen is filled or not for flushing
                    # Tempolary beam size included
                    image,bsx,bsy=self.CreateScreenImage(EM,n,tw,LT,SF)
                    a,roi=EC.get(n.replace('.MARK',''))
                    roi[4]=bsx
                    roi[5]=bsy
                    EC.put(n.replace('.MARK',''),roi)
                    # Assuming the following order.  If not, the corresponding indices have to be found from list a. 
                    Roi=[roi[0],roi[1],roi[2],roi[3]]
                    imageROI=self.ScreenImageROIfill(n,Roi,image)
                    imageROI=list(chain.from_iterable(imageROI))
                    EC.putScreenImage(n.replace('.MARK',''),imageROI)
                    Alive=0
                elif ScreenIn[Screens.index(n.replace('.MARK',''))]=='Out' or Alive==0:
                    if n.replace('.MARK','') in self.ScreenFilled:
                        EC.putScreenImage(n.replace('.MARK',''),[0]) # Flush the image since there is no beam on the screen
                        a,roi=EC.get(n.replace('.MARK',''))
                        roi[4]=0
                        roi[5]=0
                        EC.put(n.replace('.MARK',''),roi)
                        self.ScreenFilled.remove(n.replace('.MARK',''))

            if n.replace('.MARK','') in Stoppers:
                if StopperIn[Stoppers.index(n.replace('.MARK',''))]=='In':
                    Alive=0
            if 'DRIFT' not in n:
                if abs(tw.X[tw.indx[n]])>0.01 or abs(tw.Y[tw.indx[n]])>0.01:
                    # constant aperture at 10 mm
                    Alive=0
                if abs(tw.BETX[tw.indx[n]])>3000 or abs(tw.BETY[tw.indx[n]])>3000:
                    # The beam is not under control...
                    Alive=0
            
            if n.replace('.MARK','') in BPMs:
                if Alive:
                    indx=BPMs.index(n.replace('.MARK',''))
                    BPMq[indx]=tw.BCURRENT/1e-12 # C to pC
                    BPMx[indx]=tw.X[tw.indx[n]]*1e3 # m to mm
                    BPMy[indx]=tw.Y[tw.indx[n]]*1e3
                    BPMval[indx]=1

                    if n in BCBPMx.keys():
                        BPMx[indx]=BPMx[indx]+BCBPMx[n]
                        

        EC.put('BPM:Q1-VALID',BPMval)
        EC.put('BPM:X1-VALID',BPMval)
        EC.put('BPM:Y1-VALID',BPMval)
        EC.put('BPM:SIM-X1',BPMx)
        EC.put('BPM:SIM-Y1',BPMy)
        EC.put('BPM:Q1',BPMq)

        
        if len(self.ScreenFilled):
            sremoved=[]
            for s in self.ScreenFilled:
                if s+'.MARK' not in tw.NAME:
                    EC.putScreenImage(s,[0]) # Flush screen image since there is no beam on the screen
                    a,roi=EC.get(s)
                    roi[4]=0
                    roi[5]=0
                    EC.put(s,roi)
                    sremoved.append(s)
            for s in sremoved:
                self.ScreenFilled.remove(s)


    def connectChannels(self,EC):
        # Method for Virtual accelerator
        # Connect dipole magnets that are excited by single power supply.

        def cb_ps(h):
            s=info[h]
            ename=EC.prefix+s.replace('.','-')+':ROI-XMIN'
            c=EC.cafe.getPVCache(ename)
            xmin=c.value[0]
            ename=EC.prefix+s.replace('.','-')+':ROI-XMAX'
            c=EC.cafe.getPVCache(ename)
            xmax=c.value[0]
            ename=EC.prefix+s.replace('.','-')+':ROI-YMIN'
            c=EC.cafe.getPVCache(ename)
            ymin=c.value[0]
            ename=EC.prefix+s.replace('.','-')+':ROI-YMAX'
            c=EC.cafe.getPVCache(ename)
            ymax=c.value[0]

            width=abs(xmax-xmin)+1
            height=abs(ymax-ymin)+1
            ename=EC.prefix+s.replace('.','-')+':WIDTH'
            EC.put1(ename,width)
            ename=EC.prefix+s.replace('.','-')+':HEIGHT'
            EC.put1(ename,height)


        handle=[]
        info={}
        EC.cafe.openPrepare()
        Screen=EC.getDeviceList('Screen')
        for s in Screen:
            ename=EC.prefix+s.replace('.','-')+':ROI-XMAX'
            h=EC.cafe.open(ename)
            handle.append(h)
            info[h]=s
            ename=EC.prefix+s.replace('.','-')+':ROI-XMIN'
            h=EC.cafe.open(ename)
            handle.append(h)
            info[h]=s
            ename=EC.prefix+s.replace('.','-')+':ROI-YMAX'
            h=EC.cafe.open(ename)
            handle.append(h)
            info[h]=s
            ename=EC.prefix+s.replace('.','-')+':ROI-YMIN'
            h=EC.cafe.open(ename)
            handle.append(h)
            info[h]=s
            ename=EC.prefix+s.replace('.','-')+':WIDTH'
            h=EC.cafe.open(ename)
            handle.append(h)
            info[h]=s
            ename=EC.prefix+s.replace('.','-')+':HEIGHT'
            h=EC.cafe.open(ename)
            handle.append(h)
            info[h]=s

        EC.cafe.openNowAndWait(10)
        i=0

        EC.cafe.openMonitorPrepare()
        for i in range(0,len(handle)):
            h=handle[i]
            m0=EC.cafe.monitorStart(h, cb=cb_ps, dbr=EC.cyca.CY_DBR_PLAIN, mask=EC.cyca.CY_DBE_VALUE)
            i=i+1
        EC.cafe.openMonitorNowAndWait(10)

        sleep(0.1)
