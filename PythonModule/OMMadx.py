import math
import os
import shutil
from subprocess import call
from OMMadxLattice import MadXLattice
from OMType import *

import numpy as np
from time import sleep
import sys

import zmq
import zlib
import pickle

#----- thefollowing is a quick and dirty implementation with zmq. Masamitsu will fix it
sys.path.append('/afs/psi.ch/intranet/SF/Applications/on-line_model/v2.0.0/lib/python3') 
#also LD_LIBRARY_PATH needs to be set to include libmadx-linux64.so and the intel compiler
#----- end of quick and dirty implementation

try:
    from OMSwissFELmagnet import *
except:
    None


#--------
# MAD Interface


class dummyClass:
    def __init__(self,inDict):
        self.__dict__.update(inDict)


class MadX(MadXLattice):
    def __init__(self,initialize_mad=1):
        MadXLattice.__init__(self)

        self.BRMK={}
        self.switch=0
        if initialize_mad==1:
            from pythonmadx import madx
            self.pmad=madx()
        else:
            if initialize_mad==0:
                self.portNumber=15002
                self.host='sf-dds-02'
            else:
                self.portNumber=initialize_mad
                self.host=''
            'MADX Zero would be use.'
            self.switchOutput('Server')
        

        self.bellows=[]
        


        #self.flog=open('logmadx.txt','w')
        #self.flog.close()

        self.MADzero=None

        self.matlab=0

    def finish(self):
        self.pmad=None

    def command(self,txt):
        #self.flog=open('logmadx.txt','a')
        #self.flog.write(txt+'\n')
        #self.flog.close()
        self.pmad.command(txt)
    
    def sendCommand(self,line):
        #self.flog=open('logmadx.txt','a')
        #self.flog.write(line+'\n')
        #self.flog.close()
        if self.matlab:
            self.mcommand.append(line)
            return 

        if not self.MADzero:
            self.MADzero=1
            self.zcontext = zmq.Context()
            self.zsocket=self.zcontext.socket(zmq.REQ)
            self.zsocket.connect("tcp://"+self.host+":"+str(self.portNumber))

        if line=='initialize':
            self.zsocket.send_string('status')
            m=self.zsocket.recv().decode()
            if 'Old' in m:
                self.zsocket.send_string('exit')
                m=self.zsocket.recv().decode()
                sleep(1)
            self.zsocket.send_string(line)
            m=self.zsocket.recv().decode()
            return m
        else:
            #self.flog=open('logmadx.txt','a')
            #self.flog.write(line+'\n')
            #self.flog.close()
#            print(line)
            self.zsocket.send_string(line)
            m=self.zsocket.recv()
            try:
                m=m.decode()
            except:
                'm is twiss_table'

            return m

    
 
    def twiss_table(self,columns):
         return self.pmad.twiss_table(columns)
 




    def updateSFBC(self,SF):
        self.SFBC=SF.BC

    def deleteSFBC(self,SF):
        self.SFBC=None

    

    def streamFile(self,filename):
        fin=open(filename,'r')
        for line in fin.readlines():
            self.write(line)
        fin.close()    
            
    def write(self,line):
        if isinstance(line,list):
            for l in line:
                self.write(l)
        else:
            if self.switch==1:
                self.command(line)
            elif self.switch==2:
                return self.sendCommand(line)
            else:
                self.fid.write(line)

        

    def updateTDSmatrixFromEpics(self,EC=None,EM=None,PhaseOffset=None):
        # Used in Virtual Accelerator but general function

        if not EC:
            return
        if not EM:
            return

        # Possibly TDSs could be in a different group from RFs in OMEpicsChannel... to be discussed
        # For now, it is assumed in the same group to RF

        #dn,Gradient=EC.get('RF:GRADIENT')
        #dn,Phase=EC.get('RF:PHASE')
        #dn,PhaseON=EC.get('RF:PHASE-ONCREST')
        
        dn=EC.getDeviceList('RF')

        
        #klystron,Status=EC.get('RFsystem:GET-RF-READY-STATUS')
        klystron,Status=EC.get('RFsystem:GET-RF-READY-STATUS',0,'int')
        klystron,Mode=EC.get('RFsystem:GET-STATION-MODE')
        klystron,State=EC.get('RFsystem:GET-STATION-STATE')

        klystron,V=EC.get('RFsystem:GET-ACC-VOLT')
        klystron,KPhase=EC.get('RFsystem:GET-VSUM-PHASE')
        klystron,KPhaseON=EC.get('RFsystem:SET-ON-CREST-VSUM-PHASE')
        klystron,KPhaseB=EC.get('RFsystem:GET-BEAM-PHASE')

        # Check if the rf stations are in operation
        kok={}
        for i in range(0,len(klystron)):
            sec=klystron[i].split('.')[0]
            Length=EC.Cavities[klystron[i]][1]
            #if Mode[i]=='Operation' and State[i]=='On' and Status[i]=='OK':
            if State[i]=='RF ON PHAFB' or State[i]=='RF ON AMPFB' or State[i]=='RF ON FB':
                State[i]=='RF ON'
            if Mode[i]=='NORMAL' and State[i]=='RF ON' and Status[i]:
                kok[sec]=[V[i]/(len(EC.Cavities[klystron[i]])-2)/Length,KPhase[i],KPhaseON[i],KPhaseB[i]]
            else:
                kok[sec]=[0,KPhase[i],KPhaseON[i],KPhaseB[i]]

        Phase=[0]*len(dn)
        PhaseON=[0]*len(dn)
        PhaseB=[0]*len(dn)
        Gradient=[0]*len(dn)
        for i in range(0,len(dn)):
            sec=dn[i].split('.')[0]
            Gradient[i]=kok[sec][0]
            Phase[i]=kok[sec][1]
            PhaseON[i]=kok[sec][2]
            PhaseB[i]=kok[sec][3]
            

        TDSindx=[]
        for d in dn:
            if 'TDS' in d:
                TDSindx.append(dn.index(d))
        TDSon=[]
        for i in TDSindx:
            if Gradient[i]>0:
                TDSon.append([dn[i],1])
                p0=EM.Energy[dn[i]][0]
                if PhaseOffset:
                    Phase[i]=Phase[i]+PhaseON[i]-PhaseOffset[i]
                dP=Gradient[i]*math.sin(Phase[i]*math.asin(1)/90)*EM.Energy[dn[i]][2]
                # Approximated implementation to compute only for the beam trajectory
                # Need to define all the matrix element if used for other purpose...
                m45=dP/p0
                self.write("%s->RM45=%f;" % (dn[i],m45))  
            else:
                TDSon.append([dn[i],0])
                self.write("%s->RM45=%f;" % (dn[i],0))  

        return TDSon

    def updateRFmatrix(self,EM=None):
        # Used in Virtual Accelerator but general function
        if not EM:
            return
        
        for k in EM.Energy.keys():
            if 'RACC' in k:
                p0=EM.Energy[k][0]
                dP=EM.Energy[k][1]
                L=EM.Energy[k][2]
                if dP==0 or p0<=0 or (p0+dP)<0:
                    m11=1.0
                    m12=L
                    m21=0.0
                    m22=1.0
                else:
                    m11=1
                    m12=p0*L/dP*math.log((p0+dP)/p0)
                    m21=0
                    m22=p0/(p0+dP) 
                    e11=1
                    e12=0
                    e21=-0.5*dP/p0/L
                    e22=1 
                    d11=m11*e11+m12*e21
                    d12=m11*e12+m12*e22
                    d21=m21*e11+m22*e21
                    d22=m21*e12+m22*e22
                    e21=0.5*dP/(p0+dP)/L
                    m11=e11*d11+e12*d21
                    m12=e11*d12+e12*d22
                    m21=e21*d11+e22*d21
                    m22=e21*d12+e22*d22
                self.write("%s->RM11=%f;" % (k,m11))   
                self.write("%s->RM12=%f;" % (k,m12))   
                self.write("%s->RM21=%f;" % (k,m21))   
                self.write("%s->RM22=%f;" % (k,m22))   
                self.write("%s->RM33=%f;" % (k,m11))   
                self.write("%s->RM34=%f;" % (k,m12))   
                self.write("%s->RM43=%f;" % (k,m21))   
                self.write("%s->RM44=%f;" % (k,m22))  
        
    def reflectRFmatrix(self,EM=None):
        if not EM:
            return
        
        for k in EM.Energy.keys():
            if 'RACC' in k:
                p0=EM.Energy[k][0]+EM.Energy[k][1]
                dP=-EM.Energy[k][1]
                L=EM.Energy[k][2]
                if dP==0 or p0<0 or (p0+dP)<0:
                    m11=1.0
                    m12=L
                    m21=0.0
                    m22=1.0
                else:
                    m11=1
                    m12=p0*L/dP*math.log((p0+dP)/p0)
                    m21=0
                    m22=p0/(p0+dP) 
                    e11=1
                    e12=0
                    e21=-0.5*dP/p0/L
                    e22=1 
                    d11=m11*e11+m12*e21
                    d12=m11*e12+m12*e22
                    d21=m21*e11+m22*e21
                    d22=m21*e12+m22*e22
                    e21=0.5*dP/(p0+dP)/L
                    m11=e11*d11+e12*d21
                    m12=e11*d12+e12*d22
                    m21=e21*d11+e22*d21
                    m22=e21*d12+e22*d22
                self.write("%s->RM11=%f;" % (k,m11))   
                self.write("%s->RM12=%f;" % (k,m12))   
                self.write("%s->RM21=%f;" % (k,m21))   
                self.write("%s->RM22=%f;" % (k,m22))   
                self.write("%s->RM33=%f;" % (k,m11))   
                self.write("%s->RM34=%f;" % (k,m12))   
                self.write("%s->RM43=%f;" % (k,m21))   
                self.write("%s->RM44=%f;" % (k,m22))  

    def updateQuadKfromEpics(self,EC=None,SM=None,SF=None,EM=None,updateSF=0):
        # Used in Virtual Accelerator but general function
        # updateSF=-1: update only madx, updateSF=0: update only SF, updateSF=1 or other number: update madx and SF,   
        if EC==None or SM==None:
            return
        
        if EM:
            # If EM is given, compute KL from I based on the energy info stored
            dn,I=EC.get('Quad:I-SET')
            QuadKL=SM.QuadrupoleI2KL(dn,I,EM)
        else:
            dn,QuadKL=EC.get('Quad:KL')
        QuadK=SM.QuadrupoleKL2K(dn,QuadKL)
        

        if updateSF==-1: 
            for i in range(0,len(dn)):
                d=dn[i]
                k=QuadK[i]
                self.write(d+'.k1:='+str(k)+';')
        elif updateSF==0 and SF!=None:
            SF.setGroup(dn,'k1',QuadK)
        elif updateSF and SF!=None:
            for i in range(0,len(dn)):
                d=dn[i]
                k=QuadK[i]
                self.write(d+'.k1:='+str(k)+';')
            SF.setGroup(dn,'k1',QuadK)

    def updateBendAngleFromEpics(self,EC=None,SM=None,SF=None,EM=None,updateSF=0):
        # Used in Virtual Accelerator but general function
        # if updateSF==1, ele.Angle is also updated
        if EC==None or SM==None or SF==None:
            return
        
        if EM:
            # If EM is given, compute Angle from I based on the energy info stored
            dn,I=EC.get('Bend:I-SET')
            Angle=SM.BendI2Angle(dn,I,EM)
        else:
            dn,Angle=EC.get('Bend:ANGLE')
        for i in range(0,len(Angle)):
            Angle[i]=Angle[i]*EC.Bendp[i]

        if updateSF==-1: 
            for i in range(0,len(dn)):
                d=dn[i]
                ang=Angle[i]*math.pi/180.0 # Angle is in units of degree while in units of radian in madx
                self.write(d+'->ANGLE:='+str(ang)+';')
                ele=SF.getElement(d)
                if ele.e1!=0:
                    ang_e1=ang*ele.e1
                    self.write(d+'->e1:='+str(ang_e1)+';')
                if ele.e2!=0:
                    ang_e2=ang*ele.e2
                    self.write(d+'->e2:='+str(ang_e2)+';')

        elif updateSF==0 and SF!=None:
            SF.setGroup(dn,'angle',Angle)

        elif updateSF and SF!=None:
            for i in range(0,len(dn)):
                d=dn[i]
                ang=Angle[i]*math.pi/180.0 # Angle is in units of degree while in units of radian in madx
                self.write(d+'->ANGLE:='+str(ang)+';')
                ele=SF.getElement(d)
                if ele.e1!=0:
                    ang_e1=ang*ele.e1
                    self.write(d+'->e1:='+str(ang_e1)+';')
                if ele.e2!=0:
                    ang_e2=ang*ele.e2
                    self.write(d+'->e2:='+str(ang_e2)+';')
            SF.setGroup(dn,'angle',Angle)


            
    def updateCorrectorKickFromEpics(self,EC=None,SM=None,SF=None,EM=None,updateSF=0):
        # Used in Virtual Accelerator but general function
        if EC==None:
            return
        
        if EM:
            # If EM is given, compute Kick from I
            dn,I=EC.get('Corrector:I-SET')
            CorrKick=SM.CorrectorI2Kick(dn,I,EM)
        else:
            dn,CorrKick=EC.get('Corrector:KICK')
 

        for i in range(0,len(dn)):
            d=dn[i]
            k=CorrKick[i]
            if 'MCRX' in d:
                self.write(d+'.corx:='+str(k)+';')
            elif 'MCRY' in d:
                self.write(d+'.cory:='+str(k)+';')
            
        #if updateSF and SF!=None:
        #    SF.setGroup(dn,***,CorrKick)
        # This is not straightforward because most correctors are integrated into quad or bend, and they dont appear explicitly in the layout...

    def zeroCorrectorKick(self,EC=None):
        # Used in Virtual Accelerator but general function
        if EC==None:
            return

        dn=EC.getDeviceList('Corrector') 

        for i in range(0,len(dn)):
            d=dn[i]
            if 'MCRX' in d:
                self.write(d+'.corx:=0.0;')
            elif 'MCRY' in d:
                self.write(d+'.cory:=0.0;')
            


    def updatePQuadKfromEpics(self,EC=None,SM=None,SF=None,EM=None,updateSF=0):
        # Used in Virtual Accelerator but general function
        if EC==None or SM==None:
            return
        
        if EM:
            # If EM is given, compute KL from I based on the energy info stored
            dn=EC.getDeviceList('PQuad')
            PQuadKL=SM.PQuadrupoleKL(dn,EM)
        else:
            dn,PQuadKL=EC.get('PQuad:KL')

        PQuadK=SM.QuadrupoleKL2K(dn,PQuadKL) # This method is valid also for PQuad
        
        dnp,State=EC.get('PQuadMotor:X')

        if updateSF==-1:
            for i in range(0,len(dn)):
                if not State[i]: # X==0 is 'out'
                    PQuadK[i]=0
                d=dn[i]
                k=PQuadK[i]
                self.write(d+'.k1:='+str(k)+';')
        elif updateSF==0 and SF!=None:
            SF.setGroup(dn,'k1',PQuadK)
        elif updateSF and SF!=None:
            for i in range(0,len(dn)):
                if not State[i]: # X==0 is 'out'
                    PQuadK[i]=0
                d=dn[i]
                k=PQuadK[i]
                self.write(d+'.k1:='+str(k)+';')

            SF.setGroup(dn,'k1',PQuadK)



    def SequenceFromEpics(self,SF=None,EC=None,SM=None, EM=None):
        # Method for Virtual Accelerator
        if SF==None or EC==None or SM==None or EM==None:
            return

        Trace=SM.BeamPathFromEpics(EC,SF,EM)
        
        if Trace[0][1]==1:
            # Injector dump is on, and thus no beam for madx (simulator for high-energy part, that is after SB02)
            return []
        
        deadend=0
        first=1
        params,values=EC.get('SINLH03.EXIT')
        EX=values[4] # this field is for the geometrical emittance, but EXN and EYN are not available in twiss table... 
        EY=values[5] # so, let's use it...
        charge=values[6]
        self.command('Beam,particle=electron,bcurrent='+str(charge)+',EX='+str(EX)+',EY='+str(EY)+';')
        seqs=[]
        for i in range(0,len(Trace)):
            SID=str(Trace[i][0])
            self.command('use,sequence=SEQ'+SID+';')
            self.command('select, flag=twiss, clear;')
            if Trace[i][1]==-1 and deadend==0:
                if first:
                    self.command('seqedit, sequence=SEQ'+SID+';')
                    self.command('flatten;')
                    self.command('extract,sequence=SEQ'+SID+',from=SINLH03$END,to=SEQ'+SID+'.END,newname=SEQ'+SID+'N;')
                    self.command('endedit;')
                    seqs.append('SEQ'+SID+'N')                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
                    first=0
                else:
                    seqs.append('SEQ'+SID)
            elif deadend==0: # Cheking branch bend excitation
                ele=SF.getElement(Trace[i][3])
                angle=Trace[i][4]
                dangle=ele.design_angle
                if dangle<0:
                    angle=-angle
                L=ele.Length
                x=L/math.sin(dangle*math.pi/180)*(1-math.cos(dangle*math.pi/180))*(1.0-abs(angle/dangle))
                xp=math.sin(dangle*math.pi/180)*(1.0-abs(angle/dangle))
                #x=L/math.sin(dangle*math.pi/180)*(1-math.cos(dangle*math.pi/180))-L/math.sin(angle*math.pi/180)*(1-math.cos(angle*math.pi/180))
                #xp=(angle-dangle)*math.pi/180


                if abs(x)>0.01: # more than 1 cm mismatch to the next beam line
                    deadend=1
                
                self.command('seqedit, sequence=SEQ'+SID+';')
                self.command('flatten;')
                if first:
                    self.command('extract,sequence=SEQ'+SID+',from=SINLH03$END,to='+Trace[i][2]+',newname=SEQ'+SID+'N;')
                    first=0
                else:
                    self.command('extract,sequence=SEQ'+SID+',from=SEQ'+SID+'$START,to='+Trace[i][2]+',newname=SEQ'+SID+'N;')
                self.command('endedit;')
                
                self.command('use,sequence=SEQ'+SID+'N;')
                self.command(Trace[i][3]+'->ANGLE:='+str(Trace[i][4]*math.pi/180)+';') # Setting angle without changing the dipole length...
                if angle==dangle:
                    seqs.append('SEQ'+SID+'N')
                else:
                    seqs.append(['SEQ'+SID+'N',x,xp]) # position and angle error at the exit
        return seqs,Trace

    def concatenateTwiss(self,tws):
        # Method for Virtual Accelerator
        betx_max=0.0
        bety_max=0.0
        dx_max=0.0
        dx_rms=[]
        dy_max=0.0
        dy_rms=[]
        
        x_max=0.0
        x_rms=[]
        y_max=0.0
        y_rms=[]
        
        length=0.0
        Q1=0.0
        dQ1=0.0
        Q2=0.0
        dQ2=0.0


        
        indict={}
        indict['indx']={}
        nelem=0
        for tw in tws:
            memberAll=dir(tw)
            member=[x for x in memberAll if '__' not in x] # Beauty of Python!!!
            for m in sorted(member,reverse=True): # the sorting is to have LENGTH after S
                a=tw.__dict__[m]
                if m=='BETXMAX' and a>betx_max:
                    betx_max=a
                if m=='BETYMAX' and a>bety_max:
                    bety_max=a
                if m=='DXMAX' and a>dx_max:
                    dx_max=a
                if m=='DYMAX' and a>dy_max:
                        dy_max=a
                if m=='XMAX' and a>x_max:
                        x_max=a
                if m=='YMAX' and a>y_max:
                        y_max=a
                if m=='Q1':
                    Q1=Q1+a
                    dQ1=a
                if m=='Q2':
                    Q2=Q2+a
                    dQ2=a
                if m not in indict.keys():
                    if isinstance(a,list):
                        indict[m]=[]
                    elif m!='indx':
                        indict[m]=a
                if m=='S':
                    for i in range(0,len(a)):
                        a[i]=a[i]+length
                if m=='LENGTH':
                    length=length+a
                if m=='MUX':
                    for i in range(0,len(a)):
                        a[i]=a[i]+(Q1-dQ1)
                    indict[m]=indict[m]+a
                elif m=='MUY':
                    for i in range(0,len(a)):
                        a[i]=a[i]+(Q2-dQ2)
                    indict[m]=indict[m]+a
                elif isinstance(a,list):
                    indict[m]=indict[m]+a
                    if m=='DX':
                        dx_rms=dx_rms+a
                    elif m=='DY':
                        dy_rms=dy_rms+a
                    elif m=='X':
                        x_rms=x_rms+a
                    elif m=='Y':
                        y_rms=y_rms+a
                    
                    
                if m=='indx':
                    for n in tw.NAME:
                        indict['indx'][n]=a[n]+nelem
                    nelem=nelem+len(tw.NAME)

        dx_rms=np.array(dx_rms)
        dx_rms=dx_rms.std()
        dy_rms=np.array(dy_rms)
        dy_rms=dy_rms.std()
        indict['DXRMS']=dx_rms
        indict['DYRMS']=dy_rms

        x_rms=np.array(dx_rms)
        x_rms=dx_rms.std()
        y_rms=np.array(dy_rms)
        y_rms=dy_rms.std()
        indict['XRMS']=x_rms
        indict['YRMS']=y_rms

        indict['Q1']=Q1
        indict['Q2']=Q2
        indict['BETXMAX']=betx_max
        indict['BETYMAX']=bety_max
        indict['DXMAX']=dx_max
        indict['DYMAX']=dy_max
        indict['XMAX']=x_max
        indict['YMAX']=y_max


        indict.pop('__init__',None)
        indict.pop('__doc__',None)
        indict.pop('__module__',None)
        



        return dummyClass(indict)


        

    def runSequences(self,seqs,initialCondition,misalign=None,betacheck=0):
        # Method for Virtual Accelerator
        # format for initialCondition={'BETX':10.0,'ALFX':0.0,'BETY':10.0,'ALFY':0.0}
        # format for misalign={'SARUN03.MQUA100':[dx,dy],...}
        # Need to run the sequence without misalignments and correctors tocheck if the beta is not too large
        # otherwise the Twiss command fails and a fatal error occurs because pythonmadx extract Twiss table directly from memory... 
        # betacheck=1, flat run to check the beta function
        # betacheck=[seq_name,endelem], run 


        terminate=[] # [sequence,element] The beta function (x or y) is larger than 3000 m at the element in the sequence. Found from the flat run. 
        first=1
        tws=[]
        x_next=0
        px_next=0
        for s in seqs:
            try:
                betx=initialCondition['BETX']
            except:
                betx=10.0
            try:
                alfx=initialCondition['ALFX']
            except:
                alfx=0.0
            try:
                bety=initialCondition['BETY']
            except:
                bety=10.0
            try:
                alfy=initialCondition['ALFY']
            except:
                alfy=0.0
            try:
                dx=initialCondition['DX']
            except:
                dx=0.0
            try:
                dpx=initialCondition['DPX']
            except:
                dpx=0.0
            try:
                dy=initialCondition['DY']
            except:
                dy=0.0
            try:
                dpy=initialCondition['DPY']
            except:
                dpy=0.0
            try:
                x=initialCondition['X']
            except:
                x=0.0
            try:
                px=initialCondition['PX']
            except:
                px=0.0
            try:
                y=initialCondition['Y']
            except:
                y=0.0
            try:
                py=initialCondition['PY']
            except:
                py=0.0


            if isinstance(s,list): # Bending magnet is not set to the design angle
                seq=s[0] 
            else:
                seq=s

            Range='#s/#e'
            if betacheck==1:
                x=0
                px=0
                y=0
                py=0
                x_next=0
                px_next=0
                misalign=None
            elif isinstance(betacheck,list):
                if seq==betacheck[0]:
                    Range='#s/'+betacheck[1]
                
            

            iniC='range='+Range+', t=1.0, betx='+str(betx)+', alfx='+str(alfx)+', bety='+str(bety)+', alfy='+str(alfy)+', x='+str(x)+', px='+str(px) \
                +', y='+str(y)+', py='+str(py)+', dx='+str(dx)+', dpx='+str(dpx)+', dy='+str(dy)+', dpy='+str(dpy) 
        

            if isinstance(s,list): # Bending magnet is not set to the design angle 
                x_next=s[1]
                px_next=s[2]
            else:
                x_next=0
                px_next=0


            self.command('use, sequence='+seq+';')
            self.command('select, flag=error, clear;') # Clean up if any error set
            self.command('select, flag=twiss, clear;')
            #self.command('select, flag=twiss, pattern="^s";') # This does not work since pythonMad is accessing to the table in the memory directly...
            comm='twiss, sequence='+seq+','+iniC+';'
            self.command(comm)
            tw=self.twiss_table(['NAME','S','BETX','ALFX','BETY','ALFY','X','PX','Y','PY','DX','DPX','DY','DPY','MUX','MUY','ANGLE','E1','E2','K1L'])

            if betacheck==1:
                for n in tw.NAME:
                    if (tw.BETX[tw.indx[n]]>3000 or tw.BETY[tw.indx[n]]>3000) and ('DRIFT' not in n) and len(terminate)==0:
                        terminate=[seq,n]
                        #sys.exit()
                        break

            if misalign:
                #find elements in the sequence
                ee=[]
                for e in misalign.keys():
                    for etw in tw.NAME:
                        if e in etw:
                            if ('$START' not in etw) and ('$END' not in etw) and ('CX' not in etw) and ('CY' not in etw):
                                tl=misalign[e]+[etw]
                                ee.append(tl)

                if len(ee):
                    # clean up any misalignment set
                    self.command('select, flag=error, clear;')
                    # and then assign the misalignments
                    self.command('eoption,add=true;')

                    for i in range(0,len(ee)-1):
                        dx=ee[i][0]-ee[i+1][0]
                        dy=ee[i][1]-ee[i+1][1]
                        ename=ee[i][2]
                        #if ('DBPM' in ename) or ('DSCR' in ename):
                        #    ename=ename+'.mark'
                        comm='select,flag=error,range='+ename+', pattern='+ename+';'
                        self.command(comm)
                        comm='ealign, dx='+str(dx)+', dy='+str(dy)+';'
                        self.command(comm)
                    dx=ee[-1][0]
                    dy=ee[-1][1]
                    ename=ee[-1][2]
                    #if ('DBPM' in ename) or ('DSCR' in ename):
                    #        ename=ename+'.mark'
                    comm='select,flag=error,range='+ename+', pattern='+ename+';'
                    self.command(comm)
                    comm='ealign, dx='+str(dx)+', dy='+str(dy)+';'
                    self.command(comm)

                    #repeat twiss with misalignment
                    #self.command('use, sequence='+seq+';') # N.B. This command eliminates the introduced misalignments!!!!!!!!!!!!!!!!!
                    self.command('select, flag=twiss, clear;')
                    comm='twiss, sequence='+seq+','+iniC+';'
                    self.command(comm)
                    tw=self.twiss_table(['NAME','S','BETX','ALFX','BETY','ALFY','X','PX','Y','PY','DX','DPX','DY','DPY','MUX','MUY'])
                    for e in ee:
                        if 'DBPM' in e[2]:
                            # N.B. misalignment, DX/DY, is not taken into account for monitor!
                            # therefore it is applied here
                            tw.X[tw.indx[e[2]]]=tw.X[tw.indx[e[2]]]-e[0]
                            tw.Y[tw.indx[e[2]]]=tw.Y[tw.indx[e[2]]]-e[1]
                    
                    

            initialCondition['BETX']=tw.BETX[len(tw.BETX)-1]
            initialCondition['ALFX']=tw.ALFX[len(tw.ALFX)-1]
            initialCondition['BETY']=tw.BETY[len(tw.BETY)-1]
            initialCondition['ALFY']=tw.ALFY[len(tw.ALFY)-1]
            initialCondition['X']=tw.X[len(tw.X)-1]
            initialCondition['PX']=tw.PX[len(tw.PX)-1]
            initialCondition['Y']=tw.Y[len(tw.Y)-1]
            initialCondition['PY']=tw.PY[len(tw.PY)-1]
            initialCondition['DX']=tw.DX[len(tw.DX)-1]
            initialCondition['DPX']=tw.DPX[len(tw.DPX)-1]
            initialCondition['DY']=tw.DY[len(tw.DY)-1]
            initialCondition['DPY']=tw.DPY[len(tw.DPY)-1]
            if isinstance(s,list): # Bending magnet is not set to the design angle
                initialCondition['X']=initialCondition['X']+s[1]
                initialCondition['PX']=initialCondition['PX']+s[2]
            tws.append(tw)

        if betacheck==1:
            if len(terminate)==0:
                return 0
            else:
                return terminate
        else:
            return self.concatenateTwiss(tws)


    def adjustBellowsFromEpics(self,EC=None):
        # Used in Virtual Accelerator but general function
        if EC==None:
            return
        

        for bc in EC.Chicane:
            dn,values=EC.get(bc)
            sdn=dn[0].split(':')[0].split('-')
            sec=sdn[len(sdn)-2]
            arm=None
            for bcm in self.SFBC:
                if sec in bcm[0].Name:
                    arm=bcm[4]
            for bell in self.bellows:
                if sec in bell:
                    pos=values[1]
                    anglerad=math.atan(pos/1000/arm)
                    dl=arm*(1.0/math.cos(anglerad)-1.0)
                    self.write(bell+'->R12='+str(dl)+';\n')  # N.B. This implementation does not change the length of BC in Twiss (or 'S') while the optic paramters will be varied properly. 
    def adjustBellows(self,EC=None):
        # Used in Virtual Accelerator but general function
        # Input is from SF (through shallow copy of the bc bending magnet, self.SFBC)
        if EC==None:
            return
        
        for bc in EC.Chicane:
            dn=EC.getDeviceList(bc)
            sdn=dn[0].split(':')[0].split('-')
            sec=sdn[len(sdn)-2]
            arm=None
            for bcm in self.SFBC:
                if sec in bcm[0].Name:
                    arm=bcm[4]
                    angle=bcm[0].angle
            for bell in self.bellows:
                if sec in bell:
                    anglerad=angle*math.pi/180
                    dl=arm*(1.0/math.cos(anglerad)-1.0)
                    self.write(bell+'->R12='+str(dl)+';\n')  

#-----------------------------------------------------------------------------
#  This is the essential part of OMMadx in version 2. The stuff above needs to be cleaned up.



    def getMatching(self,SF,indict,matlab=0):
        # as with getOptics and getMatrix the interface to the machine must be done before.
        # also matlab is thrown out

        if 'InitialCondition' not in indict.keys():
            return None
        if 'Start' not in indict.keys():
            return None
        if 'End' not in indict.keys():
            return None

        # getMatching uses ZMQ MAD sever unless MADX bugs are fixed.
        # for matlab, this method returns a list of MADX commands
        self.switchOutput(2) 
        
        

        if matlab:
            self.matlab=1
        else:
            self.matlab=0
            self.write('initialize') # Use always fresh MADX
        self.mcommand=[]


        # Build up the required sequence
        Start=indict['Start']
        End=indict['End']
        Target=indict['TargetLocation']

        s_ele=SF.getElement(indict['Start'])
        e_ele=SF.getElement(indict['End'])
        t_ele=SF.getElement(indict['TargetLocation'])
        


        if 'MQUA' in Start:
            if ('cor' in s_ele.__dict__) or ('corx' in s_ele.__dict__) or ('cory'in s_ele.__dict__):
                Start=Start+'.Q1'
        if 'MQUA' in End:
            if ('cor' in e_ele.__dict__) or ('corx' in e_ele.__dict__) or ('cory' in e_ele.__dict__):
                End=End+'.Q2'
        if 'MQUA' in Target:
            if ('cor' in t_ele.__dict__) or ('corx' in t_ele.__dict__) or ('cory' in t_ele.__dict__):
                Target=Target+'.Q2'
        
        if s_ele and ('enable' in s_ele.__dict__):
            if s_ele.enable:
                Start=Start+'.diag'
            else:
                Start=Start+'.mark'
        if e_ele and ('enable' in e_ele.__dict__):
            if e_ele.enable:
                End=End+'.diag'
            else:
                End=End+'.mark'
        if t_ele and ('enable' in t_ele.__dict__):
            if t_ele.enable:
                Target=Target+'.diag'
            else:
                Target=Target+'.mark'


        if 'END' in End.upper(): 
            section=End.split('.')
            sec=SF.getSection(section[0])
            path=sec.mapping
        else:
            path=e_ele.mapping[0]



        try:
            b=indict['Direction']
            if b<0:
                b=-1
            else:
                b=1
        except:
            b=1

        if b==-1:
            self.invertMatrix=True   # functionality added in OMMadxLattice.py

        line=SF.BeamPath(path)
        line.writeLattice(self,SF)   # Access function for "EnergyAt" is in the facility container now.
        self.invertMatrix=False      # make sure that next time it is in forward direction as default

        self.sendCommand('beam, particle=electron, energy=100;') # Need high energu to set beta=1
        if b==-1:
            self.sendCommand('use, sequence=swissfel;')
            self.sendCommand('seqedit, sequence=swissfel;')
            self.sendCommand('flatten;')
            self.sendCommand('reflect;')
            self.sendCommand('endedit;')
            Start,End=End,Start

        Range=Start+'/'+End

        if len(indict['InitialCondition'])==4:
            [bx,ax,by,ay]=indict['InitialCondition']
            dx=0
            dpx=0
            dy=0
            dpy=0
            x=0
            px=0
            y=0
            py=0
        elif len(indict['InitialCondition'])==8:
            [bx,ax,by,ay,dx,dpy,dy,dpy]=indict['InitialCondition']
            x=0
            px=0
            y=0
            py=0
        elif len(indict['InitialCondition'])==12:
            [bx,ax,by,ay,dx,dpy,dy,dpy,x,py,y,py]=indict['InitialCondition']

 
        iniC=', t=1.0, betx='+str(bx)+', alfx='+str(b*ax) \
                +', bety='+str(by)+', alfy='+str(b*ay) +',x='+str(x) \
                +', px='+str(b*px)+', y='+str(y)+', py='+str(b*py)+', dx='+str(dx) \
                +', dpx='+str(b*dpx)+', dy='+str(dy)+', dpy='+str(b*dpy) 
            
        
        
        qknobs=indict['QuadKnobs']
        cons=indict['Constraints']

        self.sendCommand('beam, particle=electron, energy=100;')
        self.sendCommand('use, sequence=swissfel;')
        #self.sendCommand('select, flag=error, clear;')
        #self.sendCommand('select, flag=twiss, clear;')

        comm='MATCH,SEQUENCE=swissfel,RANGE='+Range \
             +',betx='+str(bx)+',alfx='+str(b*ax)+',bety='+str(by)+',alfy='+str(b*ay)+';\n'
        self.sendCommand(comm)

        for q in qknobs:
            self.sendCommand('VARY,NAME='+q+'.k1,STEP=0.0001;\n')

        for c in cons:
            self.sendCommand('CONSTRAINT,SEQUENCE=swissfel,RANGE='+Target+','+c+';\n')


        self.sendCommand('LMDIF,CALLS=1000,TOLERANCE=1e-20;\n');
        self.sendCommand('ENDMATCH;\n');
        self.sendCommand('VAL_MATCH = TAR;\n');

        
        
        if self.matlab:
            self.sendCommand('select, flag=twiss, column=NAME,S,BETX,ALFX,BETY,ALFY,X,PX,Y,PY,DX,DPX,DY,DPY,MUX,MUY,K1L;')
        else:
            self.sendCommand('column:NAME,S,BETX,ALFX,BETY,ALFY,X,PX,Y,PY,DX,DPX,DY,DPY,MUX,MUY,K1L')



        comm='twiss, range='+Start+'/'+End+', sequence=swissfel'+iniC+';'
        z=self.sendCommand(comm)

        if self.matlab:
            return self.mcommand
        else:
            p = zlib.decompress(z)
            tw=pickle.loads(p)


            QKL=[]
            for q in qknobs:
                try:
                    QKL.append(tw.K1L[tw.indx[q]])
                except:
                    QKL.append(2.0*tw.K1L[tw.indx[q+'.Q1']])

            lg=[]
            if 'Log' in indict.keys():
                if indict['Log']:
                    z=self.sendCommand('log')
                    p = zlib.decompress(z)
                    lg=pickle.loads(p)
                    lg = [x for x in lg if x != '\n']
                    tw.__dict__.update({'MADlog':lg})

            self.sendCommand('exit')
            sleep(1.0)


            return tw,QKL

    def SimpleMatching(self,indict):

        # Only execute a matching command.
        # Sequence needs to be defined outside.

        if 'InitialCondition' not in indict.keys():
            return
        if 'Knobs' not in indict.keys():
            return
        if 'Constraints' not in indict.keys():
            return
        if 'TargetLocation' not in indict.keys():
            return
        if 'Sequence' not in indict.keys():
            seq='swissfel'
        else:
            seq=indict['Sequence']

        if 'Start' not in indict.keys():
            Start='#s'
        if 'End' not in indict.keys():
            End='#s'

        Start=indict['Start']
        End=indict['End']
        
        Range=Start+'/'+End


        if len(indict['InitialCondition'])==4:
            [bx,ax,by,ay]=indict['InitialCondition']
            dx=0
            dpx=0
            dy=0
            dpy=0
            x=0
            px=0
            y=0
            py=0
        elif len(indict['InitialCondition'])==8:
            [bx,ax,by,ay,dx,dpy,dy,dpy]=indict['InitialCondition']
            x=0
            px=0
            y=0
            py=0
        elif len(indict['InitialCondition'])==12:
            [bx,ax,by,ay,dx,dpy,dy,dpy,x,py,y,py]=indict['InitialCondition']
 

        iniC=', betx='+str(bx)+', alfx='+str(ax) \
                +', bety='+str(by)+', alfy='+str(ay) +',x='+str(x) \
                +', px='+str(px)+', y='+str(y)+', py='+str(py)+', dx='+str(dx) \
                +', dpx='+str(dpx)+', dy='+str(dy)+', dpy='+str(dpy) 
            
        
        
        knobs=indict['Knobs']
        cons=indict['Constraints']
        target=indict['TargetLocation']
        


        #self.write('select, flag=error, clear;')
        #self.write('select, flag=twiss, clear;')

        comm='MATCH,SEQUENCE='+seq+',RANGE='+Range+iniC+';\n'
        self.write(comm)

        for i in range(0,len(knobs)):
            k=knobs[i]
            c=cons[i]
            t=target[i]

            self.write('VARY,NAME='+k+',STEP=0.0001;\n')
            self.write('CONSTRAINT,SEQUENCE=swissfel,RANGE='+t+','+c+';\n')


        self.write('LMDIF,CALLS=1000,TOLERANCE=1e-20;\n');
        self.write('ENDMATCH;\n');
        self.write('VAL_MATCH = TAR;\n');

        
        if 'Column' in indict.keys():
            col=indict['Column']
        else:
            col=['NAME','S','BETX','ALFX','BETY','ALFY','X','PX','Y','PY','DX','DPX','DY','DPY','MUX','MUY','K1L']



        comm='twiss, range='+Start+'/'+End+', sequence='+seq+iniC+';'
        self.write(comm)

        tw=self.twiss_table(col)
        
        return tw



    def getMatrix(self,SF,indict,matlab=0):

        # see spring clean up comments for version 2.0.0 at getPropoagation

        indict['InitialCondition']=[10,0,10,0]

        tw=self.getPropagation(SF,indict,matlab)
        
        # get the starting end ending element
        Start=indict['Start']
        End=indict['End']
        s_ele=SF.getElement(indict['Start'])
        e_ele=SF.getElement(indict['End'])

        if 'MQUA' in Start:
            if ('cor' in s_ele.__dict__) or ('corx' in s_ele.__dict__) or ('cory' in s_ele.__dict__):
                Start=Start+'.Q1'
        if 'MQUA' in End:
            if ('cor' in e_ele.__dict__) or ('corx' in e_ele.__dict__) or ('cory' in e_ele.__dict__):
                End=End+'.Q2'
        
        if s_ele and ('enable' in s_ele.__dict__):
            if s_ele.enable:
                Start=Start+'.DIAG'
            else:
                Start=Start+'.MARK'
        if e_ele and ('enable' in e_ele.__dict__):
            if e_ele.enable:
                End=End+'.DIAG'
            else:
                End=End+'.MARK'
        
        S=Start
        E=End
 
#        for ele in tw.indx.keys():
#            print(ele)

        BETXs=tw.BETX[tw.indx[S]]
        ALFXs=tw.ALFX[tw.indx[S]]
        MUXs =tw.MUX[tw.indx[S]]
        BETYs=tw.BETY[tw.indx[S]]
        ALFYs=tw.ALFY[tw.indx[S]]
        MUYs =tw.MUY[tw.indx[S]]

        BETXe=tw.BETX[tw.indx[E]]
        ALFXe=tw.ALFX[tw.indx[E]]
        MUXe =tw.MUX[tw.indx[E]]
        BETYe=tw.BETY[tw.indx[E]]
        ALFYe=tw.ALFY[tw.indx[E]]
        MUYe =tw.MUY[tw.indx[E]]

        phx=2*math.pi*(MUXe-MUXs)
        phy=2*math.pi*(MUYe-MUYs)

        R11=math.sqrt(BETXe/BETXs)*(math.cos(phx)+ALFXs*math.sin(phx))
        R12=math.sqrt(BETXe*BETXs)*math.sin(phx)
        R21=-(1.0+ALFXe*ALFXs)/math.sqrt(BETXe*BETXs)*math.sin(phx)+(ALFXs-ALFXe)/math.sqrt(BETXe*BETXs)*math.cos(phx)
        R22=math.sqrt(BETXs/BETXe)*(math.cos(phx)-ALFXe*math.sin(phx))

        R33=math.sqrt(BETYe/BETYs)*(math.cos(phy)+ALFYs*math.sin(phy))
        R34=math.sqrt(BETYe*BETYs)*math.sin(phy)
        R43=-(1.0+ALFYe*ALFYs)/math.sqrt(BETYe*BETYs)*math.sin(phy)+(ALFYs-ALFYe)/math.sqrt(BETYe*BETYs)*math.cos(phy)
        R44=math.sqrt(BETYs/BETYe)*(math.cos(phy)-ALFYe*math.sin(phy))

        return [R11,R12,R21,R22,R33,R34,R43,R44]





    def getPropagation(self,SF,indict,matlab=0):

        # spring clean up
        # No interface with the hardware. Therefore the EpicsChannel and SwissFEL Magnet part is not needed.
        # The Energy Manager is included in SF
        #

        # check for minimum of arguments in the input
        if 'InitialCondition' not in indict.keys():
            return None
        if 'Start' not in indict.keys():
            return None
        if 'End' not in indict.keys():
            return None


        # getPropagation uses ZMQ MAD sever unless MADX bugs are fixed.
        # for matlab, this method returns a list of madx command
        self.switchOutput(2) 


        if matlab:
            self.matlab=1
        else:
            self.matlab=0
            self.write('initialize') # Use always fresh MADX
        self.mcommand=[]


        # Build up the required sequence
        Start=indict['Start']
        End=indict['End']
        s_ele=SF.getElement(indict['Start'])
        e_ele=SF.getElement(indict['End'])


        if 'MQUA' in Start:
            if ('cor' in s_ele.__dict__) or ('corx' in s_ele.__dict__) or ('cory' in s_ele.__dict__):
                Start=Start+'.Q1'
        if 'MQUA' in End:
            if ('cor' in e_ele.__dict__) or ('corx' in e_ele.__dict__) or ('cory' in e_ele.__dict__):
                End=End+'.Q2'
        
        if s_ele and ('enable' in s_ele.__dict__):
            if s_ele.enable:
                Start=Start+'.diag'
            else:
                Start=Start+'.mark'
        if e_ele and ('enable' in e_ele.__dict__):
            if e_ele.enable:
                End=End+'.diag'
            else:
                End=End+'.mark'
                   

        if 'END' in End.upper(): 
            section=End.split('.')
            sec=SF.getSection(section[0])
            path=sec.mapping
        else:
            path=e_ele.mapping[0]

        try:
            b=indict['Direction']
            if b<0:
                b=-1
            else:
                b=1
        except:
            b=1

        if b==-1:
            self.invertMatrix=True   # functionality added in OMMadxLattice.py

        line=SF.BeamPath(path)
        line.writeLattice(self,SF)   # Access function for "EnergyAt" is in the facility container now.
        self.invertMatrix=False      # make sure that next time it is in forward direction as default

        self.sendCommand('beam, particle=electron, energy=100;') # Need high energu to set beta=1
        if b==-1:
            self.sendCommand('use, sequence=swissfel;')
            self.sendCommand('seqedit, sequence=swissfel;')
            self.sendCommand('flatten;')
            self.sendCommand('reflect;')
            self.sendCommand('endedit;')
            Start,End=End,Start

        # Invert the sequence if a backward propagation is required
        # And compute twiss

        if len(indict['InitialCondition'])==4:
            [bx,ax,by,ay]=indict['InitialCondition']
            dx=0
            dpx=0
            dy=0
            dpy=0
            x=0
            px=0
            y=0
            py=0
        elif len(indict['InitialCondition'])==8:
            [bx,ax,by,ay,dx,dpx,dy,dpy]=indict['InitialCondition']
            x=0
            px=0
            y=0
            py=0
        elif len(indict['InitialCondition'])==12:
            [bx,ax,by,ay,dx,dpx,dy,dpy,x,py,y,py]=indict['InitialCondition']

        


        iniC='betx='+str(bx)+', alfx='+str(b*ax) \
            +', bety='+str(by)+', alfy='+str(b*ay) +',x='+str(x) \
            +', px='+str(b*px)+', y='+str(y)+', py='+str(b*py)+', dx='+str(dx) \
            +', dpx='+str(b*dpx)+', dy='+str(dy)+', dpy='+str(b*dpy)
        

            
        seq='swissfel'
        self.sendCommand('use, sequence='+seq+';')
        
       
        if self.matlab:
            self.sendCommand('select, flag=twiss, column=NAME,S,BETX,ALFX,BETY,ALFY,X,PX,Y,PY,DX,DPX,DY,DPY,MUX,MUY,K1L;')
        else:
            self.sendCommand('column:NAME,S,BETX,ALFX,BETY,ALFY,X,PX,Y,PY,DX,DPX,DY,DPY,MUX,MUY,K1L')



        comm='twiss, range='+Start+'/'+End+', sequence='+seq+','+iniC+';'
        z=self.sendCommand(comm)


        if self.matlab:
            return self.mcommand
        else:
            p = zlib.decompress(z)
            tw=pickle.loads(p)
            # This is not needed when using MADX-ZERO server
            #if b==-1:
            #    self.updateRFmatrix(EM) # Don't forget to restore the normal rf matrix after the reflecting!
            try:
                indict['Log']
            except:
                indict['Log']=0

            lg=[]
            if 'Log' in indict.keys():
                if indict['Log']:
                    z=self.sendCommand('log')
                    p = zlib.decompress(z)
                    lg=pickle.loads(p)
            lg = [x for x in lg if x != '\n']
            tw.__dict__.update({'MADlog':lg})


            self.sendCommand('exit')
            sleep(1.0)


            return tw

    def crushMadx(self):
        try:
            self.command('m1: marker;')
            self.command('m2: marker;')
            self.command('ts: sequence, refer=centre, l=10.0;')
            self.command('m1, at=2;')
            self.command('endsequence;')
            self.command('ts2: sequence, refer=centre, l=30.0;')
            self.command('ts, at=5;')
            self.command('m2, at=12;')
            self.command('endsequence;')
            self.command('seqedit, sequence=ts2;')
            self.command('flatten;')
            self.command('extract,sequence=ts2,from=m1,to=m2,newname=te;')
            self.command('endedit;')
            self.command('ts: sequence, refer=centre, l=10.0;')
            self.command('m1, at=2;')
            self.command('endsequence;')
            self.command('ts2: sequence, refer=centre, l=30.0;')
            self.command('ts, at=5;')
        except:
            'Done'
