#import math
#import os
#import shutil
#from subprocess import call
#from OMAppTemplate import ApplicationTemplate
#from OMType import *
#import numpy as np

from OMLiTrackLattice import *    # all the stuff to generate the lattice from the online model
from PyLiTrack import *           # link to LiTrack executable
from OMWakes import *             # module to calculate and write the wakes in the machine.

#--------
# PyLiTrack Interface


class LiTrack(LiTrackLattice):
    
    def __init__(self):
        LiTrackLattice.__init__(self)  
        # inherites self.beamline which holds the lattice
        # also the two flags self.autophase and self.alloutput

        self.pylt=PyLiTrack()  # link the executable 
        self.wakefunc={'S':1,'C':2,'X':3}
        WK=Wakes()
        [S,L,T]=WK.getWake('S')
        self.pylt.wakefunction.append([S,L])
        [S,L,T]=WK.getWake('C')
        self.pylt.wakefunction.append([S,L])
        [S,L,T]=WK.getWake('X')
        self.pylt.wakefunction.append([S,L])        
        self.pylt.wake_fn=['S','C','X']


    def run(self,filename,p0,q0):
        self.initializeParticleDistribution(filename,p0,q0)
        return self.track()


    def initializeParticaleDistribution(self,filename,p0,q0):
        self.pylt.Ne=q0/1.6e-19   # number of electrons in bunch
        self.pylt.E0=p0*0.511*1e-3 # energy in GeV
        self.pylt.initPartDist(filename) # file name, which has the distribution (s,dp/p) in mm and percent


    def track(self):
        self.pylt.beamline=self.beamline
        return self.pylt.track()

