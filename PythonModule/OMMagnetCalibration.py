import math
import sys
import json
import numpy as np
from OMAppTemplate import ApplicationTemplate

class SwissFELMagnet(ApplicationTemplate):
    def __init__(self):
        ApplicationTemplate.__init__(self)
        self.data={}   # dictionary to hold all calibration data
        self.file='/afs/psi.ch/intranet/SF/Applications/config/MagnetConfig.json'  # path to dictionary
        self.generateJson()
        self.MagnetType={} # dictionary of magnet names with baugruppe

        
    def generateJson(self):
        # function is called when program cannot find a configuration file.
        # Status: ASUUMED = just assumed because of no specification available
        #         SPECIFICATION = specified based on beam dynamics simulations
        #         DESIGN = designed based on field computations, taking into account the specification
        #         MEASUREMENT = measurement with (prototype) fabricated manget
        # Ideally, the following specifications are all from MEASUREMENT. At this moment, they are also from DESIGN, SPECIFICATION or even "ASSUMED".

        self.data={}
        quad={}
        bend={}
        corr={}
        sext={}

        # sextupole [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3,R] # b1/R is gradient at Imax
        sext['HFA']  =[0.08,  0.08,  10.0, 10.1, 0.0, 0.0182,  0,  0, 22e-3] # From MEASUREMENT. Documentation: https://intranet.psi.ch/pub/Swiss_FEL/FinQuadrupoles/QFA.pdf
        sext['HFB']  =[0.08,  0.08,  10.0, 10.1, 0.0, 0.1145,  0,  0, 10e-3] # From MEASUREMENT(?). Documentation(Implementaion): the matlab version Injector on-line model, Curr2KLquad.m
        
        

        # Quadrupole [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3,R] # b1/R is gradient at Imax
        quad['QFA']  =[0.15, 0.173, 150.0, 91.1, 0.0, 0.74521, -0.00813, -0.03542, 22.5e-3] # From MEASUREMENT. Documentation: https://intranet.psi.ch/pub/Swiss_FEL/FinQuadrupoles/QFA.pdf
        quad['QFB']  =[0.08,  0.11,  10.0, 4.9,  0.0, 0.03815,   -1.8e-4,   -0.7e-3, 22.5e-3] # From MEASUREMENT(?). Documentation(Implementaion): the matlab version Injector on-line model, Curr2KLquad.m
        quad['QFC']  =[0.08,  0.11,  10.0, 10.1, 0.0,     0.5,      0.0,      0.0,     1.0] # From DESIGN(?). Documentation: Injector wiki, Subsystems (as of 14th Aug 2014)
        quad['QFD']  =[0.15,  0.1628,10.0, 5.6,  0.0, 0.23313, -27.6e-4,  15.5e-4,   0.011] # From Measurement. Documentation: FEL-SS88-007-7.pdf (found in Alfresco: Company Home > Projects > SwissFEL > Facility > 8850 Magnets)
        quad['QFF']  =[0.08, 0.0875, 10.0, 2.9,  0.0, 0.32897,  -0.5e-4,  -42.9e-4,   0.006] # From Measurement. Documentation: FEL-SS88-008-8.pdf (found in Alfresco)
        quad['QFM']  =[ 0.3,  0.311,   50.0, 21.1, 0.0,    0.64541,      -0.00296,      -0.00617,     0.011] # From Measurement
        quad['QFCOR']=[0.16, 0.11,   10.0, 10.1, 0.0,  0.0278,      0.0,      0.0,     1.0] # From DESIGN. Documentation: qp.pdf (found in Alfresco)
        quad['QFU']  =[0.04, 0.04,      0,    0,   6,     0.0,      0.0,      0.0,     1.0] # Alignment quadrupole. Where is the specification found? 10 T/m is ASSUMED.
        # Imax is specified as 1 A for the permanent magnet such that the polarity can be specified with +1 A or -1 A 
        quad['QFA-SKEW']=quad['QFA']
        quad['QFB-SKEW']=quad['QFB']
        quad['QFC-SKEW']=quad['QFC']
        quad['QFD-SKEW']=quad['QFD']
        quad['QFDM']=quad['QFD']
        quad['QFCOR-SKEW']=quad['QFCOR']


        #Bends  [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3] # b1 is the field strength [T] (linear term) at Imax

        bend['SHA']  =[0.25,   0.235,  30.942, 31.042,  0.0,   0.052168,    0.0,    0.0] # From MEASUREMENT(?). Documentation: Injector wiki, Momentum measurement (as of 14th Aug 2014). Imax is replaced by Inominal
        bend['AFL']  =[0.12,   0.1509,    50.0,   11.6,  0.0,    0.40606,  3.1e-4,-28.7e-4] # From SPECIFICATION. Documentation: FEL-GR06-143-0 (found in Alfresco)
        bend['AFBC1']=[0.25,    0.27,   200.0,   45.2,  0.0,    0.46402,  4.5e-4,-24.7e-4] # From MEASUREMENT. Documentation: AFBC1 Shimming of .... V. Vrankovic, May-June 2013 (found in Alfresco)
        bend['AFBC3']=[ 0.5,  0.5295,   150.0,  56.7,   0.0,    0.97172, -7.3e-4,-38.2e-4] # From MEASUREMENT. Documentation: FEL-SS88-040-1 (found in Alfresco)
        bend['AFS']  =[ 1.0,     1.0,   200.0,  200.1,  0.0,       0.96,    0.0,    0.0] # Where is the specification found? 5.5 degree @ 3 GeV with 200 A power supply is ASSUMED. 
        bend['AFSS'] =[0.36,    0.36,   200.0,  200.1,  0.0,      0.281,    0.0,    0.0] # Where is the specification found? 0.3 degree @ 5.8 GeV with 200 A power supply is ASSUMED. 
        bend['AFD1'] =[ 2.0,     2.0,   200.0,  200.1,  0.0,     1.6871,    0.0,    0.0] # Where is the specification found? 10 degree @ 5.8 GeV with 200 A power supply is ASSUMED. 
        bend['AFD2'] =[ 1.1,     1.1,   150.0,  150.1,  0.0,      1.609,    0.0,    0.0] # From DESIGN, AFD2-150.xls in Alfresco, 15.12.2014
        bend['AFP1'] =[ 0.3,     0.3,    50.0,   50.1,  0.0,        1.0,    0.0,    0.0] # Where is the specification found?  1 T at 50 A is ASSUMED. 
        bend['AFP2'] =[ 0.3,     0.3,    50.0,   50.1,  0.0,        1.0,    0.0,    0.0] # Where is the specification found?  1 T at 50 A is ASSUMED. 
        # Imax is specified as 1 A for the permanent magnet. The current I can then be 1 A nominally and may be 0 A when you want to "turn it off".
        bend['AFDL'] =[ 1.0,     1.016,   135,   99.7,  0.0,    1.46701,-0.01159,-0.03781] # Where is the specification found? 1 T at 50 A is ASSUMED. 


        
        # Corrector [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3] # b1 is the field strength [T] (linear term) at Imax
        corr['QFD'] =[ 0.15,    0.1903,    10.0,   2.8,  0.0,    0.04129,    -0.00078,    0.00049]
        corr['QFDM'] =[ 0.15,    0.15,    10.0,   10.1,  0.0,    0.01288,    0.0,    0.0] # From SPECIFICATION, 100 urad at 5.8 GeV, modified QFD for the low-energy part of the linac 
        corr['QFF']  =[ 0.08,    0.0999,    10.0,  5.7,  0.0,    0.06373,    -3.3e-4,    0.9e-4] 
        corr['AFL']  =[  0.1,     0.1,    10.0,   10.1,  0.0,    0.02135,    0.0,    0.0] # From SPECIFICATION, 200 urad at 3.0 GeV. Documentation: FEL-GR06-143-0.pdf (found in Alfresco)
        corr['AFBC1']=[ 0.25,   0.269,    10.0,   10.1,  0.0,   0.001211,    0.0,    0.0] # From MEASUREMENT. Documentation: AFBC1_shimming.pdf (found in Alfresco)
        corr['AFBC3']=[  0.5,     0.5,    10.0,   10.1,  0.0,   0.016305,    0.0,    0.0] # From SPECIFICATION. b0 computed from the motive-force ratio (0.929385*15*10/57/150). Documentation: FEL-SS88-040-0.pdf (found in Alfresco)
        corr['AFSS'] =[ 0.36,    0.36,   200.0,  200.1,  0.0,   0.028119,    0.0,    0.0] # Where is the specification found? 0.003 degree @ 5.8 GeV with 200 A power supply is ASSUMED. 
        corr['AFDL'] =[  1.2,     1.2,    50.0,   50.1,  0.0,   0.001454,    0.0,    0.0] # Where is the specification found? 0.01 degree @ 300 MeV with 50 A power supply is ASSUMED. 
        corr['SFC'] =[ 0.05,    0.152,    10.0,   6.2,  0.0,       0.01117,    -2.1e-4,    2.5e-4]
        corr['SFQFM']=[  0.2,  0.2171,    10.0,   10.1,  0.0,     0.0198,    0.0,    0.0] # From DESIGN. Documentation: SFQFM.pdf 
        corr['SFDD'] =[0.015,  0.0171,    20.0,   20.1,  0.0,    0.00026,    0.0,    0.0] # From MEASUREMENT. Documentation: https://intranet.psi.ch/pub/Swiss_FEL/FinSteeringMagnets/SFDD.pdf
        corr['SFD1'] =[0.015,  0.0171,    20.0,   20.1,  0.0,    0.00026,    0.0,    0.0] # Where is the specification found? Copied from SFDD
        corr['SFB']  =[0.005,   0.005,    10.0,   10.1,  0.0,    0.00667,    0.0,    0.0] # Where is the specification found? 1 mrad @ 10 MeV with 10 A power supply is ASSUMED. 
        corr['RES-KICKER-AC'] =[ 1.0,     1.0,    1.0,   1.1,  0.0,     0.0,    0.0,    0.0] # Where is the specification found? No field is ASSUMED
        corr['RES-KICKER-DC'] =[0.21,    0.21,    1.0,   1.1,  0.0,     0.0,    0.0,    0.0] # Where is the specification found? No field is ASSUMED
        corr['SFU']  =[ 0.06,    0.0316,    10.0,   4.0,  0.0,       0.00746,   0.2e-4,   -0.9e-4]
        corr['SFU-H'] = corr['SFU']
        corr['U15'] = [ 0.06,    0.06,    10.0,   10.1,  0.0,       0.02,    0.0,    0.0] # Where is the specification found? 1 mrad @ 300 MeV with 10 A power supply is ASSUMED. 
        corr['UE40'] = [ 0.06,    0.06,    10.0,   10.1,  0.0,       0.02,    0.0,    0.0] # Where is the specification found? 1 mrad @ 300 MeV with 10 A power supply is ASSUMED. 


        self.data['Quadrupole']=quad
        self.data['Dipole']=bend
        self.data['Corrector']=corr   
        self.data['Sextupole']=sext
        return

#--------------------------------------------------
# setting up the references to the magnet names

    def writeBend(self,ele):
        if ele.Name not in self.MagnetType.keys():
            self.MagnetType[ele.Name]=ele.Baugruppe
#            print(ele.Name,ele.Baugruppe)
            if 'cor' in ele.__dict__:
                name=ele.Name.replace('MBND','MCRX')
                self.MagnetType[name]=ele.Baugruppe
        return

    def writeQuadrupole(self,ele):     
        if ele.Name not in self.MagnetType.keys():
            self.MagnetType[ele.Name]=ele.Baugruppe
            if 'corx' in ele.__dict__:
                name=ele.Name.replace('MQUA','MCRX')
                self.MagnetType[name]=ele.Baugruppe
            if 'cory' in ele.__dict__:
                name=ele.Name.replace('MQUA','MCRY')
                self.MagnetType[name]=ele.Baugruppe
        return
   
    def writeCorrector(self,ele):                
        if ele.Name not in self.MagnetType.keys():
            self.MagnetType[ele.Name]=ele.Baugruppe
            if 'corx' in ele.__dict__:
                name=ele.Name.replace('MCOR','MCRX')
                self.MagnetType[name]=ele.Baugruppe
            if 'cory' in ele.__dict__:
                name=ele.Name.replace('MCOR','MCRY')
                self.MagnetType[name]=ele.Baugruppe
        return

    def writeSextupole(self,ele):       
        if ele.Name not in self.MagnetType.keys():
            self.MagnetType[ele.Name]=ele.Baugruppe
        return
       

    def writeUndulator(self,ele): 
        #Bug to fix here
        if ele.Name not in self.MagnetType.keys():
            self.MagnetType[ele.Name]=ele.Baugruppe
            if 'cor' in ele.__dict__:
                name=ele.Name.replace('UIND','MCRX')
                self.MagnetType[name]=ele.Baugruppe
        return


    def writeSolenoid(self,ele):
        if ele.Name not in self.MagnetType.keys():
            self.MagnetType[ele.Name]=ele.Baugruppe
        return



#----------------------------------------------------------------------------------
# utility functions to simplify the access functions

    def getBrho(self,ele,EM):
        if isinstance(EM,int) or isinstance(EM,float):
            Energy=float(EM)
        else:
            if isinstance(ele,str):
                Name=ele
            else:
                Name=ele.Name
            Energy=EM.Energy[Name][0]   # argument EM is energy server - danger for possible wrong type 

        Energy=Energy*1e-6
        gamma=(Energy+0.511)/0.511 # Energy in units of MeV since 29.06.2015
        beta=math.sqrt(1-1/gamma**2)
        p=511e3*beta*gamma/1e9 # momentum in GeV/c
        brho=p/0.299792 # P[GeV/c]=0.3*Brho[T.m]
        return brho                

    def getSpec(self,ele,typ):
        if typ not in self.data.keys():
            print('No valid type given: %s' %typ)
            return False

        if isinstance(ele,str):
            Name=ele
            # first look for explicit name
            if Name in self.MagnetType.keys(): # this is the generate look up table for each magnet name
                return self.getSpec(self.MagnetType[Name],typ)
            #second look for baugruppe
            if Name in self.data[typ].keys():
                spec=self.data[typ][Name]
                if isinstance(spec,list):
                    return spec
                if 'Ref' in spec.keys():
                    spec=self.getSpec(str(spec['Ref']),typ)
                return spec  
            else:
                print('Calibration has no entry for %s' % Name)
                return False
        else:
            Name=ele.Name                      # specific element type of online model - danger for possible wrong type
            if self.data[typ].has_key(Name):
                return self.getSpec(Name,typ)
            else:
                return self.getSpec(ele.Type,typ)

#----------------------------
# conversion functions


# get the specific value for a current. The meaning depends on the group. The order defines the transverse dependence, e.g. order =1 means quadrupole
    def getVal(self,ele,I,group,order,EM):

        brho=self.getBrho(ele,EM) # get the relativisitc regidity of the beam
        if brho <=0:
            return 0

        spec=self.getSpec(ele,group)          # get calibration data, searchable by either device name, magnet type or ele of the online model.
        if isinstance(spec,bool):   # check for invalid element
            return 0




        R=1
        if order < 1:
            [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3]=spec  # quick conversion for dipoles which have no reference radius
        else:
            [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3,R]=spec  # quick conversion for quadrupoles and sextupoles

        if order > 0:
            R=R**order/order  # cross check needed. Is R^2 good for sextupole or must it be R^2/2
    
        if abs(I)<Ilin:
            B = (b0 + b1*abs(I)/Imax)
        else:
            B = (b0 + b1*abs(I)/Imax + b2*((abs(I)-Ilin)**2/(Imax-Ilin)**2) + b3*((abs(I)-Ilin)**3/(Imax-Ilin)**3))


        val=np.sign(I)*B/brho*MagneticLength/R
        if order < 1:
            val=val*180/math.pi    # for dipoles and correctors convert to angle in degree. Correctors have them calibrated back in the calling functions
        return val
   
    def getI(self,ele,val,group,order,EM):

        brho=self.getBrho(ele,EM)
        if brho <=0:
            return 0

        spec=self.getSpec(ele,group)
        if isinstance(spec,bool):   # check for invalid element
            return 0

        R=1
        if order<1:
            [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3]=spec  # quick conversion
        else:
            [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3,R]=spec  # quick conversion

        if order > 0:
            R=R**order/order   
        if order < 1:
            val=val*math.pi/180

        B = brho*val/MagneticLength
        I=Imax*(B*R -b0)/b1

        if abs(I)>Ilin:
# correction from Masamitsu, email 3.1.17
            coeff=[b3/(Imax-Ilin)**3,b2/(Imax-Ilin)**2,b1/Imax,b0-abs(B)*R+b1*Ilin/Imax]
            rt=np.roots(coeff)
            if len(rt)==1:
                rt=rt[0]
            else:
                rtind=np.argmin(np.abs(np.abs(rt)-(abs(I)-Ilin)))
                rt=abs(rt[rtind])
            I=val/abs(val)*(rt+Ilin)
        return I    

#   quick check for permanent quadrupole. Shouldn't be there also permanent dipoles to be covered.

    def getPKL(self,ele,group,EM):
        spec=self.getSpec(ele,group)
        if isinstance(spec,bool):   # check for invalid element
            return 0
        [PhysicalLength,MagneticLength,Imax,Ilin,b0,b1,b2,b3,R]=spec  # quick conversion
        brho=self.getBrho(ele,EM)
        if brho <=0:
            return 0
        return b0/brho*MagneticLength



#----------------------------------------------------------
# access function

    def SextupoleI2KL(self,ele,I,EM=None):
        # if argument is list of element, loop over all elements
        if isinstance(ele,list):
            KLlist=[]
            for i in range(0,len(ele)):
                KLlist.append(self.SextupoleI2KL(ele[i],I[i],EM))
            return KLlist
        # check for useful energy value
        if not EM:
            print('Calibration needs energy value')
            return 0
        return self.getVal(ele,I,'Sextupole',2,EM)
                              

    def SextupoleKL2I(self,ele,kl,EM=None):
        # if argument is list of element, loop over all elements
        if isinstance(ele,list):
            Ilist=[]
            for i in range(0,len(ele)):
                Ilist.append(self.SextupoleKL2I(ele[i],kl[i],EM))
            return Ilist
        # check for useful energy value
        if not EM:
            print('Calibration needs energy value')
            return 0
        return self.getI(ele,kl,'Sextupole',2,EM)        


    def QuadrupoleI2KL(self,ele,I,EM=None):
        # if argument is list of element, loop over all elements
        if isinstance(ele,list):
            KLlist=[]
            for i in range(0,len(ele)):
                KLlist.append(self.QuadrupoleI2KL(ele[i],I[i],EM))
            return KLlist
        # check for useful energy value
        if not EM:
            print('Calibration needs energy value')
            return 0
        return self.getVal(ele,I,'Quadrupole',1,EM)
                              

    def QuadrupoleKL2I(self,ele,kl,EM=None):
        # if argument is list of element, loop over all elements
        if isinstance(ele,list):
            Ilist=[]
            for i in range(0,len(ele)):
                Ilist.append(self.QuadrupoleKL2I(ele[i],kl[i],EM))
            return Ilist
        # check for useful energy value
        if not EM:
            print('Calibration needs energy value')
            return 0
        return self.getI(ele,kl,'Quadrupole',1,EM)
                              

    def PQuadrupoleKL(self,ele,EM=None):
        # if argument is list of element, loop over all elements
        if isinstance(ele,list):
            KLlist=[]
            for i in range(0,len(ele)):
                KLlist.append(self.PQuadrupoleKL(ele[i],EM))
            return KLlist
        # check for useful energy value
        if not EM:
            print('Calibration needs energy value')
            return 0
        return self.getPKL(ele,'Quadrupole',EM)


    def BendI2Angle(self,ele,I,EM=None):
        # if argument is list of element, loop over all elements
        if isinstance(ele,list):
            Anglist=[]
            for i in range(0,len(ele)):
                Anglist.append(self.BendI2Angle(ele[i],I[i],EM))
            return Anglist
        # check for useful energy value
        if not EM:
            print('Calibration needs energy value')
            return 0
        return self.getVal(ele,I,'Dipole',0,EM)
    

    def BendAngle2I(self,ele,angle,EM=None):
        # if argument is list of element, loop over all elements
        if isinstance(ele,list):
            Ilist=[]
            for i in range(0,len(ele)):
                Ilist.append(self.BendAngle2I(ele[i],angle[i],EM))
            return Ilist
        # check for useful energy value
        if not EM:
            print('Calibration needs energy value')
            return 0
        return self.getI(ele,angle,'Dipole',0,EM)        

    def CorrectorI2Kick(self,ele,I,EM=None):
        # if argument is list of element, loop over all elements
        if isinstance(ele,list):
            Anglist=[]
            for i in range(0,len(ele)):
                Anglist.append(self.CorrectorI2Kick(ele[i],I[i],EM))
            return Anglist
        # check for useful energy value
        if not EM:
            print('Calibration needs energy value')
            return 0
        return self.getVal(ele,I,'Corrector',0,EM)*math.pi/180
    

    def CorrectorKick2I(self,ele,angle,EM=None):
        # if argument is list of element, loop over all elements
        if isinstance(ele,list):
            Ilist=[]
            for i in range(0,len(ele)):
                Ilist.append(self.CorrectorKick2I(ele[i],angle[i],EM))
            return Ilist
        # check for useful energy value
        if not EM:
            print('Calibration needs energy value')
            return 0
        return self.getI(ele,angle*180/math.pi,'Corrector',0,EM)        


#------------------------------------
# aux function (are they really needed?)


    def QuadrupoleKL2K(self,dn,QuadKL,Mode='P'):
        # K of the physical length by default

        if isinstance(dn,list):
            QuadK=[]
            for i in range(0,len(dn)):
                QuadK.append(self.QuadrupoleKL2K(dn[i],QuadKL[i],Mode))
            return QuadK
                  
        spec=self.getSpec(dn,'Quadrupole')
        if Mode=='P':
            L=spec[0]
        elif Mode=='M':
            L=spec[1]
        return QuadKL/L


    def QuadrupoleK2KL(self,dn,QuadK,Mode='P'):
        # Need to know if K is of physical length or magnetic length 
        if isinstance(dn,list):
            QuadKL=[]
            for i in range(0,len(dn)):
                QuadKL.append(self.QuadrupoleK2KL(dn[i],QuadK[i],Mode))
            return QuadKL
                  
        spec=self.getSpec(dn,'Quadrupole')
        if Mode=='P':
            L=spec[0]
        elif Mode=='M':
            L=spec[1]
        return QuadK*L



    def SextupoleKL2K(self,dn,QuadKL,Mode='P'):
        # K of the physical length by default

        if isinstance(dn,list):
            QuadK=[]
            for i in range(0,len(dn)):
                QuadK.append(self.SextupoleKL2K(dn[i],QuadKL[i],Mode))
            return QuadK
                  
        spec=self.getSpec(dn,'Sextupole')
        if Mode=='P':
            L=spec[0]
        elif Mode=='M':
            L=spec[1]
        return QuadKL/L


    def SextupoleK2KL(self,dn,QuadK,Mode='P'):
        # Need to know if K is of physical length or magnetic length 
        if isinstance(dn,list):
            QuadKL=[]
            for i in range(0,len(dn)):
                QuadKL.append(self.SextupoleK2KL(dn[i],QuadK[i],Mode))
            return QuadKL
                  
        spec=self.getSpec(dn,'Sextupole')
        if Mode=='P':
            L=spec[0]
        elif Mode=='M':
            L=spec[1]
        return QuadK*L







