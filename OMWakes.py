import math
# import matplotlib.pyplot as plt

class Wakes:
    def __init__(self):

        self.c= 2.99792458e8
        self.Z0 = 120*math.pi


        self.N=50000
        # OK for python2 and python3
        self.lwake=list(range(self.N))
        self.twake=list(range(self.N))
        self.s=list(range(self.N))


        #N=500 # 2 micron step


#            self.s[i+N]=s0+i*5*ds
#            self.s[i+2*N]=6*s0+i*10*ds


    def getWake(self,band,writeSDDS=''):       
        par=[1, 1, 1]
        if band=='S':
            par=self.getSBandPar()
            s0=50e-3
        if band=='X':
            par=self.getXBandPar()
            s0=50e-3
        if band=='C':
            par=self.getCBandPar()
            s0=10e-3

        ds=s0/self.N
        for i in range(self.N):
            self.s[i]=i*ds

        a=par[2]
        s00=self.gets00(par[0],par[1],par[2]);

        for i in range(len(self.s)):
            self.lwake[i]=self.getWlc(self.s[i],s00,a)
            self.twake[i]=self.getWtca(self.s[i],s00,a)
        
        if len(writeSDDS)>0:
            self.writeSDDS(band,writeSDDS)
 
        # plotting the wake to cross check with bolkos matlab scripts    
#        plt.plot(self.s,self.lwake)
#        plt.xlabel('Test')
#        plt.xlim(0,0.01)
#        plt.show()

        return [self.s, self.lwake, self.twake]    

#--------------------------------------
#   write SDDS wakefile


    def writeSDDS(self,band,path):
        filename=path+'/wake_l_'+band+'-band.sdds'
        fid=open(filename,'w')
        fid.write('SDDS1\n')
        fid.write('&column name=z,    units=m,    type=double,    &end\n')
        fid.write('&column name=W,    units=V/C,  type=double,    &end\n')
        fid.write('&column name=t,    units=s,    type=double,    &end\n')
        fid.write('&column name=WL,   units=V/C/m,    type=double,    &end\n')
        fid.write('&data mode=ascii, &end\n')
        fid.write('! page number 1\n')
        fid.write('%i\n' % len(self.s))
        for i in range(len(self.s)):
            fid.write('  %12.6e  %12.6e  %12.6e %12.6e\n' % (self.s[i],self.lwake[i],self.s[i]/self.c,self.lwake[i]))
        fid.close()

        filename=path+'/wake_t_'+band+'-band.sdds'
        fid=open(filename,'w')
        fid.write('SDDS1\n')
        fid.write('&column name=z,    units=m,    type=double,    &end\n')
        fid.write('&column name=W,    units=V/C/m,  type=double,    &end\n')
        fid.write('&column name=t,    units=s,    type=double,    &end\n')
        fid.write('&column name=WT,   units=V/C/m$a2$n,    type=double,    &end\n')
        fid.write('&data mode=ascii, &end\n')
        fid.write('! page number 1\n')
        fid.write('%i\n' % len(self.s))
        for i in range(len(self.s)):
            fid.write('  %12.6e  %12.6e  %12.6e %12.6e\n' % (self.s[i],self.twake[i],self.s[i]/self.c,self.twake[i]))
        fid.close()

#----------------------------------------------------------------
#   geometry specific data


    def getCBandPar(self):
        L=17.4948913e-3;
        g=L-2.500843e-3;
        Rad=[7.2536062,   7.2412883,    7.2289165,    7.2164893,    7.2040059,    7.1914655,    7.1788676,    7.1662119,    7.1534986,    7.1407276,    7.1278995,   7.1150147,    7.1020737,    7.0890772,    7.0760258,    7.0629203,    7.0497613,    7.0365495,    7.0232853,    7.0099694,    6.9966020,    6.9831834,    6.9697137,    6.9561929,    6.9426208,    6.9289970,    6.9153212,    6.9015925,   6.8878104,    6.8739738,    6.8600817,    6.8461329,    6.8321263,    6.8180604,    6.8039340,    6.7897454,    6.7754933,    6.7611760,    6.7467922,    6.7323403,    6.7178188,    6.7032264,    6.6885615,    6.6738229,    6.6590094,   6.6441198,    6.6291528,    6.6141076,    6.5989831,    6.5837784,    6.5684928,    6.5531254,    6.5376754,    6.5221423,    6.5065254,    6.4908239,    6.4750373,    6.4591648,    6.4432058,    6.4271594,    6.4110249,    6.3948015,   6.3784880,    6.3620834,    6.3455867,    6.3289964,    6.3123112,    6.2955295,    6.2786498,    6.2616702,    6.2445888,    6.2274037,    6.2101127,    6.1927137,    6.1752042,    6.1575820,    6.1398446,    6.1219895,    6.1040142,   6.0859160,    6.0676924,    6.0493408,    6.0308586,    6.0122432,    5.9934921,    5.9746026,    5.9555723,    5.9363986,    5.9170790,    5.8976109,    5.8779918,    5.8582190,    5.8382900,    5.8182018,    5.7979518,    5.7775368,   5.7569538,    5.7361993,    5.7152698,    5.6941616,    5.6728705,    5.6513921,    5.6297219,    5.6078548,    5.5857857,    5.5635088,    5.5410184,    5.5183082,    5.4953719,    5.4722028,    5.4487940,    5.4251386]
        a=0;
        for i in range(len(Rad)):
            a=a+1/Rad[i]/Rad[i]
        a=a/len(Rad)
        a=math.sqrt(1/a)*1e-3
        return [L, g, a]

    def getSBandPar(self):
        L=33.324e-3;  # length of cups
        g=L-5e-3;     # length of cavity = cell - wall thickness
        a=self.getRadii(12.695e-3,9.31e-3,122)
        return [L, g, a]


    def getXBandPar(self):
        L=10.414e-3;  # length of cups
        g=L-0.5e-3*(3.3+3.586)     # length of cavity = cell - wall thickness
        a=self.getRadii(4.991e-3,4.106e-3,72)
        return [L, g, a]

#----------------------------
# Numerical receipe version of the complimentary error function

    def erfc(self,x):
    # constants
        a1 =  0.254829592
        a2 = -0.284496736
        a3 =  1.421413741
        a4 = -1.453152027
        a5 =  1.061405429
        p  =  0.3275911

    # Save the sign of x
        sign = 1
        if x < 0:
            sign = -1
        x = abs(x)

    # A&S formula 7.1.26
        t = 1.0/(1.0 + p*x)
        y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*math.exp(-x*x)

        return 1-sign*y


#------------------------------------------------------------------
#   wake model


    def gets00(self,L,g,a):   # characteristic length of the wakefield    
        x=a/( L*(1-0.465*math.sqrt(g/L) -0.0704*g/L) )
        return g/8*x*x

    def getWlc(self,s,s00,a):
        arg=math.pi*s/4/s00
        return self.Z0*self.c/math.pi/a/a * math.exp(arg) * self.erfc(math.sqrt(arg))

    def getWlca(self,s,s00,a):   # old model used for ELEGANT. Use getWlc instead
        return self.Z0*self.c/math.pi/a/a * math.exp(-math.sqrt(s/s00))

    def getWtca(self,s,s00,a): 
        return 4*self.Z0*self.c*s00 /math.pi/(a*a*a*a) * ( 1-( 1 + math.sqrt(s/s00) )*math.exp(-math.sqrt(s/s00)) )

    def getRadii(self,R0,R1,N):
        avg=0
        dR=(R1-R0)/(N-1)
        for i in range(N):
            R=R0+i*dR
            avg=avg+1/R/R
        avg=avg/N
        return math.sqrt(1/avg)

