import math
import os
import sys
from OMAppTemplate import ApplicationTemplate
import numpy as np
from string import *

from epics import PV


#--------
# Energy Manager Interface

class MachineInterface(ApplicationTemplate):    
    def __init__(self):
        ApplicationTemplate.__init__(self)
        self.quads={}
        self.bends={}
        self.sexts={}
        self.cors ={}
        self.ID   ={}
        self.DCP  ={}
        self.dechirper={}
        self.energy={}
        self.RF={}

#------------- access the machine

    def writePVs(self,pvs):
        for pv in pvs.keys():
            chn=PV(pv)
            chn.put(pvs[pv])


    def readMagnet(self):
        val={}
        for p in self.quads.keys():
            val[p]=self.quads[p].get() 
            if val[p] is None:
                print('Cannot read from', p)
        for p in self.bends.keys():
            val[p]=self.bends[p].get()        
            if val[p] is None:
                print('Cannot read from', p)
        for p in self.sexts.keys():
            val[p]=self.sexts[p].get() 
            if val[p] is None:
                print('Cannot read from', p)
        return val
       
    def readRF(self):
        val={}
        for p in self.RF.keys():
            val[p]=[p.get() for p in self.RF[p]]
            if val[p] is None:
                print('Cannot read from', p)
        return val

    def readID(self):
        val={}
        for p in self.ID.keys():
            val[p]=self.ID[p].get()
            if val[p] is None:
                print('Cannot read from', p)
        for p in self.DCP.keys():
            val[p]=self.DCP[p].get()
            if val[p] is None:
                print('Cannot read from', p)
        return val

    def readEnergy(self):
        val={}
        for p in self.energy.keys():
            val[p]=self.energy[p].get()
            if val[p] is None:
                print('Cannot read from', p)
        return val



# update the model directly

    def updateModel(self,SF=None):
        self.updateRF(SF)
        self.updateIDs(SF)
        self.updateMagnets(SF)

    def updateRF(self,SF=None):
        if SF is None:
            return
        rf=self.readRF()
        for ele in rf.keys():
            listRF = SF.listElement(ele+'.RACC',1)
            phase = rf[ele][1]
            grad  = rf[ele][0]/len(listRF)*1e6
            if phase is None or grad is None:
                print('Invalid value for RF settings')
            for acc in listRF:
                acc.Phase = phase
                acc.Gradient = grad/acc.Length
        SF.forceEnergyAt('SINLH01.DBAM010',140e6)


    def updateIDs(self,SF=None):
        if SF is None:
            return
        for p in self.ID.keys():
            val=self.ID[p].get()
            if val is None:
                print('Invalid value for',p)
            name=p.replace('-','.')[0:15]
            ele=SF.getElement(name)
            if ':K' in p:
                ele.K=val
            if ':P' in p:
                if val == 0:
                    ele.kx=0
                    ele.ky=1
                elif val > 2:
                    ele.kx=0.5
                    ele.ky=0.5
                else:
                    ele.kx=1
                    ele.ky=0

        for p in self.DCP.keys():
            val=self.DCP[p].get()
            if val is None:
                print('Invalid value for',p)

            name=p.replace('-','.')[0:15]
            ele=SF.getElement(name)
            if ':K' in p:
                ele.gap=val*1e-3   # epics channels are in mm
            if ':P' in p:
                ele.offset=val*1e-3
          

    def updateMagnets(self,SF=None):
        if SF is None:
            return

        for quad in self.quads.keys():
            val=self.quads[quad].get()
            if val is None:
                print('Invalude value for',quad)
            name=quad.replace('-','.')
            ele=SF.getElement(name)
            ele.k1=val/ele.Length
                    

        for sext in self.sexts.keys():
            val=self.sexts[sext].get()
            if val is None:
                print('Invalude value for',sext)

            name=sext.replace('-','.')
            ele=SF.getElement(name)
            ele.k2=val/ele.Length

        for bend in self.bends.keys():
            val=self.bends[bend].get()
            if val is None:
                print('Invalude value for',bend)

            name=bend.replace('-','.')
            ele=SF.getElement(name)
            if 'SATUN' in name:
                val=0
            ele.angle=np.sign(ele.design_angle)*np.abs(val)





#------------ construct the pv channels for the machine interface

    def writeLine(self,line,seq):
        return

    def demandMapID(self):
        'Energy manager always requests Map ID to Layout manger'
        return 0


    def writeBend(self,ele):
        name0=ele.Name.replace('.','-')
        if 'MBNP' in name0:
            return
        if 'MBND100' in name0:
            if name0 not in self.energy.keys():
                self.energy[name0]=PV('%s:ENERGY-OP' % name0)
        if 'SINEG' in name0 or 'S10DI' in name0 or 'S10MA' in name0 or 'SATSY' in name0 or 'SATCL01' in name0:
            return
        if 'BD01' in name0 or 'S20SY' in name0:
            return
        name=name0[0:7]+'-MBND100'

        if name0 not in self.bends.keys():
            pvname='%s:BEND-ANGLE' % name
            if 'SINLH02' in name0 or 'SINBC02' in name0 or 'S10BC02' in name0:
                pvname=pvname.replace('BEND-ANGLE','ANGLE-CALC')
            self.bends[name0]=PV(pvname)

    def writeQuadrupole(self,ele):
        if 'MQUP' in ele.Name:
            return
        name=ele.Name.replace('.','-')
        if name not in self.quads.keys():
            self.quads[name]=PV('%s:K1L-SET' % name)
   
    def writeCorrector(self,ele):   
        return

    def writeSextupole(self,ele):  
        name=ele.Name.replace('.','-')
        if name not in self.sexts.keys():
            self.sexts[name]=PV('%s:K2L-SET' % name)
 
    def writeUndulator(self,ele):

        if 'UDCP' in ele.Name:
            name=ele.Name.replace('.','-')
            name0=ele.Name.replace('.','-')
            if name0 not in self.DCP.keys():
                self.DCP[name0+':K']=PV('%s:GAP' % name)
                self.DCP[name0+':P']=PV('%s:CENTER' % name)
            return
     
        if 'UIND' not in ele.Name:
            return
#        if 'SATUN10' in ele.Name:
#            return

        name=ele.Name.replace('.','-')+':K'
        if name not in self.ID.keys():
            if 'SINLH' in name:
                self.ID[name]=PV(name)
            else:
                self.ID[name]=PV(name+'_SET')
            if 'SATUN' in name:
                name2=ele.Name.replace('.','-')+':P'
                self.ID[name2]=PV(name2+'OL-SET')

    def writeRF(self,ele):  
        if not 'RACC100' in ele.Name:
            return
        if 'SATCB01' in ele.Name:
            return
        name0=ele.Name[0:7]
        if name0 not in self.RF.keys():
            PVs=[PV(name0+'-RSYS:SET-ACC-VOLT'),PV(name0+'-RSYS:SET-BEAM-PHASE')]
            self.RF[name0]=PVs

  
