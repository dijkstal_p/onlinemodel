import math
import tempfile
from os import listdir, remove, chdir, system
from subprocess import PIPE, Popen, run
from PyQt5.QtCore import QTimer, pyqtSignal, QObject


from OMFacility import *
from OMMachineInterface import *
from OMMadxLat import *

class OMMadXTracker(QObject):

    trackingDone = pyqtSignal()

    def __init__(self):

        super(QObject, self).__init__()
        super(OMMadXTracker, self).__init__()

        self.Log=None

        print('Initializing Online Model')
        self.Facility=Facility()
        self.Facility.forceEnergyAt('SINLH01.DBAM010', 140e6)

        print('collecting EPICS Channels')
        self.MI=MachineInterface()
        self.Facility.writeFacility(self.MI)

        self.Madx=OMMadXLattice()
        self.setLine(0)   # choose aramis line as initial destination

        self.tempdir=None

        self.timer = QTimer()
        self.timer.timeout.connect(self.progress)



    #------------------------------------
    # machine Interface

    def updateFromMachine(self):
        self.MI.updateModel(self.Facility)

    def track(self,start='#s',end='#e',variables={}):
        self.res=[]
        self.writeLattice(variables)
        for var in variables.keys():
            if 'betax' in var or 'betay' in var or 'alphax' in var or 'alphay' in var:
                continue
            self.Madx.write('%s := %s;\n' % (var, variables[var]))
        self.writeLatticeTracking(start,end)
        if self.tempdir is not None:
            self.tempdir.cleanup()
            self.tempdir=None
        self.tempdir=tempfile.TemporaryDirectory()
        print('Madx is writing to directory:',self.tempdir)
        with open(self.tempdir.name + '/tmp-lattice.madx', 'w') as f:
            for line in self.Madx.cc:
                f.write(line)

        chdir(self.tempdir.name)
        self.p = Popen(["/sf/bd/bin/madx", "tmp-lattice.madx"], stdout=PIPE, bufsize=100,
                       universal_newlines=True)
        self.timer.start(10)

        

#-------------------------
#  time function to monitor progress
    def progress(self):
        if not self.p.poll() is None:
            self.timer.stop()
            self.res=self.parseTwissFile()
            print('Tracking Done')
            self.trackingDone.emit()
            return
        idx = 100
        while True:
            line = self.p.stdout.readline()
            if not line or idx is 0:
                break
            if 'entr' in line or 'call' in line or 'info' in line or 'lot' in line or 'flattened' in line:
                idx -= 1
            else:
                if self.Log is None:
                    print(line.strip())
                else:
                    self.Log.appendPlainText(line.strip())
                idx -= 1
        if idx is 0:
            self.timer.start(10)


    def parseTwissFile(self):
        start = False
        skipline = False
        res = []
        print('parsing twiss file')
        with open(self.tempdir.name+'/twiss.dat') as f:
            for line in f:
                if '* NAME' in line:
                    names = line.split()[1:]
                    res.append(names)
                    start = True
                    skipline = True
                    continue
                elif start is False:
                    continue
                elif skipline is True:
                    skipline = False
                    continue
                else:
                    val = line.split()
                    res.append(val)

        return res



#------------------------------------------------------
# function to generate the madx tracking files

    def writeLattice(self,initopt=None):            
        self.Madx.clear()
        self.Madx.write('option,-echo;\n')
        if isinstance(initopt,dict):
            if 'betax' in initopt.keys():
                self.Madx.write('betax0=%e;\n' % initopt['betax'])
            else:
                self.Madx.write('betax0=30;\n')
            if 'betay' in initopt.keys():
                self.Madx.write('betay0=%e;\n' % initopt['betay'])
            else:
                self.Madx.write('betay0=30;\n')

            if 'alphax' in initopt.keys():
                self.Madx.write('alphax0=%e;\n' % initopt['alphax'])
            else:
                self.Madx.write('alphax0=0;\n')
            if 'betax' in initopt.keys():
                self.Madx.write('alphay0=%e;\n' % initopt['alphay'])
            else:
                self.Madx.write('alphay0=0;\n')
            if 'energy' in initopt.keys():
                self.Madx.write('beam, particle=electron,energy=%f,sigt=1e-3,sige=1e-4;\n\n' % initopt['energy'])
            else:
                self.Madx.write('beam, particle=electron,energy=140,sigt=1e-3,sige=1e-4;\n\n')
        else:
            self.Madx.write('betax0=30;\n')
            self.Madx.write('betay0=30;\n')
            self.Madx.write('alphax0=0;\n')
            self.Madx.write('alphay0=0;\n\n')
            self.Madx.write('beam, particle=electron,energy=140,sigt=1e-3,sige=1e-4;\n\n')

        # update fixed angle of bending magnets for the different paths
        if 'SAT' in self.dest:
            ang = self.Facility.getRegExpElement('S20SY02', 'MKAC', 'design_kick')
            self.Facility.setRegExpElement('S20SY02', 'MKAC0.0', 'cory', ang[0])
            ang = self.Facility.getRegExpElement('S20SY02', 'MKDC', 'design_kick')
            self.Facility.setRegExpElement('S20SY02', 'MKDC0.0', 'cory', ang[0])
            self.Facility.setRegExpElement('S20SY02', 'MBND', 'angle', 2)
        else:
            self.Facility.setRegExpElement('S20SY02', 'MK.C0.0', 'cory', 0)
            self.Facility.setRegExpElement('S20SY02', 'MBND', 'angle', 0)

        if 'SPO' in self.dest:
            ang = self.Facility.getRegExpElement('S30CB15', 'MKAC', 'design_kick')
            self.Facility.setRegExpElement('S30CB15', 'MKAC0.0', 'cory', ang[0])
            ang = self.Facility.getRegExpElement('S30CB15', 'MKDC', 'design_kick')
            self.Facility.setRegExpElement('S30CB15', 'MKDC0.0', 'cory', ang[0])
            self.Facility.setRegExpElement('S30CB15', 'MBND', 'angle', 2)
        else:
            self.Facility.setRegExpElement('S30CB15', 'MK.C0.0', 'cory', 0)
            self.Facility.setRegExpElement('S30CB15', 'MBND', 'angle', 0)


        if 'S10' in self.dest:
            self.Facility.setRegExpElement('S10DI01', 'MBND', 'angle', 20)
        else:
            self.Facility.setRegExpElement('S10DI01', 'MBND', 'angle', 0)

        # write the lattice
        self.line.writeLattice(self.Madx, self.Facility)  # write lattice to madx




    def writeLatticeTracking(self,start='#s',end='#e'):
        self.Madx.write('use, sequence=swissfel;\n')
        self.Madx.write('Select,flag=Error,pattern="S[AP][TO].*";\n')
        self.Madx.write('EALIGN,DY=0.01;\n')
        self.Madx.write('select, flag=twiss, column=NAME,S,BETX,ALFX,BETY,ALFY,X,PX,Y,PY,DX,DPX,DDX,DY,DPY,DDY,MUX,MUY,RE56,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44;\n')
        self.Madx.write(
            'twiss, range=%s/%s,rmatrix, sequence=swissfel,betx=betax0,bety=betay0,alfx=alphax0,alfy=alphay0,''file="twiss.dat";\n' % (start,end))
        self.Madx.write('plot, haxis = s, vaxis = betx, bety, range =  #s/#e,colour=100;\n')
        self.Madx.write('plot, haxis = s, vaxis = dx, dy, range =  #s/#e,colour=100;\n')
        self.Madx.write('exit;')



    def setLine(self, destination):
        #   selecting destination and the model
        self.dest = 'SARBD02'
        if destination == 1:
            self.dest = 'SATBD02'
        elif destination == 2:
            self.dest = 'S10BD01'
        elif destination == 3:
            self.dest = 'SPOSY04'
 #       print('***** setting destination for:' ,self.dest)
        sec = self.Facility.getSection(self.dest)
        path = sec.mapping
        self.line = self.Facility.BeamPath(path)
        self.line.setRange('SINLH01')
